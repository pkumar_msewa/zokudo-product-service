package com.cards.zokudo.response;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class TokenResponseDTO {

    private String authorizationToken;
}
