package com.cards.zokudo.response;

import lombok.Data;
import lombok.ToString;
import org.springframework.http.HttpStatus;
@Data
public class ApiResponse<T>{

    private final HttpStatus status;
    private final String message;
    private final String code;
    private T body;

    public ApiResponse(HttpStatus status, String message, String code) {
        this.status = status;
        this.message = message;
        this.code = code;
    }

    public ApiResponse(final HttpStatus status, final String message, final String code, final T body) {
        this.status = status;
        this.message = message;
        this.code = code;
        this.body = body;
    }

    public static <T> ApiResponse.ApiResponseBuilder<T> builder() {
        return new ApiResponse.ApiResponseBuilder();
    }

    @ToString
    public static class ApiResponseBuilder<T> {
        private HttpStatus status = HttpStatus.OK;
        private String message = "Success";
        private String code = HttpStatus.OK.toString();
        private T body;

        public ApiResponse.ApiResponseBuilder<T> status(final HttpStatus status) {
            this.status = status;
            return this;
        }

        public ApiResponse.ApiResponseBuilder<T> message(final String message) {
            this.message = message;
            return this;
        }

        public ApiResponse.ApiResponseBuilder<T> code(final String code) {
            this.code = code;
            return this;
        }

        public ApiResponse.ApiResponseBuilder<T> body(final T body) {
            this.body = body;
            return this;
        }

        public ApiResponse<T> build() {
            return new ApiResponse(this.status, this.message, this.code, this.body);
        }
    }
}

