package com.cards.zokudo.response;

import com.cards.zokudo.entities.Users;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class UserDetailsResponse {

    private Users users;
    private String token;
}
