package com.cards.zokudo.dto.response;

import lombok.Data;

@Data
public class PageAndPrivilegeDTO {
    private String pageName;
    private String privilegeName;
}
