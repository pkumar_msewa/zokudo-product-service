package com.cards.zokudo.dto.response;

import lombok.Data;

@Data
public class CreateProgramResponseDTO {
    private String programHashcode;
}
