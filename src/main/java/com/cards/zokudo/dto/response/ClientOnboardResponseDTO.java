package com.cards.zokudo.dto.response;

import lombok.Data;

@Data
public class ClientOnboardResponseDTO {
    private String userHashId;
    private long clientId;
}
