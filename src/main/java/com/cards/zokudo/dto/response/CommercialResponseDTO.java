package com.cards.zokudo.dto.response;

import lombok.Data;

@Data
public class CommercialResponseDTO {
    private String commercialHashId;
}
