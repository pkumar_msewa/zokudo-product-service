package com.cards.zokudo.dto.response;

import com.cards.zokudo.entities.AgentDetails;
import lombok.Data;

import javax.ws.rs.core.MultivaluedMap;
import java.util.List;
import java.util.Map;

@Data
public class PoolBalanceResponseDTO {
    private List<Long> superDistList;
    private Map<String, MultivaluedMap<String, Long>> distAgentListMap;
    private Map<String, Object> superDistAgentListMap;
    private Map<String, List<AgentDetails>> agentIdList;

}
