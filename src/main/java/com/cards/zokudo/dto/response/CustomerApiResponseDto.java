package com.cards.zokudo.dto.response;

import lombok.Data;

@Data
public class CustomerApiResponseDto {
    private long userId;
    private String userHashId;
    private String message;
}
