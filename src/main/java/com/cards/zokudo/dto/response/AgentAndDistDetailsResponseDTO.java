package com.cards.zokudo.dto.response;

import lombok.Data;

@Data
public class AgentAndDistDetailsResponseDTO {
    private Long agentId;
    private  String agentName;
    private  Long distributorId;
    private String distributorName;

}
