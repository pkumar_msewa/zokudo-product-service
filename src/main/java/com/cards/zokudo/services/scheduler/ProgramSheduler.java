package com.cards.zokudo.services.scheduler;

import com.cards.zokudo.entities.Program;
import com.cards.zokudo.repositories.ProgramRepository;
import com.cards.zokudo.util.Constants;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.util.List;
import java.util.Properties;

@Service
@Slf4j
public class ProgramSheduler {

    @Autowired
    ProgramRepository programRepository;


//    @Scheduled(cron = "0 0 10,22 ? * *")
    public void checkLowPrefundClient() {
        log.info("Low Prefund Cron Begins...");
        List<Program> programList = programRepository.findAllProgramsBasedOnBalance();
        for(Program program : programList) {

            String body = constructBodyOfEmail(program.getProgramName(), program.getBalance());

           // String[] sendTo = {program.getEmail()};
            //sendFromGMail(Constants.EMAIL_ID_FOR_ACTIVATION_AND_OTP, Constants.EMAIL_ID_PASSWORD_FOR_ACTIVATION_AND_OTP

            //String[] sendTo = {program.getEmail()};
            /*sendFromGMail(Constants.EMAIL_ID_FOR_ACTIVATION_AND_OTP, Constants.EMAIL_ID_PASSWORD_FOR_ACTIVATION_AND_OTP
            ,sendTo,"Prefund Balance Status", body);*/
          //  ,sendTo,"Prefund Balance Status", body);
        }

        log.info("Low Prefund Cron Ends...");
    }

    private String constructBodyOfEmail(String entityName, Double balance) {
        StringBuffer header = new StringBuffer(
                "<!DOCTYPE html>\n" +
                        "<html>\n" +
                        "<head>\n" +
                        "</head>\n" +
                        "<body>\n" +
                        "Dear "+entityName+",<br>" +
                        "Your Prefund balance is Rs"+balance+". Please prefund your account for hassle free services.<br>"+
                        "Regards,<br>"+
                        "Team Autobots.\n"+
                        "</body></html>"
        );
        return  header.toString();
    }

    private static void sendFromGMail(String from, String pass, String[] to, String subject, String body) {
        Properties props = System.getProperties();
        String host = "smtp.gmail.com";
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.smtp.host", host);
        props.put("mail.smtp.user", from);
        props.put("mail.smtp.password", pass);
        props.put("mail.smtp.port", "587");
        props.put("mail.smtp.auth", "true");

        Session session = Session.getInstance(props);
        MimeMessage message = new MimeMessage(session);

        try {
            message.setFrom(new InternetAddress(from));
            InternetAddress[] toAddress = new InternetAddress[to.length];

            // To get the array of addresses
            for (int i = 0; i < to.length; i++) {
                toAddress[i] = new InternetAddress(to[i]);
            }

            for (int i = 0; i < toAddress.length; i++) {
                message.addRecipient(Message.RecipientType.TO, toAddress[i]);
            }

            message.setSubject(subject);
            message.setText(body, "UTF-8", "html");
            Transport transport = session.getTransport("smtp");
            transport.connect(host, from, pass);
            transport.sendMessage(message, message.getAllRecipients());
            transport.close();
        } catch (AddressException ae) {
            ae.printStackTrace();
        } catch (MessagingException me) {
            me.printStackTrace();
        }

    }



}
