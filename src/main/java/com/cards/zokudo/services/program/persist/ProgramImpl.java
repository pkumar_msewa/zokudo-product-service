package com.cards.zokudo.services.program.persist;

import com.cards.zokudo.dto.request.ProgramResponseDTO;
import com.cards.zokudo.dto.response.CommonServiceResponse;
import com.cards.zokudo.dto.response.CreateProgramResponseDTO;
import com.cards.zokudo.dto.response.PoolBalanceResponseDTO;
import com.cards.zokudo.entities.*;
import com.cards.zokudo.enums.*;
import com.cards.zokudo.exceptions.BadRequestException;
import com.cards.zokudo.exceptions.BizException;
import com.cards.zokudo.exceptions.CustomRuntimeException;
import com.cards.zokudo.repositories.*;
import com.cards.zokudo.response.ApiResponse;
import com.cards.zokudo.services.ClientService;
import com.cards.zokudo.services.SORService;
import com.cards.zokudo.services.WalletService;
import com.cards.zokudo.util.AppConstants;
import com.cards.zokudo.util.CommonUtil;
import com.cards.zokudo.util.Constants;
import com.cards.zokudo.util.UrlMetaData;
import com.google.common.collect.Sets;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.MultiValuedMap;
import org.apache.commons.collections4.multimap.ArrayListValuedHashMap;
import org.apache.commons.lang3.StringUtils;
import org.codehaus.jettison.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.*;
import javax.ws.rs.client.Client;
import javax.ws.rs.core.MultivaluedHashMap;
import javax.ws.rs.core.MultivaluedMap;

@Slf4j
@Service
public class ProgramImpl implements ProgramInf {

    private final UsersRepository usersRepository;
    private final CorporateProcessorRepository corporateProcessorRepository;
    private final ProgramRepository programRepository;
    private final UsersHasProgramRepository usersHasProgramRepository;
    private final CardTypeRepository cardTypeRepository;
    private final ClientDetailsRepository clientDetailsRepository;
    private final BinRangeRepository binRangeRepository;
    private final Client client;
    private final String applicationLevelUserName;
    private final String applicationLevelUserPassword;
    private final UrlMetaData urlMetaData;
    private final WalletService walletService;
    private final ProgramHasBinRepository programHasBinRepository;
    private final SubBinRangeRepository subBinRangeRepository;
    private final RoleRepository roleRepository;
    private final AgentDetailsRepository agentDetailsRepository;
    private final ClientService clientService;
    private final DistributorRepository distributorRepository;
    private final SORService sorService;

    @Autowired
    public ProgramImpl(final UsersRepository usersRepository,
                       final CorporateProcessorRepository corporateProcessorRepository,
                       final ProgramRepository programRepository,
                       final UsersHasProgramRepository usersHasProgramRepository,
                       final CardTypeRepository cardTypeRepository,
                       final ClientDetailsRepository clientDetailsRepository,
                       final BinRangeRepository binRangeRepository,
                       @Qualifier(value = "client") final Client client,
                       @Value("${applicationLevel.user.name}") final String applicationLevelUserName,
                       @Value("${applicationLevel.user.password}") final String applicationLevelUserPassword,
                       final UrlMetaData urlMetaData,
                       final WalletService walletService,
                       final ProgramHasBinRepository programHasBinRepository,
                       final SubBinRangeRepository subBinRangeRepository,
                       final RoleRepository roleRepository,
                       final AgentDetailsRepository agentDetailsRepository,
                       final ClientService clientService,
                       final DistributorRepository distributorRepository,
                       final SORService sorService) {
        this.usersRepository = usersRepository;
        this.corporateProcessorRepository = corporateProcessorRepository;
        this.programRepository = programRepository;
        this.usersHasProgramRepository = usersHasProgramRepository;
        this.cardTypeRepository = cardTypeRepository;
        this.clientDetailsRepository = clientDetailsRepository;
        this.binRangeRepository = binRangeRepository;
        this.client = client;
        this.applicationLevelUserName = applicationLevelUserName;
        this.applicationLevelUserPassword = applicationLevelUserPassword;
        this.urlMetaData = urlMetaData;
        this.walletService = walletService;
        this.programHasBinRepository = programHasBinRepository;
        this.subBinRangeRepository = subBinRangeRepository;
        this.roleRepository = roleRepository;
        this.agentDetailsRepository = agentDetailsRepository;
        this.clientService = clientService;
        this.distributorRepository = distributorRepository;
        this.sorService = sorService;
    }

    @Override
    public Object execute(ProgramRequestDTO programDTO, String programUrl) {

        // validate program request
        validateProgram(programDTO);

        ClientDetails clientDetails = clientDetailsRepository.findByEmail(programDTO.getUser().getEmail());

        /* check for existing client's programPlan*/
        validateProgramPlan(clientDetails.getId(), programDTO.getProgramPlan());

        /* check limit for GPR/GC program */
        validateGPRGCProgramLimit(clientDetails, programDTO.getReloadableType());

        // execute program onboarding
        Program program = onboardProgram(programDTO, clientDetails);

        // * * Create Program for SOR Service.
        log.info("** Update New Program for SOR Module with program name {} | program Hashid {} | program Id {}",program.getProgramName(),
                program.getProgramHashId(),program.getId());
        sorService.createProgram(program,programUrl,programDTO.getReloadableType());

        programDTO.setProgramId(program.getId());

        /*wallet service - create default currencies*/
        walletService.addCurrencyWithClient(clientDetails.getId(), programDTO, programUrl);

        /*wallet service - create default pocket*/
        final CommonServiceResponse pocketServiceResp = walletService.createDefaultPocket(program.getId(), clientDetails.getId(), programUrl);
        if (!AppConstants.successCode.equalsIgnoreCase(pocketServiceResp.getCode())) {
            throw new CustomRuntimeException(false, pocketServiceResp.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }

        /*wallet service - create client accounts*/
        walletService.createClientAccount(clientDetails.getId(), programUrl);

        /*wallet service - add default fees to the client */
        walletService.addDefaultFeesForClient(clientDetails.getId(), program.getId(), programUrl);
        walletService.addDefaultRevenueForProgram(clientDetails.getId(), program.getId(), programUrl);

        /*wallet service - add default clientCommission and discount*/
        walletService.addDefaultClientCommissionForClientByProgram(clientDetails.getId(), program.getId(), programUrl);
        walletService.addDefaultClientDiscountForClientByProgram(clientDetails.getId(), program.getId(), programUrl);

        return onboardProgramResponse(programDTO, program, clientDetails);
    }

    private void validateProgramPlan(Long clientId, ProgramPlans programPlan) {
        Map<String, ProgramPlans> programPlanMap;
        programPlanMap = getProgramPlanDetails(clientId);

        if(programPlanMap.size() !=0) {
            if (!programPlanMap.get("programPlan").equals(programPlan)){
                throw new BadRequestException(BizErrors.BAD_REQUEST_ERROR.getValue(), "ProgramPlan can't be changed once assigned! client has already opted for{}: " + programPlanMap.get("programPlan") + " plan");
            }
        }
    }

    private void validateGPRGCProgramLimit(ClientDetails clientDetails, String reloadableCardType) {
        Map<String, Integer> programTypeMap;
        programTypeMap = getProgramTypeDetails(clientDetails.getId());
        if(reloadableCardType.equalsIgnoreCase("GPR") & programTypeMap.get("GPR") >= 1){
            throw new BadRequestException(BizErrors.BAD_REQUEST_ERROR.getValue(), "client has exceeded GPR program limit!");
        }

        if((reloadableCardType.equalsIgnoreCase("GC") || reloadableCardType.equalsIgnoreCase("GC1")) & programTypeMap.get("GC") >= 1){
            throw new BadRequestException(BizErrors.BAD_REQUEST_ERROR.getValue(), "client has exceeded GC program limit!");
        }
    }

    private ApiResponse onboardProgramResponse(ProgramRequestDTO programDTO, Program program, ClientDetails clientDetails) {
        if (!programDTO.isAddProgram()) {

            clientDetails.setOnboardingState(OnboardingState.PROGRAM);
            clientDetailsRepository.save(clientDetails);
        }
        CreateProgramResponseDTO createProgramResponseDTO = new CreateProgramResponseDTO();
        createProgramResponseDTO.setProgramHashcode(program.getProgramHashId());

        ApiResponse<Object> response = ApiResponse.builder()
                .code("200")
                .message("Success")
                .body(createProgramResponseDTO)
                .build();
        return response;
    }

    private Program onboardProgram(ProgramRequestDTO programDTO, final ClientDetails clientDetails) {

        Program program = addProgram(programDTO, clientDetails);

        userHasProgramEntry(program, programDTO.getUser());

        /* mapping selected bin range to the program*/
        programHasBin(programDTO, program);

        //update sub bin range status
        updateSubBinRangeStatus(programDTO);
        updateCorporateProcessorStatus(programDTO);


        return program;
    }

    private void updateSubBinRangeStatus(ProgramRequestDTO programDTO) {
        log.info("Inside update sub bin range status for programRequest :{}", programDTO.toString());
        List<String> binRanges = programDTO.getBinNumbers();
        log.info("BinRanges size :{}", binRanges.size());
        List<SubBinRange> subBinRanges = new ArrayList<>();
        for (String binRange : binRanges) {
            log.info("updating binRange :{}", binRange);
            SubBinRange subBinRangeResult = subBinRangeRepository.findBySubBin(binRange.substring(6, 8));
            subBinRangeResult.setStatus("ASSIGNED");
            //subBinRangeResult.setCardType(programDTO.getCardType());
            subBinRanges.add(subBinRangeResult);
        }
        subBinRangeRepository.saveAll(subBinRanges);
        log.info("Bin Range updated successfully");

    }

    private Program addProgram(ProgramRequestDTO programDTO, final ClientDetails clientDetails) {
        try {
            log.info("adding a new program with program name: {}",programDTO.getProgramName());
            String programUrl = getHostUrl(programDTO.getProgramName());
            log.info("ProgramUrl: {}",programUrl);
            Program program = new Program();
            program.setCurrency(CommonUtil.getString(programDTO.getCurrency()));
            program.setHostUrl(programUrl);
            program.setBalance(0.0);
            //program.setBinId(programDTO.getBinId());
            program.setCardImageUrl(CommonUtil.uploadImage(programDTO.getCardImage(), program.getHostUrl(), Constants.CARD_IMAGE_DIR));
            program.setCorporateProcessor(programDTO.getCorporateProcessor());
            //program.setNotificationBaseUrl(CommonUtil.getString(programDTO.getNotificationUrl()));
            program.setProgramHashId(Constants.PROGRAM + CommonUtil.getRandomString(6).toUpperCase());
            program.setProgramLogo(CommonUtil.uploadImage(programDTO.getProgramLogo(), program.getHostUrl(), Constants.PROGRAM_LOGO_DIR));
            program.setProgramName(CommonUtil.getString(programDTO.getProgramName()));
            //program.setWalletRequired(programDTO.isWalletRequiredBoolean());
            program.setStatus(programDTO.isAddProgram() ? Status.ACTIVE : Status.INACTIVE);
            // program.setEmail(programDTO.getEmail());
            //program.setMobile(programDTO.getMobile());
            program.setClientId(clientDetails.getId());
            program.setProgramPlan(programDTO.getProgramPlan());
            programRepository.save(program);
            log.info("new program added successfully!");
            return program;
        } catch (DataIntegrityViolationException e) {
            log.error(e.getMessage(),e);
            throw new BizException(BizErrors.APPLICATION_ERROR.getValue(), "program name already exist! please provide new program name");
        }

    }

    private void updateCorporateProcessorStatus(ProgramRequestDTO programDto) {
        CorporateProcessor corporateProcessor = programDto.getCorporateProcessor();
        corporateProcessor.setStatus(Status.ASSIGNED);
        corporateProcessor.setZaggleCompanyId(programDto.getCompanyId());
        corporateProcessorRepository.save(corporateProcessor);
    }

    private void userHasProgramEntry(Program program, Users user) {
    	if(program.getProgramPlan().toString().equals(ProgramPlans.DISTRIBUTOR.toString())) {
    		Role role = roleRepository.findByName(Roles.ROLE_CLIENT_DIST.toString());
        	Set<Role> roles = Sets.newHashSet();
        	roles.add(role);
        	user.setUserRoles(roles);
        	usersRepository.save(user);
    	}
        UsersHasProgram usersProgram = new UsersHasProgram();
        usersProgram.setUsers(user);
        usersProgram.setProgram(program);
        usersProgram.setStatus(Status.ACTIVE);
        usersHasProgramRepository.save(usersProgram);
    }

    private void programHasBin(ProgramRequestDTO programDTO, Program program) {
        List<ProgramHasBin> listOfProgramBin = new ArrayList<>();
        ProgramHasBin programHasBin = new ProgramHasBin();
        try {
            for (String binNumber : programDTO.getBinNumbers()) {
                //ProgramHasBin programHasBin = new ProgramHasBin();
                String reloadableCardType;
                programHasBin.setBinNumber(binNumber);
                programHasBin.setProgramId(program.getId());
                if(programDTO.getReloadableType().equalsIgnoreCase("GC1")){
                    reloadableCardType = "GC";
                    log.info("received program type as GC1, setting as GC");
                }
                else
                    reloadableCardType = programDTO.getReloadableType();
                programHasBin.setProgramType(programDTO.getCardType() + reloadableCardType);
                listOfProgramBin.add(programHasBin);
            }
            programHasBinRepository.saveAll(listOfProgramBin);
        } catch (Exception e) {
            log.info("Error while saving bin.");
            throw new BizException(BizErrors.APPLICATION_ERROR.getValue(), "Unable to Add bin range. ");
        }
    }

    private String getHostUrl(String programName) {
        programName = programName.length() > 3 ? programName.substring(0, 3) : programName;
        return (programName + CommonUtil.getRandomString(6 - programName.length())).toLowerCase();
    }

    private void validateProgram(ProgramRequestDTO programRequestDTO) {

        fieldValidation(programRequestDTO);
        dataValidation(programRequestDTO);

    }

    private void dataValidation(ProgramRequestDTO programRequestDTO) {
        Users user = usersRepository.findByUserHashId(programRequestDTO.getUserHashId());
        if (!programRequestDTO.isAddProgram()) {
            programRequestDTO.setEmail(user.getEmail());
            programRequestDTO.setMobile(user.getMobile());
        }

        if (user == null)
            throw new BadRequestException(BizErrors.BAD_REQUEST_ERROR.getValue(), "client does not exist!");

        if (user.isBlocked())
            throw new BadRequestException(BizErrors.BAD_REQUEST_ERROR.getValue(), "client is blocked!can not assign a program");

        CorporateProcessor corporateProcessor = corporateProcessorRepository.getOne(Long.parseLong(programRequestDTO.getCorporateProcessorId()));
        if (corporateProcessor == null)
            throw new BadRequestException(BizErrors.BAD_REQUEST_ERROR.getValue(), "corporate processor not found");

        if (!Status.UNASSIGNED.equals(corporateProcessor.getStatus()))
            throw new BadRequestException(BizErrors.BAD_REQUEST_ERROR.getValue(), "invalid status of corporate processor");

        /*BinRange binRange = binRangeRepository.findByProgramType(programRequestDTO.getReloadableType());
        if(binRange == null){
            throw new BadRequestException(BizErrors.BAD_REQUEST_ERROR.getValue(), "Invalid program type ! No bin is available for the selected programType");
        }

        programRequestDTO.setBinId(binRange.getBin());*/
        programRequestDTO.setCorporateProcessor(corporateProcessor);
        programRequestDTO.setUser(user);
    }

    private void fieldValidation(ProgramRequestDTO programRequestDTO) {
        //Program program = programRepository.findByProgramHashId(programRequestDTO.getProgramHashId());

        if (StringUtils.isBlank(programRequestDTO.getUserHashId()) & StringUtils.isBlank(programRequestDTO.getClientId())) {
            throw new BadRequestException(BizErrors.BAD_REQUEST_ERROR.getValue(), "Provide either clientId or userHashId");
        }

        if (StringUtils.isNotBlank(programRequestDTO.getUserHashId())) {
            programRequestDTO.setUserHashId(CommonUtil.getString(programRequestDTO.getUserHashId()));
        }

        if (StringUtils.isBlank(programRequestDTO.getCorporateProcessorId()))
            throw new BadRequestException(BizErrors.BAD_REQUEST_ERROR.getValue(), "please select corporate processor");

        if (StringUtils.isBlank(programRequestDTO.getCurrency()))
            throw new BadRequestException(BizErrors.BAD_REQUEST_ERROR.getValue(), "please select currency");

        if (StringUtils.isBlank(programRequestDTO.getProgramName()))
            throw new BadRequestException(BizErrors.BAD_REQUEST_ERROR.getValue(), "please enter program name");

        if (programRequestDTO.getProgramPlan() == null)
            throw new BadRequestException(BizErrors.BAD_REQUEST_ERROR.getValue(), "please enter program plan");

        if (StringUtils.isBlank(programRequestDTO.getReloadableType())
                || (!programRequestDTO.getReloadableType().equalsIgnoreCase("GPR")
                & !programRequestDTO.getReloadableType().equalsIgnoreCase("GC")
                & !programRequestDTO.getReloadableType().equalsIgnoreCase("GC1"))
        ) {
            throw new BadRequestException(BizErrors.BAD_REQUEST_ERROR.getValue(), "please enter reloadableType");
        }

        if (StringUtils.isBlank(programRequestDTO.getCardType())
                || (!programRequestDTO.getCardType().equalsIgnoreCase("PHYSICAL")
                & !programRequestDTO.getCardType().equalsIgnoreCase("VIRTUAL")
                & !programRequestDTO.getCardType().equalsIgnoreCase("PHY_VIR"))
        ) {
            throw new BadRequestException(BizErrors.BAD_REQUEST_ERROR.getValue(), "please select at least one card type!");
        }
        if (StringUtils.isNotBlank(programRequestDTO.getEmbossing())) {
            programRequestDTO.setEmbossing(CommonUtil.getString(programRequestDTO.getEmbossing()));
        }

        if (StringUtils.isNotBlank(programRequestDTO.getCollaterals())) {
            programRequestDTO.setCollaterals(CommonUtil.getString(programRequestDTO.getCollaterals()).replaceAll(" ", ","));
        }

    }

    @Override
    public Object updateProgram(ProgramRequestDTO programDTO) {
        try {
            Program program = programRepository.findByProgramHashId(programDTO.getProgramHashId());
            if (programDTO.getCurrency() != null) {
                program.setCurrency(CommonUtil.getString(programDTO.getCurrency()));
            }
            if (programDTO.getProgramName() != null) {
                program.setHostUrl(getHostUrl(programDTO.getProgramName()));
            }
            if (programDTO.getCardImage() != null) {
                program.setCardImageUrl(CommonUtil.uploadImage(programDTO.getCardImage(), program.getHostUrl(), Constants.CARD_IMAGE_DIR));
            }
            if (programDTO.getCorporateProcessor() != null) {
                program.setCorporateProcessor(programDTO.getCorporateProcessor());
            }
           /* if (programDTO.getNotificationUrl() != null) {
                program.setNotificationBaseUrl(CommonUtil.getString(programDTO.getNotificationUrl()));
            }*/
            if (programDTO.getProgramLogo() != null) {
                program.setProgramLogo(CommonUtil.uploadImage(programDTO.getProgramLogo(), program.getHostUrl(), Constants.PROGRAM_LOGO_DIR));
            }
            if (programDTO.getProgramName() != null) {
                program.setProgramName(CommonUtil.getString(programDTO.getProgramName()));
            }
//            if (programDTO.getCardTypeEntity() != null) {
//                program.setCardType(programDTO.getCardTypeEntity());
//            }

            programRepository.save(program);
            return ApiResponse.builder().body(true).build();
        } catch (DataIntegrityViolationException e) {
            log.error(e.getMessage());
            throw new BizException(BizErrors.APPLICATION_ERROR.getValue(), "failed  to update program ");
        }
    }

    @Override
    public ResponseEntity<?> findProgramById(final String programId) {
        Program program = programRepository.findById(Long.parseLong(programId));
        if (program == null) {
            throw new BizException("No program found.", "No program found.");
        }
        return new ResponseEntity<>(program, HttpStatus.OK);
    }

    @Override
    public ResponseEntity<?> getListOfProgramIdAndName() {
        List<Object> programList = programRepository.getProgramIdAndName();
//        List<Program> programList = programRepository.findAll();
        log.info("programList size : {}", programList.size());
        if (programList == null) {
            throw new BizException("No program list found.", "No program list found.");
        }
        return new ResponseEntity<>(programList, HttpStatus.OK);
    }

    @Override
    public ResponseEntity<?> getListOfProgramIdAndNameByProgramIds(ProgramRequestDTO programDTO) {
    	List<Long> programIds = new ArrayList<>();
    	for (String program : programDTO.getProgramIdList()) {
			programIds.add(Long.parseLong(program));
		}
        List<Object> programList = programRepository.getListOfProgramIdAndAndName(programIds);
//        List<Program> programList = programRepository.findAll();
        log.info("programList size : {}", programList.size());
        if (programList == null) {
            throw new BizException("No program list found.", "No program list found.");
        }
        return new ResponseEntity<>(programList, HttpStatus.OK);
    }

    public List<Object> getListOfDistributorProgramIds() {
        List<Object> programList = programRepository.getProgramIdList();
        log.info("programList size : {}", programList.size());
        if (programList == null) {
            throw new BizException("No program list found.", "No program list found.");
        }
        return programList;
    }

    @Override
    public ResponseEntity<?> getProgramType(Long programId) {
        ProgramHasBin programType = programHasBinRepository.findByProgramId(programId);
        if (programType == null) {
            throw new BizException("No program type found for the provided programId");
        }
        return new ResponseEntity<>(programType, HttpStatus.OK);
    }

    @Override
    public ResponseEntity<?> getProgramforClient(String clientHashId) {
        log.info("getting onboarded program name GPR/GC");
        try {
            ClientDetails clientsDetails = clientDetailsRepository.getByClientHashId(clientHashId);
            Map<String, Integer> programTypeMap;
            programTypeMap = getProgramTypeDetails(clientsDetails.getId());

            log.info("Client has onboarded for the program{}: " + programTypeMap);
            return new ResponseEntity<>(programTypeMap, HttpStatus.OK);
        }catch(Exception e){
            e.getMessage();
            throw new BizException(BizErrors.APPLICATION_ERROR.getValue(), "Unable to get programType details: "+e.getMessage());
        }
    }

    private Map<String, Integer> getProgramTypeDetails(Long clientId) {
        try {
            Map<String, Integer> programTypeMap = new HashMap<>();
            programTypeMap.put("GPR", 0);
            programTypeMap.put("GC", 0);
            List<Program> programList = programRepository.findAllProgramByClientId(clientId);
            if (programList.size() == 0) {
                return programTypeMap;
            }
            for (Program program : programList) {
                ProgramHasBin programHasBin = programHasBinRepository.findByProgramId(program.getId());
                if (programHasBin.getProgramType().equalsIgnoreCase(String.valueOf(ProgramTyps.PHYSICALGPR)) |
                        programHasBin.getProgramType().equalsIgnoreCase(String.valueOf(ProgramTyps.VIRTUALGPR)) |
                        programHasBin.getProgramType().equalsIgnoreCase(String.valueOf(ProgramTyps.PHY_VIRGPR))) {
                    programTypeMap.put("GPR", 1);
                }

                if (programHasBin.getProgramType().equalsIgnoreCase(String.valueOf(ProgramTyps.PHYSICALGC)) |
                        programHasBin.getProgramType().equalsIgnoreCase(String.valueOf(ProgramTyps.VIRTUALGC)) |
                        programHasBin.getProgramType().equalsIgnoreCase(String.valueOf(ProgramTyps.PHY_VIRGC))) {
                    programTypeMap.put("GC", 1);
                }
            }
            return programTypeMap;
        }catch(Exception e){
            e.getMessage();
            throw new BizException(BizErrors.APPLICATION_ERROR.getValue(), "Unable to get programType details: "+e.getMessage());
        }
    }

    @Override
    public ResponseEntity<?> getProgramPlanforClient(String clientHashId) {
        log.info("getting programPlan for the client");
        try {
            ClientDetails clientsDetails = clientDetailsRepository.getByClientHashId(clientHashId);
            Map<String, ProgramPlans> programPlanMap;
            programPlanMap = getProgramPlanDetails(clientsDetails.getId());

            return new ResponseEntity<>(programPlanMap, HttpStatus.OK);
        }catch(Exception e){
            e.getMessage();
            throw new BizException(BizErrors.APPLICATION_ERROR.getValue(), "Unable to get programPlan details: "+e.getMessage());

        }
    }

    @Override
    public ResponseEntity<?> getListOfClients(String programUrl, String role, String loginUserHashId) {
        log.info("Inside getListOfClients for role: {}",role);
        try {
            Program programDetails = programRepository.findByHostUrl(programUrl);
            PoolBalanceResponseDTO poolBalanceResponseDTO = new PoolBalanceResponseDTO();
            if (programDetails == null) {
                throw new BizException(BizErrors.APPLICATION_ERROR.getValue(), "Unable to get program details: ");
            }
            //fetching list of clientIds and superDistributorId
            List<JSONObject> superDistributorProgramList;
            Map<String, Object> superAdminAgentListMap = new HashMap<>();

            if (Roles.ROLE_ADMIN.toString().equals(role)) {
                log.info("getting list of superDistributorId where role: {}", role);
                superDistributorProgramList = programRepository.getListOfSuperDistPrograms(ProgramPlans.DISTRIBUTOR);
                if (superDistributorProgramList != null) {
                    List<Object> agentDetailsListForAdmin = new ArrayList<>();
                    List<Object> agentDetailsListByProgram = new ArrayList<>();
                    for (JSONObject superProgramObj : superDistributorProgramList) {
                        /* get clientDetails for program - superProgramObj*/
                        JSONObject clientDetails = clientDetailsRepository.getclientDetailsById(superProgramObj.getLong("clientId"));

                        /* get all distributor under program - superProgramObj*/
                        List<JSONObject> distributorList = distributorRepository.getDistributorByProgramHashId(superProgramObj.getString("programHashId"));

                        List<Object> distAndAgentList = new ArrayList<>();
                        /* get all agent Details list for each distributor in distributorList */
                        if(distributorList != null || !distributorList.isEmpty()){
                            for(JSONObject distObj : distributorList){
                                List<JSONObject> agentDetailsList = agentDetailsRepository.getAgentIdByDistributorHashId(distObj.getString("userHashId"));

                                /* create list of distObj and agentDetailsList */
                                distAndAgentList.add(distObj);
                                distAndAgentList.add(agentDetailsList);
                            }
                        }
                        /* add superProgramObj, superDist, distAndAgentList in agentDetailsListForAdmin list at 0, 1 2, index resp */
                        agentDetailsListForAdmin.add(superProgramObj);
                        agentDetailsListForAdmin.add(clientDetails);
                        agentDetailsListForAdmin.add(distAndAgentList);

                        agentDetailsListByProgram.add(agentDetailsListForAdmin);
                    }
                    superAdminAgentListMap.put("AgentListBySuperDist", agentDetailsListByProgram);
                }
                //poolBalanceResponseDTO.setSuperDistList(superDistributorProgramList);
                poolBalanceResponseDTO.setSuperDistAgentListMap(superAdminAgentListMap);
                return new ResponseEntity<>(poolBalanceResponseDTO, HttpStatus.OK);
            }
            if (Roles.ROLE_CLIENT_DIST.toString().equals(role) & loginUserHashId != null) {
                log.info("getting list of agents under superDistributorHashId: {} and role: {}", loginUserHashId, role);
                //validateProgramforSuperDistributor(programDetails, loginUserHashId);

                String programHashId = programDetails.getProgramHashId();
                List<Object> agentDetailsBySuperDistributorList = new ArrayList<>();
                Map<String, List<Object>> agentIdsBydistributorListMap = new HashMap<>();

                /* get distributor list by programHashId*/
                List<DistributorEntity> distributorDetailsList = distributorRepository.findByProgramHashId(programHashId);

                if (distributorDetailsList != null & !distributorDetailsList.isEmpty()) {
                    for (DistributorEntity distObj : distributorDetailsList) {

                        /* get list of agentIds By distributorHashId */
                        List<AgentDetails> agentDetailsList = agentDetailsRepository.findByDistributorHashId(distObj.getUserHashId());

                        List<Object> agentDetailsByDistributorList = new ArrayList<>();
                        agentDetailsByDistributorList.add(distObj);
                        agentDetailsByDistributorList.add(agentDetailsList);

                        agentDetailsBySuperDistributorList.add(agentDetailsByDistributorList);
                        //agentIdsBydistributorMap.put(distObj, agentIdList);
                    }
                    agentIdsBydistributorListMap.put("agentListByDistributor", agentDetailsBySuperDistributorList);
                }
                //poolBalanceResponseDTO.setDistAgentListMap(loggedInUsersAgentListListMap);
                return new ResponseEntity<>(agentIdsBydistributorListMap, HttpStatus.OK);
            }
            if (Roles.ROLE_DISTRIBUTOR.toString().equals(role) & loginUserHashId != null) {
                log.info("getting list of agentDetails under DistributorId: {} and role: {}", loginUserHashId, role);
                DistributorEntity distributorEntity = distributorRepository.findByUserHashId(loginUserHashId);
                List<AgentDetails> agentDetailsList = agentDetailsRepository.findByDistributorHashId(loginUserHashId);
                List<Object> agentDetailsListByDistributor = new ArrayList<>();
                MultivaluedMap<String, Object> agentDetailsListMap = new MultivaluedHashMap<>();
                agentDetailsListByDistributor.add(distributorEntity);
                agentDetailsListByDistributor.add(agentDetailsList);
                agentDetailsListMap.put("agentDetailsList", agentDetailsListByDistributor);
                //poolBalanceResponseDTO.setAgentIdList("agentIdList", agentIdList);
                return new ResponseEntity<>(agentDetailsListMap, HttpStatus.OK);
            }
            if (Roles.ROLE_AGENT.toString().equals(role) & loginUserHashId != null) {
                log.info("getting agentDetails for agent: {}", loginUserHashId);
                AgentDetails agentDetails = agentDetailsRepository.findByUserHashId(loginUserHashId);
                Map<String, AgentDetails> agentDetailsListMap = new HashMap<>();
                agentDetailsListMap.put("agentDetails", agentDetails);
                return new ResponseEntity<>(agentDetailsListMap, HttpStatus.OK);
            }
        }catch(Exception e){
            e.getMessage();
            log.error("error occurred while executing getListOfPoolBalances: "+e.getMessage());
            throw new BizException(BizErrors.APPLICATION_ERROR.getValue(), "Unable to get agentIdList");
        }
        return null;
    }

    @Override
    public Object getListOfProgramsByProgramPlans(String programUrl) {
        log.info("Inside getListOfProgramsByProgramPlans");
        try {
            List<Program> programs = programRepository.findAll();
            List<ProgramHasBin> programHasBins = programHasBinRepository.findAll();
            Map<Long,String> programHasBinsMap = new HashMap<Long,String>();

            programHasBins.forEach(programHasBin -> programHasBinsMap.put(programHasBin.getProgramId(),programHasBin.getProgramType()));

            List<ProgramResponseDTO> defaultProgramList = new ArrayList<>();
            List<ProgramResponseDTO> lendingProgramList = new ArrayList<>();
            List<ProgramResponseDTO> distProgramList = new ArrayList<>();
            for(Program program : programs){
                ProgramResponseDTO programRequestDTO = new ProgramResponseDTO();
                programRequestDTO.setProgramId(String.valueOf(program.getId()));
                programRequestDTO.setProgramName(program.getProgramName());
                programRequestDTO.setProgramPlan(program.getProgramPlan());
                programRequestDTO.setProgramTyps(programHasBinsMap.get(program.getId()));


                if(program.getProgramPlan().equals(ProgramPlans.DEFAULT)){
                    defaultProgramList.add(programRequestDTO);
                }
                if(program.getProgramPlan().equals(ProgramPlans.LENDING)){
                    lendingProgramList.add(programRequestDTO);
                }
                if(program.getProgramPlan().equals(ProgramPlans.DISTRIBUTOR)){
                    distProgramList.add(programRequestDTO);
                }
            }
            Map<String, List<ProgramResponseDTO>> programList = new HashMap<>();
            programList.put("DEFAULT", defaultProgramList);
            programList.put("LENDING", lendingProgramList);
            programList.put("DISTRIBUTOR", distProgramList);

            return ApiResponse.builder().body(programList).build();
        }catch(Exception e){
            e.getMessage();
            log.error("error occurred while getting program list by ProgramPlans: "+e);
            throw new BizException(BizErrors.APPLICATION_ERROR.getValue(), "Unable to get programList");
        }
    }

    private void validateProgramforSuperDistributor(Program programDetails, String loginUserHashId) {
        ClientDetails clientDetails = clientDetailsRepository.getByClientHashId(loginUserHashId);
        if(programDetails.getClientId() != clientDetails.getId()){
            log.error("program validation failed for superDistributor. programHashId: {} &  clientId: {}",programDetails.getProgramHashId(), clientDetails.getId());
            throw new BizException(BizErrors.APPLICATION_ERROR.getValue(), "program validation failed for loginUserHashId!");
        }
        if(!programDetails.getProgramPlan().equals(ProgramPlans.DISTRIBUTOR)){
            log.error("programPlan validation failed for superDistributor. programHashId: {} &  clientId: {}",programDetails.getProgramHashId(), clientDetails.getId());
            throw new BizException(BizErrors.APPLICATION_ERROR.getValue(), "superDistributor does not belong to DISTRIBUTOR programPlan! Available Plan: "+programDetails.getProgramPlan());
        }
    }

    private Map<String, ProgramPlans> getProgramPlanDetails(Long clientId) {
        try {
            Map<String, ProgramPlans> programPlanMap = new HashMap<>();
            //programPlanMap.put("programPlan", );
            List<Program> programList = programRepository.findAllProgramByClientId(clientId);
            if (programList.size() != 0) {
                for (Program program : programList) {
                    programPlanMap.put("programPlan", program.getProgramPlan());
                }
                return programPlanMap;
            }
            return programPlanMap;
        }catch(Exception e){
            e.getMessage();
            throw new BizException(BizErrors.APPLICATION_ERROR.getValue(), "Unable to get programPlan details: "+e.getMessage());
        }
    }

}


