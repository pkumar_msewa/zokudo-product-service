package com.cards.zokudo.services.program.persist;

import com.cards.zokudo.entities.CardType;
import com.cards.zokudo.entities.CorporateProcessor;
import com.cards.zokudo.entities.Users;
import com.cards.zokudo.enums.ProgramPlans;
import lombok.Data;
import org.springframework.web.multipart.MultipartFile;
import java.util.List;

@Data
public class ProgramRequestDTO {

    private String programName;
    private ProgramPlans programPlan;
    private String corporateProcessorId;
    private String currency;
   // private String notificationUrl;
    private String clientId;
    private String reloadableType;
    private String cardType;
   // private String walletRequired;
    private String userHashId;
    private String actionType;
    private String embossing;
    private String collaterals;
    private List<String> binNumbers;
    private MultipartFile cardImage;
    private MultipartFile programLogo;

    private CardType cardTypeEntity;
    private boolean walletRequiredBoolean;
    private Users user;
    private CorporateProcessor corporateProcessor;
    private String programHashId;
    private String companyId;
    private boolean addProgram;
    private long programId;
    private List<String> programIdList;

    private String email;
    private String mobile;
}
