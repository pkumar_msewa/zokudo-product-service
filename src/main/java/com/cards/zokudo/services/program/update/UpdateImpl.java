package com.cards.zokudo.services.program.update;

import com.cards.zokudo.entities.Program;
import com.cards.zokudo.enums.BizErrors;
import com.cards.zokudo.enums.Status;
import com.cards.zokudo.exceptions.BizException;
import com.cards.zokudo.repositories.ProgramRepository;
import com.cards.zokudo.response.ApiResponse;
import com.cards.zokudo.services.program.persist.ProgramRequestDTO;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public class UpdateImpl implements UpdateInf {

    private final ProgramRepository programRepository;

    public UpdateImpl(ProgramRepository programRepository) {
        this.programRepository = programRepository;
    }

    @Override
    public Object execute(ProgramRequestDTO programDTO) {
        return null;
    }

    @Override
    public Object executeBlockUnblock(ProgramRequestDTO programDTO) {
        Program program = validateBlockUnblock(programDTO);
        blockUnblock(program, programDTO.getActionType());
        return ApiResponse.builder().body(true).build();
    }

    private void blockUnblock(Program program, String actionType) {

        try {
            program.setStatus(Status.getEnum(actionType));
            programRepository.save(program);
        } catch (Exception e) {
            log.error(e.getMessage());
            throw new BizException(BizErrors.APPLICATION_ERROR.getValue(), "can not update status of program");
        }

    }

    private Program validateBlockUnblock(ProgramRequestDTO programDTO) {
        Program program = programRepository.findByProgramHashId(programDTO.getProgramHashId());
        if (program == null)
            throw new BizException(BizErrors.BAD_REQUEST_ERROR.getValue(), "program does not exist");

        if (StringUtils.isBlank(programDTO.getActionType()))
            throw new BizException(BizErrors.BAD_REQUEST_ERROR.getValue(), "action type should not empty");

        if (programDTO.getActionType().equals(program.getStatus()))
            throw new BizException(BizErrors.BAD_REQUEST_ERROR.getValue(), "program already " + program.getStatus());

        return program;
    }
}
