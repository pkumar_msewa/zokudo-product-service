package com.cards.zokudo.services.program.persist;


import org.springframework.http.ResponseEntity;

public interface ProgramInf {

    Object execute(ProgramRequestDTO programDTO, String programUrl);

    Object updateProgram(ProgramRequestDTO programDTO);

    ResponseEntity<?> findProgramById(String programId);

    ResponseEntity<?> getListOfProgramIdAndName();

    ResponseEntity<?> getListOfProgramIdAndNameByProgramIds(ProgramRequestDTO programDTO);

    ResponseEntity<?> getProgramType(Long programId);

    ResponseEntity<?> getProgramforClient(String clientHashId);

    ResponseEntity<?> getProgramPlanforClient(String clientHashId);

    ResponseEntity<?> getListOfClients(String programUrl, String role, String loginUserHashId);

    Object getListOfProgramsByProgramPlans(String programUrl);
}
