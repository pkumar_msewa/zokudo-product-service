package com.cards.zokudo.services.program.list;

import com.cards.zokudo.dto.request.CorporateProcessorFilterDto;
import com.cards.zokudo.dto.request.FilteredProgramListDto;
import com.cards.zokudo.entities.Processor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

public interface ListInf  {
    Object execute(String clientId,String page,String size);

    Object executeListProcessors();


    Object executeprogramlist(String client_id, String programHashId);

    Object executeListProcessors(String role, String page, String size);

    Object filteredCorparateProcessorList(HttpServletRequest request, HttpServletResponse response, CorporateProcessorFilterDto requestBody);

    Object listProgramWithFilters(HttpServletRequest request, HttpServletResponse response, FilteredProgramListDto filteredProgramListDto);

    List<Processor> fetchProcessors();

    Object getProgramListByClient(String clientId, String page, String size);

    Object getProgramByProgramName(String programName);
}
