package com.cards.zokudo.services.program.list;

import com.cards.zokudo.dto.request.CorporateProcessorFilterDto;
import com.cards.zokudo.dto.request.FilteredProgramListDto;
import com.cards.zokudo.entities.CorporateProcessor;
import com.cards.zokudo.entities.Processor;
import com.cards.zokudo.entities.Program;
import com.cards.zokudo.enums.BizErrors;
import com.cards.zokudo.enums.Roles;
import com.cards.zokudo.enums.Status;
import com.cards.zokudo.exceptions.BizException;
import com.cards.zokudo.repositories.ClientDetailsRepository;
import com.cards.zokudo.repositories.CorporateProcessorRepository;
import com.cards.zokudo.repositories.ProcessorRepository;
import com.cards.zokudo.repositories.ProgramRepository;
import com.cards.zokudo.response.ApiResponse;
import com.cards.zokudo.util.CommonUtil;
import com.google.common.collect.Maps;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;

@Slf4j
@Service
public class ListImpl implements ListInf {

    private final ProgramRepository programRepository;
    private final CorporateProcessorRepository corporateProcessorRepository;
    private final ClientDetailsRepository clientDetailsRepository;
    private final ProcessorRepository processorRepository;


    @Autowired
    public ListImpl(ProgramRepository programRepository, CorporateProcessorRepository corporateProcessorRepository,
                    ClientDetailsRepository clientDetailsRepository, ProcessorRepository processorRepository) {
        this.programRepository = programRepository;
        this.corporateProcessorRepository = corporateProcessorRepository;
        this.clientDetailsRepository = clientDetailsRepository;
        this.processorRepository = processorRepository;
    }


    @Override
    public Object execute(String clientId, String page, String size) {
        List<Program> programs = null;
        if (!StringUtils.isBlank(clientId)) {
            programs = programRepository.findByClientId(clientDetailsRepository.findUserId(Long.parseLong(clientId)));
        } else if ((!StringUtils.isBlank(page)) && (!StringUtils.isBlank(size))) {
            Sort sort = new Sort(Sort.Direction.DESC, "createdAt");
            Pageable pageable = PageRequest.of(Integer.parseInt(page), Integer.parseInt(size), sort);
            Object program = programRepository.findAll(pageable);
            return ApiResponse.builder().body(program).build();

        } else {
            programs = programRepository.findAll();
        }
        return ApiResponse.builder().body(programs).build();
    }


    @Override
    public Object executeListProcessors() {
        List<CorporateProcessor> corporateProcessors = corporateProcessorRepository.findAllByStatus(Status.UNASSIGNED);

        return ApiResponse.builder().body(corporateProcessors).build();
    }

    @Override
    public Object executeprogramlist(String client_id, String programHashId) {
        try {
            Program program = programRepository.findByProgramHashId(programHashId);
            Map<String, Program> map = Maps.newHashMap();
            map.put("programDetails", program);
            return map;
        } catch (Exception e) {
            throw new BizException(BizErrors.BAD_REQUEST_ERROR.getValue(), "failed to fetch the program");
        }
    }

    @Override
    public Object executeListProcessors(String role, String page, String size) {

            try {
                Object corporateProcessor = null;
                if (page != null && size != null) {
                    Sort sort = new Sort(Sort.Direction.DESC, "createdAt");
                    Pageable pageable = PageRequest.of(Integer.parseInt(page), Integer.parseInt(size), sort);
                    corporateProcessor = corporateProcessorRepository.findAll(pageable);
                } else {
                    corporateProcessor = corporateProcessorRepository.findAll();
                }
                Map<String, Object> map = Maps.newHashMap();
                map.put("corporateProcessorList", corporateProcessor);
                return map;
            } catch (Exception e) {
                log.error(e.getMessage());
                throw new BizException(BizErrors.BAD_REQUEST_ERROR.getValue(), "error in getting corporate Processor list");
            }

    }

    @Override
    public Object filteredCorparateProcessorList(HttpServletRequest request, HttpServletResponse response, CorporateProcessorFilterDto requestBody) {

        try {

            String role = request.getHeader("role");
            int page = Integer.parseInt(request.getHeader("page"));
            int size = Integer.parseInt(request.getHeader("size"));
            Page<CorporateProcessor> corporateProcessors;
            Sort sort = new Sort(Sort.Direction.DESC, "createdAt");
            Pageable pageable = PageRequest.of(page, size, sort);
            if (StringUtils.isNotBlank(requestBody.getDateRange())) {
                String dateArray[] = requestBody.getDateRange().split("-");
                String startDateStr = CommonUtil.dateTimeFromatter.format(CommonUtil.dateFormatterSlashWithTime.parse(dateArray[0] + CommonUtil.startTime));
                String endDateStr = CommonUtil.dateTimeFromatter.format(CommonUtil.dateFormatterSlashWithTime.parse(dateArray[1] + CommonUtil.endTime));
                Date startDate = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").parse(startDateStr);
                Date endDate = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").parse(endDateStr);
                corporateProcessors = corporateProcessorRepository.findAllByDate(startDate, endDate, pageable);
                return corporateProcessors;
            } else {
                Specification<CorporateProcessor> specification = getCorporateProcessorSpecification(requestBody);
                corporateProcessors = corporateProcessorRepository.findAll(specification, pageable);
                return corporateProcessors;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    @Override
    public Object listProgramWithFilters(HttpServletRequest request, HttpServletResponse response, FilteredProgramListDto filteredProgramListDto) {

        try {

            String role = request.getHeader("role");
            int page = Integer.parseInt(request.getHeader("page"));
            int size = Integer.parseInt(request.getHeader("size"));
            Page<Program> corporateProcessors;
            Sort sort = new Sort(Sort.Direction.DESC, "createdAt");
            Pageable pageable = PageRequest.of(page, size, sort);
            if (StringUtils.isNotBlank(filteredProgramListDto.getDateRange())) {
                String[] dateArray = filteredProgramListDto.getDateRange().split("-");
                String startDateStr = CommonUtil.dateTimeFromatter.format(CommonUtil.dateFormatterSlashWithTime.parse(dateArray[0] + CommonUtil.startTime));
                String endDateStr = CommonUtil.dateTimeFromatter.format(CommonUtil.dateFormatterSlashWithTime.parse(dateArray[1] + CommonUtil.endTime));
                Date startDate = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").parse(startDateStr);
                Date endDate = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").parse(endDateStr);
                corporateProcessors = programRepository.findAllByCreatedAtBetween(startDate, endDate, pageable);
                return corporateProcessors;
            } else {
                Specification<Program> specification = getProgramSpecification(filteredProgramListDto);
                corporateProcessors = programRepository.findAll(specification, pageable);
                return corporateProcessors;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    @Override
    public List<Processor> fetchProcessors() {
        return processorRepository.findByStatus(Status.ACTIVE);
    }

    private Specification<Program> getProgramSpecification(FilteredProgramListDto requestBody) {

        Specification<Program> specification =
                (corporateProcessorRoot, criteriaQuery, criteriaBuilder) -> criteriaBuilder.equal(criteriaBuilder.literal(1), 1);
        try {
            if (StringUtils.isNotBlank(requestBody.getProgramName())) {
                specification = specification.and((root, query, criteriaBuilder) ->
                        criteriaBuilder.equal(root.get("programName"), requestBody.getProgramName())
                );
            }
            if (StringUtils.isNotBlank(requestBody.getProgramHashId())) {
                specification = specification.and((root, query, criteriaBuilder) ->
                        criteriaBuilder.equal(root.get("programHashId"), requestBody.getProgramHashId())
                );
            }
            if (StringUtils.isNotBlank(requestBody.getBusinessType())) {
                specification = specification.and((root, query, criteriaBuilder) ->
                        criteriaBuilder.equal(root.get("businessId"), requestBody.getBusinessType())
                );
            }
            /*if (StringUtils.isNotBlank(requestBody.getProcessorName())) {
                specification = specification.and((root, query, criteriaBuilder) ->
                        criteriaBuilder.equal(root.get("corporateProcessor").get("name"), requestBody.getProcessorName())
                );
            }*/
            if (requestBody.getCorporateProcessorId() != null) {
                specification = specification.and((root, query, criteriaBuilder) ->
                        criteriaBuilder.equal(root.get("corporateProcessor").get("id"), requestBody.getCorporateProcessorId())
                );
            }
            if (requestBody.getClientCode() != null) {
                specification = specification.and((root, query, criteriaBuilder) ->
                        criteriaBuilder.equal(root.get("clientId"), requestBody.getClientCode())
                );
            }
        } catch (Exception e) {
            log.error(e.getMessage());
            throw new BizException(BizErrors.BAD_REQUEST_ERROR.getValue(), "Internal server error. Please try again after sometimes");
        }
        return specification;
    }

    private Specification<CorporateProcessor> getCorporateProcessorSpecification(CorporateProcessorFilterDto requestBody) {

        Specification<CorporateProcessor> specification =
                (corporateProcessorRoot, criteriaQuery, criteriaBuilder) -> criteriaBuilder.equal(criteriaBuilder.literal(1), 1);
        try {
            if (StringUtils.isNotBlank(requestBody.getCorporateName())) {
                specification = specification.and((root, query, criteriaBuilder) ->
                        criteriaBuilder.equal(root.get("name"), requestBody.getCorporateName())
                );
            }
            if (StringUtils.isNotBlank(requestBody.getCorporateCode())) {
                specification = specification.and((root, query, criteriaBuilder) ->
                        criteriaBuilder.equal(root.get("code"), requestBody.getCorporateCode())
                );
            }
            if (StringUtils.isNotBlank(requestBody.getProcessorName())) {
                specification = specification.and((root, query, criteriaBuilder) ->
                        criteriaBuilder.equal(root.get("code"), requestBody.getCorporateCode())
                );
            }
        } catch (Exception e) {
            log.error(e.getMessage());
            throw new BizException(BizErrors.BAD_REQUEST_ERROR.getValue(), "Internal server error. Please try again after sometimes");
        }
        return specification;
    }

    @Override
    public Object getProgramListByClient(String clientId, String page, String size) {
        log.info("Get Program list by client : {}", clientId);

        if (StringUtils.isBlank(clientId)) {
            log.error("Client id is blank");
            throw new BizException(BizErrors.DATA_NOT_FOUND.getValue(), "Client id is blank");
        }

        Sort sort = new Sort(Sort.Direction.DESC, "createdAt");
        Pageable pageable = PageRequest.of(Integer.parseInt(page), Integer.parseInt(size), sort);
        Page<Program> programPageList = programRepository.findByClientId(Long.parseLong(clientId), pageable);
        log.info("ProgramList size :{}", programPageList.getSize());
        if (programPageList.isEmpty()) {
            log.error("Program details not found for client : {}", clientId);
            throw new BizException(BizErrors.DATA_NOT_FOUND.getValue(), "No data found");
        }
        return programPageList;
    }

    @Override
    public Object getProgramByProgramName(String programName) {
        log.info("get program by program name: {}", programName);
        if (StringUtils.isBlank(programName)) {
            log.error("program name is blank");
            throw new BizException(BizErrors.DATA_NOT_FOUND.getValue(), "program name is blank");
        }
        Program program = programRepository.findByProgramName(programName);
        if (program == null) {
            log.error("Program details not found for program name : {}", programName);
            throw new BizException(BizErrors.DATA_NOT_FOUND.getValue(), "No data found");
        }
        return program;
    }
}
