package com.cards.zokudo.services.program.update;

import com.cards.zokudo.services.program.persist.ProgramRequestDTO;

public interface UpdateInf {
    Object execute(ProgramRequestDTO programDTO);

    Object executeBlockUnblock(ProgramRequestDTO programDTO);
}
