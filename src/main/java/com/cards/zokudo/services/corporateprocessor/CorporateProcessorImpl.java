package com.cards.zokudo.services.corporateprocessor;

import com.cards.zokudo.entities.Processor;
import com.cards.zokudo.entities.CorporateProcessor;
import com.cards.zokudo.enums.BizErrors;
import com.cards.zokudo.enums.Status;
import com.cards.zokudo.exceptions.BadRequestException;
import com.cards.zokudo.exceptions.BizException;
import com.cards.zokudo.repositories.ProcessorRepository;
import com.cards.zokudo.repositories.CorporateProcessorRepository;
import com.cards.zokudo.response.ApiResponse;
import com.cards.zokudo.util.CommonUtil;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public class CorporateProcessorImpl implements CorporateProcessorInf {

    private final ProcessorRepository processorRepository;
    private final CorporateProcessorRepository corporateProcessorRepository;

    @Autowired
    public CorporateProcessorImpl(ProcessorRepository processorRepository, CorporateProcessorRepository corporateProcessorRepository) {
        this.processorRepository = processorRepository;
        this.corporateProcessorRepository = corporateProcessorRepository;
    }

    @Override
    public Object execute(ProcessorDTO processorDTO) {

        // validation
        validateCorporateProcessor(processorDTO);

        //execution
        addCorporateProcessor(processorDTO);

        return ApiResponse.builder().body(true).build();
    }

    private void addCorporateProcessor(ProcessorDTO processorDTO) {

        try {
            CorporateProcessor corporateProcessor = new CorporateProcessor();
            corporateProcessor.setName(CommonUtil.getString(processorDTO.getCorporateName()));
            corporateProcessor.setCode(CommonUtil.getString(processorDTO.getCorporateCode()));
            corporateProcessor.setStatus(Status.UNASSIGNED);
            corporateProcessor.setCardProcessor(processorDTO.getCardProcessor());
            corporateProcessorRepository.save(corporateProcessor);
        } catch (DataIntegrityViolationException e) {
            log.error(e.getMessage());
            throw new BizException(BizErrors.APPLICATION_ERROR.getValue(), "Business Id should be unique");
        } catch (Exception e) {
            throw new BizException(BizErrors.APPLICATION_ERROR.getValue(), "process failed to assign processor to the business");
        }


    }

    private void validateCorporateProcessor(ProcessorDTO processorDTO) {

        if (StringUtils.isBlank(processorDTO.getCorporateCode()))
            throw new BadRequestException(BizErrors.BAD_REQUEST_ERROR.getValue(), "corporate code can not be empty");

        if (StringUtils.isBlank(processorDTO.getCorporateName()))
            throw new BadRequestException(BizErrors.BAD_REQUEST_ERROR.getValue(), "corporate name can not be empty");
        if (StringUtils.isBlank(processorDTO.getProcessor()))
            throw new BadRequestException(BizErrors.BAD_REQUEST_ERROR.getValue(), "please select processor");


        Processor cardProcessor = processorRepository.getOne(Long.parseLong(processorDTO.getProcessor()));

        if (cardProcessor == null)
            throw new BadRequestException(BizErrors.BAD_REQUEST_ERROR.getValue(), "invalid card processor");

        processorDTO.setCardProcessor(cardProcessor);

    }
}
