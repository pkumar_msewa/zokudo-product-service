package com.cards.zokudo.services.corporateprocessor;

import com.cards.zokudo.entities.Processor;
import lombok.Data;

@Data
public class ProcessorDTO {

    private String corporateName;
    private String corporateCode;
    private String processor;
    private Processor cardProcessor;

}
