package com.cards.zokudo.services.corporateprocessor;

public interface CorporateProcessorInf {
    Object execute(ProcessorDTO processorDTO);
}
