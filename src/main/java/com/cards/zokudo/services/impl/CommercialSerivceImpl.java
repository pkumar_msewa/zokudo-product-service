package com.cards.zokudo.services.impl;

import com.cards.zokudo.dto.request.CreateCommercialDTO;
import com.cards.zokudo.dto.request.FilterCommercialListDto;
import com.cards.zokudo.dto.request.FilteredProgramListDto;
import com.cards.zokudo.dto.response.CommercialResponseDTO;
import com.cards.zokudo.entities.*;
import com.cards.zokudo.enums.*;
import com.cards.zokudo.exceptions.BadRequestException;
import com.cards.zokudo.exceptions.BizException;
import com.cards.zokudo.repositories.ClientDetailsRepository;
import com.cards.zokudo.repositories.CommercialsRepository;
import com.cards.zokudo.repositories.ProgramRepository;
import com.cards.zokudo.repositories.UsersRepository;
import com.cards.zokudo.response.ApiResponse;
import com.cards.zokudo.services.CommercialService;
import com.cards.zokudo.util.CommonUtil;
import com.cards.zokudo.util.Constants;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.servlet.http.HttpServletRequest;
import java.text.SimpleDateFormat;
import java.util.*;

import static com.cards.zokudo.util.CommonUtil.*;
import static com.cards.zokudo.util.Constants.CLINET_DOCUMENT_DIR;
import static com.cards.zokudo.util.Constants.DEFAULT_FILE_SIZE_IN_MB;

@Service
public class CommercialSerivceImpl implements CommercialService {

    @Autowired
    private ProgramRepository programRepository;
    @Autowired
    private CommercialsRepository commercialsRepository;
    @Autowired
    private ClientDetailsRepository clientDetailsRepository;
    @Autowired
    private UsersRepository usersRepository;

    @Override
    public ApiResponse<Object> createCommercial(CreateCommercialDTO createCommercialDTO, HttpServletRequest request, String programUrl) {

        validateDTO(createCommercialDTO);
        String authorization = request.getHeader("Authorization");
        final String username = ((new String(Base64.getDecoder().decode((authorization.replaceAll("Basic ", ""))))).split(":"))[0];

       /* Program program = programRepository.findByProgramHashId(createCommercialDTO.getProgramHashId());
        if (program == null) {
            throw new BadRequestException(BizErrors.BAD_REQUEST_ERROR.getValue(), "No program found with given program hash Id");
        }*/

        Commercials existingCommercial = commercialsRepository.findByClientId(Long.parseLong(createCommercialDTO.getClientId()));
        if (Objects.nonNull(existingCommercial)) {
            throw new BadRequestException(BizErrors.APPLICATION_ERROR.getValue(), "Commercial Already Exists with given Program hash id");
        }
        //program is not stored in ZOKUDO APPLICATION
        Program program = null;
        Commercials commercialEntity = createCommercialEntity(createCommercialDTO, program, username);
        Optional<ClientDetails> optional = clientDetailsRepository.findById(Long.parseLong(createCommercialDTO.getClientId()));
        if (optional.isPresent()) {
        	ClientDetails clientDetails = optional.get();
        	clientDetails.setOnboardingState(OnboardingState.COMMERCIAL);
            clientDetailsRepository.save(clientDetails);
        } else {
            throw new BadRequestException(BizErrors.NULL_ERROR.getValue(), "ClientDetails not found for email: " + username);
        }

        CommercialResponseDTO commercialResponseDTO = new CommercialResponseDTO();
        commercialResponseDTO.setCommercialHashId(commercialEntity.getHashId());

        Users loggedInUser = getLoggedInUser(request, usersRepository, programUrl);
       // ClientDetails createdUser = getClientDetailsFromProgram(program);
        //sendEmailToManagement(loggedInUser, createdUser.getUsers(), createdUser, program, commercialEntity);

        return ApiResponse.builder()
                .code("200")
                .message("Success")
                .body(commercialResponseDTO)
                .build();

    }

    private ClientDetails getClientDetailsFromProgram(Program program) {

        Optional<UsersHasProgram> optionalUsersHasProgram = program.getUsersPrograms().stream()
                .filter(usersHasProgram -> usersHasProgram.getProgram().equals(program))
                .findFirst();
        if (optionalUsersHasProgram.isPresent()) {
            Users users = optionalUsersHasProgram.get().getUsers();
            ClientDetails clientDetails = clientDetailsRepository.findByUsers(users);
            if (Objects.nonNull(clientDetails)) {
                return clientDetails;
            } else {
                throw new BizException(BizErrors.APPLICATION_ERROR.getValue(), "ClientDetails not found from the Program");
            }
        } else {
            throw new BizException(BizErrors.APPLICATION_ERROR.getValue(), "UsersHasProgram not found from the Program");
        }
    }

    private Commercials createCommercialEntity(CreateCommercialDTO createCommercialDTO, Program program, String username) {
        Commercials commercial = new Commercials();
        commercial.setCreatedBy(username);
        commercial.setOneTimeFee(Float.parseFloat(createCommercialDTO.getOneTimeFee()));
        commercial.setFixed(Boolean.parseBoolean(createCommercialDTO.getFixed()));
        commercial.setRevenueSharing(commercial.isFixed() ? createCommercialDTO.getRevenueSharing() : createCommercialDTO.getRevenueSharing() + " %");
        commercial.setMonthlyMaintenance(Float.parseFloat(createCommercialDTO.getMonthlyMaintenance()));
        commercial.setPhysicalCardCost(Double.parseDouble(createCommercialDTO.getPhysicalCardCost()));
        commercial.setVirtualCardCost(Double.parseDouble(createCommercialDTO.getVirtualCardCost()));
        commercial.setRemarks(createCommercialDTO.getRemarks());
        commercial.setProgram(null);
        commercial.setOtp(CommonUtil.generateSixDigitNumericString());
        commercial.setHashId(Constants.COMMERCIAL + CommonUtil.getRandomString(6).toUpperCase());
        commercial.setStatus(Status.INACTIVE);
        commercial.setCommercialDocUrl(uploadImage(createCommercialDTO.getCommercialDocument(), commercial.getHashId() + S3FilePostfix.COMMERCIAL_DOCUMENT, CLINET_DOCUMENT_DIR));
        commercial.setClientId(Long.parseLong(createCommercialDTO.getClientId()));
        commercialsRepository.save(commercial);
        return commercial;
    }

    private void validateDTO(CreateCommercialDTO createCommercialDTO) {

       /* if (StringUtils.isBlank(createCommercialDTO.getFixed()) ||
                (!createCommercialDTO.getFixed().equalsIgnoreCase("TRUE") & !createCommercialDTO.getFixed().equalsIgnoreCase("FALSE"))) {
            throw new BadRequestException(BizErrors.BAD_REQUEST_ERROR.getValue(), "Invalid fixed value");
        }*/
        if (StringUtils.isBlank(createCommercialDTO.getOneTimeFee()) || !CommonUtil.isFloatString(createCommercialDTO.getOneTimeFee())) {
            throw new BadRequestException(BizErrors.BAD_REQUEST_ERROR.getValue(), "Invalid oneTimeFee value");
        }
        if (StringUtils.isBlank(createCommercialDTO.getRevenueSharing()) || !CommonUtil.isFloatString(createCommercialDTO.getRevenueSharing())) {
            throw new BadRequestException(BizErrors.BAD_REQUEST_ERROR.getValue(), "Invalid revenueSharing value");
        }
        if (StringUtils.isBlank(createCommercialDTO.getMonthlyMaintenance()) || !CommonUtil.isFloatString(createCommercialDTO.getMonthlyMaintenance())) {
            throw new BadRequestException(BizErrors.BAD_REQUEST_ERROR.getValue(), "Invalid monthlyMaintenance value");
        }
        if (StringUtils.isBlank(createCommercialDTO.getPhysicalCardCost()) || !CommonUtil.isFloatString(createCommercialDTO.getPhysicalCardCost())) {
            throw new BadRequestException(BizErrors.BAD_REQUEST_ERROR.getValue(), "Invalid PhysicalCardCost value");
        }
        if (StringUtils.isBlank(createCommercialDTO.getVirtualCardCost()) || !CommonUtil.isFloatString(createCommercialDTO.getVirtualCardCost())) {
            throw new BadRequestException(BizErrors.BAD_REQUEST_ERROR.getValue(), "Invalid VirtualCardCost value");
        }
/*        if (StringUtils.isBlank(createCommercialDTO.getRemarks())) {
            throw new BadRequestException(BizErrors.BAD_REQUEST_ERROR.getValue(), "Invalid remarks value");
        }
         if (StringUtils.isBlank(createCommercialDTO.getProgramHashId())) {
            throw new BadRequestException(BizErrors.BAD_REQUEST_ERROR.getValue(), "programHashId is Required");
        }
        *
        */
        if(StringUtils.isNotBlank(createCommercialDTO.getRemarks())){
            createCommercialDTO.setRemarks(getString(createCommercialDTO.getRemarks()));
        }
       
        if (Objects.nonNull(createCommercialDTO.getCommercialDocument()) && createCommercialDTO.getCommercialDocument().getSize() > DEFAULT_FILE_SIZE_IN_MB) {
            throw new BadRequestException(BizErrors.BAD_REQUEST_ERROR.getValue(), "Exceeded Max file size for commercialDocument");
        }
    }

    private void validateEditCommercialDTO(CreateCommercialDTO createCommercialDTO) {

        if (StringUtils.isNotBlank(createCommercialDTO.getFixed()) &&
                (!createCommercialDTO.getFixed().equalsIgnoreCase("TRUE") & !createCommercialDTO.getFixed().equalsIgnoreCase("FALSE"))) {
            throw new BadRequestException(BizErrors.BAD_REQUEST_ERROR.getValue(), "Invalid fixed value");
        }
        if (StringUtils.isNotBlank(createCommercialDTO.getOneTimeFee()) && !CommonUtil.isFloatString(createCommercialDTO.getOneTimeFee())) {
            throw new BadRequestException(BizErrors.BAD_REQUEST_ERROR.getValue(), "Invalid oneTimeFee value");
        }
        if (StringUtils.isNotBlank(createCommercialDTO.getRevenueSharing()) && !CommonUtil.isFloatString(createCommercialDTO.getRevenueSharing())) {
            throw new BadRequestException(BizErrors.BAD_REQUEST_ERROR.getValue(), "Invalid revenueSharing value");
        }
        if (StringUtils.isNotBlank(createCommercialDTO.getMonthlyMaintenance()) && !CommonUtil.isFloatString(createCommercialDTO.getMonthlyMaintenance())) {
            throw new BadRequestException(BizErrors.BAD_REQUEST_ERROR.getValue(), "Invalid monthlyMaintenance value");
        }
        if (StringUtils.isNotBlank(createCommercialDTO.getPhysicalCardCost()) && !CommonUtil.isFloatString(createCommercialDTO.getPhysicalCardCost())) {
            throw new BadRequestException(BizErrors.BAD_REQUEST_ERROR.getValue(), "Invalid PhysicalCardCost value");
        }
        if (StringUtils.isNotBlank(createCommercialDTO.getVirtualCardCost()) && !CommonUtil.isFloatString(createCommercialDTO.getVirtualCardCost())) {
            throw new BadRequestException(BizErrors.BAD_REQUEST_ERROR.getValue(), "Invalid VirtualCardCost value");
        }
        if (StringUtils.isBlank(createCommercialDTO.getRemarks())) {
            throw new BadRequestException(BizErrors.BAD_REQUEST_ERROR.getValue(), "Invalid remarks value");
        }

        if (Objects.nonNull(createCommercialDTO.getCommercialDocument()) && createCommercialDTO.getCommercialDocument().getSize() > DEFAULT_FILE_SIZE_IN_MB) {
            throw new BadRequestException(BizErrors.BAD_REQUEST_ERROR.getValue(), "Exceeded Max file size for commercialDocument");
        }
    }

    private void sendEmailToManagement(Users loggedInUser, Users cretedUser, ClientDetails clientDetailOfCretedUser, Program program, Commercials commercials) {
        String body = constructBodyOfEmail(loggedInUser, cretedUser, clientDetailOfCretedUser, program, commercials);
        sendFromGMail(Constants.EMAIL_ID_FOR_ACTIVATION_AND_OTP, Constants.EMAIL_ID_PASSWORD_FOR_ACTIVATION_AND_OTP
                , Constants.RECIPIENTS_FOR_OTP_ACTIVATION, Constants.SUBJECT_FOR_OTP_ACTIVATION.replace("{CLIENT_ NAME}", clientDetailOfCretedUser.getEntityName()), body);
    }

    private String constructBodyOfEmail(Users loggedInUser, Users createdUser, ClientDetails clientDetailOfCretedUser, Program program, Commercials commercials) {

        StringBuffer header = new StringBuffer(
                "<!DOCTYPE html>\n" +
                        "<html>\n" +
                        "<head>\n" +
                        "<style>\n" +
                        "table {\n" +
                        "  font-family: arial, sans-serif;\n" +
                        "  border-collapse: collapse;\n" +
                        "  width: 100%;\n" +
                        "}\n" +
                        "\n" +
                        "td, th {\n" +
                        "  border: 1px solid #dddddd;\n" +
                        "  text-align: left;\n" +
                        "  padding: 8px;\n" +
                        "}\n" +
                        "\n" +
                        "tr:nth-child(even) {\n" +
                        "  background-color: #dddddd;\n" +
                        "}\n" +
                        "</style>\n" +
                        "</head>\n" +
                        "<body>\n" +
                        "Dear Admin,<br>" +
                        "This is a verification mail to on-board " + clientDetailOfCretedUser.getEntityName() + " onto autobots 2.0\n" +
                        "<br>OTP: " + commercials.getOtp() +
                        "<br><table>\n" +
                        "  <tr>\n" +
                        "    <th>Column Name</th>\n" +
                        "    <th>Value</th>\n" +
                        "  </tr>\n"
        );

        header.append("<tr><td>Created By</td>" + loggedInUser.getEmail() + "<td></td></tr>");
        header.append("<tr><td>Email Id</td>" + createdUser.getEmail() + "<td></td></tr>");
        header.append("<tr><td>Mobile</td>" + createdUser.getMobile() + "<td></td></tr>");
        header.append("<tr><td>Address</td>" + clientDetailOfCretedUser.getAddress() + "<td></td></tr>");
        header.append("<tr><td>Citizenship</td>" + clientDetailOfCretedUser.getCitizenship() + "<td></td></tr>");
        header.append("<tr><td>POC Email</td>" + clientDetailOfCretedUser.getPocEmail() + "<td></td></tr>");
        header.append("<tr><td>POC Mobile</td>" + clientDetailOfCretedUser.getPocMobile() + "<td></td></tr>");
        header.append("<tr><td>Board Resolution Form Signed</td>" + clientDetailOfCretedUser.getBoardResolutionFormSigned() + "<td></td></tr>");
        header.append("<tr><td>Company Pan</td>" + clientDetailOfCretedUser.getCompanyPan() + "<td></td></tr>");
        header.append("<tr><td>Copy Of Director Front</td>" + clientDetailOfCretedUser.getCopyOfDirectorFront() + "<td></td></tr>");
        header.append("<tr><td>Copy Of Director Back</td>" + clientDetailOfCretedUser.getCopyOfDirectorBack() + "<td></td></tr>");
        header.append("<tr><td>Director Address Proof Front</td>" + clientDetailOfCretedUser.getDirectorAddressProofFront() + "<td></td></tr>");
        header.append("<tr><td>Director Address Proof Back</td>" + clientDetailOfCretedUser.getDirectorAddressProofBack() + "<td></td></tr>");
        header.append("<tr><td>GST Certificate</td>" + clientDetailOfCretedUser.getGstCertificate() + "<td></td></tr>");
        header.append("<tr><td>Incorporation Certificate</td>" + clientDetailOfCretedUser.getIncorporationCertificate() + "<td></td></tr>");
        header.append("<tr><td>Program Onboarding Plan</td>" + clientDetailOfCretedUser.getProgramOnboardingPlan() + "<td></td></tr>");
        header.append("<tr><td>Project Commitment</td>" + clientDetailOfCretedUser.getProjectionCommitment() + "<td></td></tr>");
        header.append("<tr><td>Program Name</td>" + program.getProgramName() + "<td></td></tr>");
        header.append("<tr><td>Program Currency</td>" + program.getCurrency() + "<td></td></tr>");
        header.append("<tr><td>Program Logo</td>" + program.getProgramLogo() + "<td></td></tr>");
        header.append("<tr><td>Card Image</td>" + program.getCardImageUrl() + "<td></td></tr>");
        header.append("<tr><td>Corporate Processor Name</td>" + program.getCorporateProcessor().getName() + "<td></td></tr>");
        header.append("<tr><td>Wallet Required</td>" + program.isWalletRequired() + "<td></td></tr>");
        header.append("<tr><td>Collateral</td>" + program.getCollaterals() + "<td></td></tr>");
        header.append("<tr><td>Embossing</td>" + program.getEmbossing() + "<td></td></tr>");
        header.append("<tr><td>Physical Card Cost</td>" + commercials.getPhysicalCardCost() + "<td></td></tr>");
        header.append("<tr><td>Virtual Card Cost</td>" + commercials.getVirtualCardCost() + "<td></td></tr>");
        header.append("<tr><td>Monthly Maintenance</td>" + commercials.getMonthlyMaintenance() + "<td></td></tr>");
        header.append("<tr><td>One time Fee</td>" + commercials.getOneTimeFee() + "<td></td></tr>");
        header.append("<tr><td>Revenue Sharing</td>" + commercials.getRevenueSharing() + "<td></td></tr>");
        header.append("<tr><td>Remarks</td>" + commercials.getRemarks() + "<td></td></tr>");
        header.append("<tr><td>Commercial Document</td>" + commercials.getCommercialDocUrl() + "<td></td></tr>");
        header.append("</table><br>");
        header.append("</body></html>");
        return header.toString();
    }

    private static void sendFromGMail(String from, String pass, String[] to, String subject, String body) {
        Properties props = System.getProperties();
        String host = "smtp.gmail.com";
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.smtp.host", host);
        props.put("mail.smtp.user", from);
        props.put("mail.smtp.password", pass);
        props.put("mail.smtp.port", "587");
        props.put("mail.smtp.auth", "true");

        Session session = Session.getInstance(props);
        MimeMessage message = new MimeMessage(session);

        try {
            message.setFrom(new InternetAddress(from));
            InternetAddress[] toAddress = new InternetAddress[to.length];

            // To get the array of addresses
            for (int i = 0; i < to.length; i++) {
                toAddress[i] = new InternetAddress(to[i]);
            }

            for (int i = 0; i < toAddress.length; i++) {
                message.addRecipient(Message.RecipientType.TO, toAddress[i]);
            }

            message.setSubject(subject);
            message.setText(body, "UTF-8", "html");
            Transport transport = session.getTransport("smtp");
            transport.connect(host, from, pass);
            transport.sendMessage(message, message.getAllRecipients());
            transport.close();
        } catch (AddressException ae) {
            ae.printStackTrace();
        } catch (MessagingException me) {
            me.printStackTrace();
        }

    }


    @Override
    public Object getCommercialList(String programUrl, String role, String page, String size,FilterCommercialListDto dto) {
    	
    	try {

            Page<Commercials> corporateProcessors;
            Sort sort = new Sort(Sort.Direction.DESC, "createdAt");
            Pageable pageable = PageRequest.of(Integer.parseInt(page), Integer.parseInt(size), sort);
            if (dto.getClientCode()!= null) {
            	Specification<Commercials> specification = getCommercialSpecification(dto);
                corporateProcessors = commercialsRepository.findAll(specification, pageable);
                return corporateProcessors;
                
            } else {
                String[] dateArray = dto.getDateRange().split("-");
                String startDateStr = CommonUtil.dateTimeFromatter.format(CommonUtil.dateFormatterSlashWithTime.parse(dateArray[0] + CommonUtil.startTime));
                String endDateStr = CommonUtil.dateTimeFromatter.format(CommonUtil.dateFormatterSlashWithTime.parse(dateArray[1] + CommonUtil.endTime));
                Date startDate = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").parse(startDateStr);
                Date endDate = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").parse(endDateStr);
                corporateProcessors = commercialsRepository.findAllByCreatedAtBetween(startDate, endDate, pageable);
                return corporateProcessors;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    @Override
    public ApiResponse<Object> updateCommercial(CreateCommercialDTO createCommercialDTO, HttpServletRequest request, String programUrl) {
        try {
            validateEditCommercialDTO(createCommercialDTO);
            Commercials commercial = commercialsRepository.findByHashId(createCommercialDTO.getHashId());
            if (commercial.getStatus().getValue().equalsIgnoreCase(Status.ACTIVE.getValue())) {
                throw new BadRequestException(BizErrors.BAD_REQUEST_ERROR.getValue(), "Commercial can only be edited before Activation");
            }
            commercial.setOneTimeFee(Float.parseFloat(createCommercialDTO.getOneTimeFee()));
            commercial.setFixed(Boolean.parseBoolean(createCommercialDTO.getFixed()));
            commercial.setRevenueSharing(commercial.isFixed() ? createCommercialDTO.getRevenueSharing() : createCommercialDTO.getRevenueSharing() + " %");
            commercial.setMonthlyMaintenance(Float.parseFloat(createCommercialDTO.getMonthlyMaintenance()));
            commercial.setPhysicalCardCost(Double.parseDouble(createCommercialDTO.getPhysicalCardCost()));
            commercial.setVirtualCardCost(Double.parseDouble(createCommercialDTO.getVirtualCardCost()));
            commercial.setRemarks(createCommercialDTO.getRemarks());
            if (Objects.nonNull(createCommercialDTO.getCommercialDocument()))
                commercial.setCommercialDocUrl(uploadImage(createCommercialDTO.getCommercialDocument(), createCommercialDTO.getHashId() + S3FilePostfix.COMMERCIAL_DOCUMENT, CLINET_DOCUMENT_DIR));
            commercialsRepository.save(commercial);
            return ApiResponse.builder().body(true).build();
        } catch (Exception e) {
            e.printStackTrace();
            throw new BizException("500", BizErrors.APPLICATION_ERROR.getValue(), e);
        }
    }

    @Override
    public Object getCommercialByHashId(String programUrl, String role, String hashId) {
        try {
            Commercials commercials = commercialsRepository.findByHashId(hashId);
            Map<String, Commercials> map = new HashMap<>();
            map.put("commercialDetails", commercials);
            return map;
        } catch (Exception e) {
            throw new BadRequestException(BizErrors.BAD_REQUEST_ERROR.getValue(), "failed to get the Commercial");
        }
    }

    @Override
    public ApiResponse getCommercialByProgramId(String programUrl, String program_id) {

        Commercials commercials = commercialsRepository.findByProgramId(Long.parseLong(program_id));

        if (commercials == null)
            throw new BadRequestException(BizErrors.BAD_REQUEST_ERROR.getValue(), "commercial details not found for this program");

        return ApiResponse.builder().body(commercials).build();
    }
    
    private Specification<Commercials> getCommercialSpecification(FilterCommercialListDto requestBody) {

        Specification<Commercials> specification =
                (corporateProcessorRoot, criteriaQuery, criteriaBuilder) -> criteriaBuilder.equal(criteriaBuilder.literal(1), 1);
        try {
            if (requestBody.getClientCode() != null) {
                specification = specification.and((root, query, criteriaBuilder) ->
                        criteriaBuilder.equal(root.get("clientId"), requestBody.getClientCode())
                );
            }
          /*  if(!StringUtils.isBlank(requestBody.getCommercialStatus())) {
                Status cardStatus = Status.valueOf(requestBody.getCommercialStatus());
                specification = specification.and((Cards, mq, mb) -> mb.equal(Cards.get("cardStatus"), cardStatus));
            }*/
        } catch (Exception e) {
            throw new BizException(BizErrors.BAD_REQUEST_ERROR.getValue(), "Internal server error. Please try again after sometimes");
        }
        return specification;
    }
    
    
}