package com.cards.zokudo.services.impl;

import com.cards.zokudo.dto.request.AgentDTO;
import com.cards.zokudo.dto.response.AgentAndDistDetailsResponseDTO;
import com.cards.zokudo.entities.*;
import com.cards.zokudo.enums.*;
import com.cards.zokudo.exceptions.BadRequestException;
import com.cards.zokudo.exceptions.BizException;
import com.cards.zokudo.repositories.*;
import com.cards.zokudo.response.ApiResponse;
import com.cards.zokudo.services.AgentInf;
import com.cards.zokudo.services.WalletService;
import com.cards.zokudo.services.program.persist.ProgramImpl;
import com.cards.zokudo.util.CommonUtil;
import com.cards.zokudo.util.Constants;
import com.cards.zokudo.util.CustomBcryptPasswordEncoder;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.apache.commons.validator.routines.EmailValidator;
import org.glassfish.jersey.internal.guava.Sets;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.text.ParseException;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import static com.cards.zokudo.util.CommonUtil.*;

@Slf4j
@Service
public class AgentImpl implements AgentInf {

    private final ProgramRepository programRepository;
    private final RoleRepository roleRepository;
    private final UsersRepository usersRepository;
    private final AgentDetailsRepository agentDetailsRepository;
    private final AccountDetailsRepository accountDetailsRepository;
    private final UsersHasProgramRepository usersHasProgramRepository;
    private final WalletService walletService;
    private final ProgramImpl programImpl;
    private final DistributorRepository distributorRepository;

    @Autowired
    public AgentImpl(ProgramRepository programRepository,
                     RoleRepository roleRepository,
                     UsersRepository usersRepository,
                     AgentDetailsRepository agentDetailsRepository,
                     AccountDetailsRepository accountDetailsRepository,
                     UsersHasProgramRepository usersHasProgramRepository,
                     WalletService walletService,
                     ProgramImpl programImpl,
                     DistributorRepository distributorRepository) {
        this.programRepository = programRepository;
        this.roleRepository = roleRepository;
        this.usersRepository = usersRepository;
        this.agentDetailsRepository = agentDetailsRepository;
        this.accountDetailsRepository = accountDetailsRepository;
        this.usersHasProgramRepository = usersHasProgramRepository;
        this.walletService = walletService;
        this.programImpl = programImpl;
        this.distributorRepository = distributorRepository;
    }

    @Override
    public ApiResponse onBoardAgent(AgentDTO agentDTO, HttpServletRequest request, String programUrl) {

        Program programDetails = programRepository.findByHostUrl(programUrl);
        // validation process

        validateAgent(agentDTO, programUrl, request.getHeader("loggedInUserHashId"));

        Users users = registerAgent(agentDTO);

        AgentDetails agent = saveAgentDetails(users, agentDTO);

        openAgentAccount(users);

        boolean b = assignRoles(users);

        assignPrograms(users, agentDTO.getProgram());
        log.info("Add default commission for retailer.");
        walletService.addDefaultRetailerComm(agent.getId(), programDetails.getId(), programUrl);
        log.info("Commission default added for retailer.");

        /*Add Default Retailer Discount*/
        walletService.addDefaultRetailerDiscount(agent.getId(), programDetails.getId(), programUrl);

        log.info("Create Default Agent(Retailer) Accounts for AgentId :{}", agent.getId());
        walletService.createAgentAccount(agent.getId(), programDetails.getClientId(), programUrl);
        log.info("After Creating accounts for retailer");

        return addAdminResponse(b, agentDTO, programUrl);

    }

    private ApiResponse addAdminResponse(boolean b, AgentDTO agentDTO, String programUrl) {

        if (!b)
            return ApiResponse.builder().body("Agent created! we are facing issues in role assignment.").build();

        sendAgentEmailWithCredentials(agentDTO, programUrl);

        return ApiResponse.builder().body("Agent created successfully!").build();
    }

    private void sendAgentEmailWithCredentials(AgentDTO agentDTO, String programUrl) {
        String[] to = {agentDTO.getEmail()};
        CommonUtil.sendFromGMail(Constants.SET_FROM, Constants.EMAIL_ID_PASSWORD_FOR_ACTIVATION_AND_OTP, to, "Agent On-Boarding", constructAgentDetails(agentDTO, programUrl));

    }

    // need to implement email template!
    private String constructAgentDetails(AgentDTO agentDTO, String programUrl) {
        StringBuffer header = new StringBuffer(
                "<!DOCTYPE html>\n" +
                        "<html>\n" +
                        "<head>\n" +
                        "</head>\n" +
                        "<body>\n" +
                        "Dear " + agentDTO.getFullName() + ",<br>" +
                        "You have onboarded as an Agent in zokudo System. Please find the Login details below.<br>" +
                        "Username : " + agentDTO.getEmail() + "<br>" +
                        "Password : " + agentDTO.getPassword() + "<br>" +
                        "Host URL : https://admin.zokudo.com/zokudo-ui/" + programUrl + "/auth/program" +
                        "Regards,<br>" +
                        "Team Zokudo.\n" +
                        "</body></html>"
        );
        return header.toString();
    }

    private void assignPrograms(Users users, Program program) {
        try {
            UsersHasProgram usersHasProgram = new UsersHasProgram();
            usersHasProgram.setUsers(users);
            usersHasProgram.setProgram(program);
            usersHasProgram.setStatus(Status.ACTIVE);
            usersHasProgramRepository.save(usersHasProgram);

        } catch (Exception e) {
            e.printStackTrace();
            throw new BizException(BizErrors.APPLICATION_ERROR.getValue(), "error!while assigning program");
        }
    }

    private boolean assignRoles(Users users) {

        try {
            Role role = roleRepository.findByName(String.valueOf(Roles.ROLE_AGENT));
            Set roleSet = Sets.newHashSet();
            roleSet.add(role);
            users.setUserRoles(roleSet);
            usersRepository.save(users);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    private void openAgentAccount(Users users) {

        // agent account should open in wallet service

        //        try {
//            AccountDetails accountDetails = new AccountDetails();
//            accountDetails.setStatus(Status.ACTIVE);
//            accountDetails.setBalance(0.0);
//            accountDetailsRepository.save(accountDetails);
//        } catch (Exception e) {
//            throw new BizException(BizErrors.APPLICATION_ERROR.getValue(), "error! while creating new account for agent.");
//        }

    }

    private AgentDetails saveAgentDetails(Users users, AgentDTO dto) {
        try {
            log.info("Updating distributor details");
            AgentDetails agentDetails = new AgentDetails();
            agentDetails.setFullName(dto.getFullName());
            agentDetails.setAddress(dto.getAddress());
            agentDetails.setEmail(dto.getEmail());
            agentDetails.setCompanyName(dto.getCompanyName());
            agentDetails.setMobile(dto.getMobile());
            agentDetails.setUserHashId(users.getUserHashId());
            agentDetails.setCreatedBy(users.getCreatedBy());
            agentDetails.setAddress(dto.getAddress());
            agentDetails.setNameOfDirector(dto.getNameOfDirector());
            agentDetails.setCitizenship(dto.getCitizenship());
            agentDetails.setDobOfDirector(parseDate(dto.getDobOfDirector()));
            agentDetails.setProgramHashId(dto.getProgram().getProgramHashId());
            agentDetails.setProgramHashId(dto.getProgram().getProgramHashId());
            agentDetails.setDistributorHashId(dto.getLoggedInUserHashId());
            agentDetails.setDirectorAddressProofFront(uploadImage(dto.getDirectorAddressProofFront(), users.getUserHashId() + S3FilePostfix.DIRECTOR_ADDRESS_FRONT, Constants.AGENT_DOCUMENT_DIR));
            agentDetails.setDirectorAddressProofBack(uploadImage(dto.getDirectorAddressProofBack(), users.getUserHashId() + S3FilePostfix.DIRECTOR_ADDRESS_FRONT, Constants.AGENT_DOCUMENT_DIR));
            agentDetails.setGstCertificate(uploadImage(dto.getGstCertificate(), users.getUserHashId() + S3FilePostfix.GST, Constants.AGENT_DOCUMENT_DIR));
            agentDetails.setCompanyPan(uploadImage(dto.getCompanyPan(), users.getUserHashId() + S3FilePostfix.COMPANY_PAN, Constants.AGENT_DOCUMENT_DIR));
            agentDetails.setStatus(Status.ACTIVE);
            agentDetailsRepository.save(agentDetails);
            return agentDetails;
        } catch (ParseException e) {
            log.error(ExceptionUtils.getStackTrace(e));
            usersRepository.delete(users);
            throw new BizException(BizErrors.BAD_REQUEST_ERROR.getValue(), "Invalid dobOfDirector");
        } catch (Exception e) {
            log.error(ExceptionUtils.getStackTrace(e));
            usersRepository.delete(users);
            throw new BizException(BizErrors.BAD_REQUEST_ERROR.getValue(), "failed to update the distributor details");
        }
    }

    @Override
    public Object agentList(AgentDTO dto, String role,String programUrl) {
        Page<AgentDetails> agentDetails = null;
        try {
            Program program = programRepository.findByHostUrl(programUrl);
            Date startDate = null, endDate = null;
            if (!StringUtils.isBlank(dto.getDateRage())) {
                String[] dateArray = dto.getDateRage().split("-");

                // need to use the specific date time formatter coming from frontend

                startDate = CommonUtil.dateFormatterSlashWithTime.parse(dateArray[0] + CommonUtil.startTime);
                endDate = CommonUtil.dateFormatterSlashWithTime.parse(dateArray[1] + CommonUtil.endTime);
            }
            Sort sort = new Sort(Sort.Direction.DESC, "createdAt");
            Pageable pageable = PageRequest.of(Integer.parseInt(dto.getPage()), Integer.parseInt(dto.getSize()), sort);
            agentDetails = agentDetailsRepository.findAll(agentSpecificationBasedOnFilter(dto,program.getProgramHashId(), role, startDate, endDate), pageable);

        } catch (Exception e) {
            log.error(e.getMessage());
        }
        return agentDetails;
    }


    private Specification<AgentDetails> agentSpecificationBasedOnFilter(AgentDTO dto, String programHashId, String role, Date startDate, Date endDate) {
        Specification<AgentDetails> spec = null;

        try {
            spec = (distDetails, mq, mb) -> mb.equal(mb.literal(1), 1);
            if (startDate != null) {
                spec = spec.and((distDetails, cq, cb) -> cb.between(distDetails.get("createdAt"), startDate, endDate));
            }
            if (Roles.ROLE_CLIENT_DIST.toString().equals(role)) {
                spec = spec.and((distDetails, cq, cb) -> cb.equal(distDetails.get("programHashId"), programHashId));
            }
            if (Roles.ROLE_DISTRIBUTOR.toString().equals(role)) {
                if (dto.getUserHashId() != null) {
                    spec = spec.and((distDetails, cq, cb) -> cb.equal(distDetails.get("distributorHashId"), dto.getUserHashId()));
                }
            }
            if (Roles.ROLE_AGENT.toString().equals(role)) {
                if (dto.getUserHashId() != null) {
                    spec = spec.and((distDetails, cq, cb) -> cb.equal(distDetails.get("userHashId"), dto.getUserHashId()));
                }
            }
            if(StringUtils.isNotBlank(dto.getMobile())){
                spec = spec.and((distDetails, cq, cb) -> cb.equal(distDetails.get("mobile"), dto.getMobile()));
            }
            if(StringUtils.isNotBlank(dto.getEmail())){
                spec = spec.and((distDetails, cq, cb) -> cb.equal(distDetails.get("email"), dto.getEmail()));
            }
            if(StringUtils.isNotBlank(dto.getCompanyName())){
                spec = spec.and((distDetails, cq, cb) -> cb.equal(distDetails.get("companyName"), dto.getCompanyName()));
            }

        } catch (Exception e) {
            throw new BizException(BizErrors.APPLICATION_ERROR.getValue(), "Internal server error. Please try again after sometimes");
        }
        return spec;
    }



    public Object agentListforSuperDistriButor(String loggedInUserHashId, String role, String dateRange, String page, String size, String programUrl) {
        List<Object> clientIdList = programImpl.getListOfDistributorProgramIds();
        Page<AgentDetails> agentDetails = null;
        try {
            Program program = programRepository.findByHostUrl(programUrl);
            Date startDate = null, endDate = null;
            if (!StringUtils.isBlank(dateRange)) {
                String[] dateArray = dateRange.split("-");

                // need to use the specific date time formatter coming from frontend

                startDate = CommonUtil.dateFormatterSlashWithTime.parse(dateArray[0] + CommonUtil.startTime);
                endDate = CommonUtil.dateFormatterSlashWithTime.parse(dateArray[1] + CommonUtil.endTime);
            }
            Sort sort = new Sort(Sort.Direction.DESC, "createdAt");
            Pageable pageable = PageRequest.of(Integer.parseInt(page), Integer.parseInt(size), sort);
            agentDetails = agentDetailsRepository.findAll(agentSpecification(loggedInUserHashId, program.getProgramHashId(), role, startDate, endDate), pageable);

        } catch (Exception e) {
            log.error(e.getMessage());
        }
        return agentDetails;
    }

    @Override
    public Object executeBlockUnblock(AgentDTO agentDto) {

        AgentDetails agentDetails = agentDetailsRepository.findByUserHashId(agentDto.getUserHashId());
        if (agentDetails == null)
            throw new BizException(BizErrors.BAD_REQUEST_ERROR.getValue(), "invalid user hash  Id");

        agentDetails.setStatus(Status.getEnum(agentDto.getActionType()));
        agentDetailsRepository.save(agentDetails);

        Users users = usersRepository.findByUserHashId(agentDetails.getUserHashId());
        users.setStatus(Status.getEnum(agentDto.getActionType()));
        users.setBlocked(Status.BLOCKED.equals(users.getStatus()) ? true : false);
        usersRepository.save(users);
        return ApiResponse.builder().body(true).build();
    }

    private Specification<AgentDetails> agentSpecification(String loggedInUserHashId, String programHashId, String role, Date startDate, Date endDate) {
        Specification<AgentDetails> spec = null;

        try {
            spec = (distDetails, mq, mb) -> mb.equal(mb.literal(1), 1);
            if (startDate != null) {
                spec = spec.and((distDetails, cq, cb) -> cb.between(distDetails.get("createdAt"), startDate, endDate));
            }
            if (Roles.ROLE_CLIENT_DIST.toString().equals(role)) {
                spec = spec.and((distDetails, cq, cb) -> cb.equal(distDetails.get("programHashId"), programHashId));
            }
            if (Roles.ROLE_DISTRIBUTOR.toString().equals(role)) {
                if (loggedInUserHashId != null) {
                    spec = spec.and((distDetails, cq, cb) -> cb.equal(distDetails.get("distributorHashId"), loggedInUserHashId));
                }
            }
            if (Roles.ROLE_AGENT.toString().equals(role)) {
                if (loggedInUserHashId != null) {
                    spec = spec.and((distDetails, cq, cb) -> cb.equal(distDetails.get("userHashId"), loggedInUserHashId));
                }
            }
        } catch (Exception e) {
            throw new BizException(BizErrors.APPLICATION_ERROR.getValue(), "Internal server error. Please try again after sometimes");
        }
        return spec;
    }

    private void validateAgent(AgentDTO dto, String programUrl, String loggedInUserHashId) {

        if (StringUtils.isBlank(dto.getAddress()))
            throw new BizException(BizErrors.APPLICATION_ERROR.getValue(), "address should not be empty");

        if (StringUtils.isBlank(dto.getEmail()))
            throw new BizException(BizErrors.APPLICATION_ERROR.getValue(), "email should not be empty");

        if (!EmailValidator.getInstance().isValid(dto.getEmail()))
            throw new BizException(BizErrors.APPLICATION_ERROR.getValue(), "enter valid email id.");


        if (StringUtils.isBlank(dto.getCompanyName()))
            throw new BizException(BizErrors.APPLICATION_ERROR.getValue(), "company name should not be empty");

        if (StringUtils.isBlank(dto.getMobile()))
            throw new BizException(BizErrors.APPLICATION_ERROR.getValue(), "mobile number should not be empty");


        if (!CommonUtil.isValidMobileNumber(dto.getMobile()))
            throw new BizException(BizErrors.INVALID_VALUE.getValue(), "enter valid mobile number");

        if (StringUtils.isBlank(dto.getNameOfDirector()))
            throw new BizException(BizErrors.INVALID_VALUE.getValue(), "name of director should not be empty!");

        if (StringUtils.isBlank(dto.getCitizenship()))
            throw new BizException(BizErrors.INVALID_VALUE.getValue(), "enter Citizenship!");

        if (StringUtils.isBlank(dto.getDobOfDirector()))
            throw new BizException(BizErrors.INVALID_VALUE.getValue(), "dob should not be empty!");


//        if (dto.getGstCertificate() == null)
//            throw new BizException(BizErrors.INVALID_VALUE.getValue(), "upload gst certificate");
//
//        if (dto.getCompanyPan() == null)
//            throw new BizException(BizErrors.INVALID_VALUE.getValue(), "upload pan");


        Users users = usersRepository.findByEmail(dto.getEmail());
        if (users != null) {
            throw new BizException(BizErrors.BAD_REQUEST_ERROR.getValue(), "email already exist, please use other email id");
        }

        users = usersRepository.findByMobile(dto.getMobile());
        if (users != null) {
            throw new BizException(BizErrors.BAD_REQUEST_ERROR.getValue(), "contact number already present");
        }

        AgentDetails agentDetails = agentDetailsRepository.findByCompanyName(dto.getCompanyName());
        if (agentDetails != null) {
            throw new BizException(BizErrors.BAD_REQUEST_ERROR.getValue(), "entity name already present");
        }

        //validate client
        agentDetails = agentDetailsRepository.findByEmail(dto.getEmail());
        if (agentDetails != null) {
            throw new BizException(BizErrors.BAD_REQUEST_ERROR.getValue(), "email id already present, please enter other email");
        }

        agentDetails = agentDetailsRepository.findByMobile(dto.getMobile());
        if (agentDetails != null) {
            throw new BizException(BizErrors.BAD_REQUEST_ERROR.getValue(), "contact number already present");
        }

        Program program = programRepository.findByHostUrl(programUrl);

        if (program == null || !Status.ACTIVE.equals(program.getStatus()))
            throw new BizException(BizErrors.BAD_REQUEST_ERROR.getValue(), "invalid program!");

        dto.setProgram(program);
        dto.setLoggedInUserHashId(loggedInUserHashId);

    }

    private Users registerAgent(AgentDTO agentDto) {
        try {

            Users users = new Users();
            users.setFullName(agentDto.getFullName());
            users.setMobile(agentDto.getMobile());
            users.setUsername(agentDto.getEmail());
            users.setEmail(agentDto.getEmail());
            users.setChannelId(agentDto.getLoggedInUserHashId());
            users.setUserHashId(randomString(10, "AGENT"));
            users.setMobileToken(generateSixDigitNumericString());
            users.setStatus(Status.ACTIVE);
            users.setCreatedBy(agentDto.getProgram().getCreatedBy());
            users.setOtpGenerationTime(new Date(System.currentTimeMillis()));
            users.setPassword(CustomBcryptPasswordEncoder.getBcryptPasswordEncoder().encode(agentDto.getPassword()));
            users.setProductType(UserType.AGENT);
            usersRepository.save(users);
            return users;
        } catch (Exception e) {
            throw new BizException(BizErrors.BAD_REQUEST_ERROR.getValue(), "failed to update the client in user table");
        }
    }

    @Override
    public ApiResponse<?> getAgentDetails(String agentHashId) {


        AgentDetails agentDetails = agentDetailsRepository.findByUserHashId(agentHashId);
        if (agentDetails == null)
            throw new BadRequestException(BizErrors.BAD_REQUEST_ERROR.getValue(), "could not found agent details");

        return ApiResponse.builder().body(agentDetails).build();
    }

    @Override
    public Object agentListByDsitributorHash(String role, String productType, String programUrl, String distributorHashId, String category) {
        log.info("Inside agentListByRole() values: " + programUrl + " , role" + role);
        Program program = programRepository.findByHostUrl(programUrl);
        if (Roles.ROLE_ADMIN.toString().equals(role)) {
            return agentDetailsRepository.findAll();
        } else if (Roles.ROLE_CLIENT_DIST.toString().equals(role)){
            return agentDetailsRepository.findByProgramHashId(program.getProgramHashId());
         }else if (Roles.ROLE_DISTRIBUTOR.toString().equals(role) || category != null
                || category.equals(Roles.ROLE_DISTRIBUTOR.toString())) {
            return agentDetailsRepository.findByDistributorHashId(distributorHashId);
        }
        return null;
    }

    @Override
    public Object getRetailerList(String programUrl, String role, String loginUserHashId) {
        List<AgentDetails> agentEntites = null;
        Program programDetails = programRepository.findByHostUrl(programUrl);
        log.info("** Inside getRetailerList " + programUrl);
        try {
            agentEntites = agentDetailsRepository.findAll(RetailerSpecificationForAllRoles(programDetails.getProgramHashId(), role, loginUserHashId));
            return agentEntites;
        } catch (Exception e) {
            log.error("error while fetching distributor by program hash" + e.getMessage());
            throw new BizException("Error while fetching distributor");
        }
    }

    private Specification<AgentDetails> RetailerSpecificationForAllRoles(String programHashId, String role, String loginUserHashId) {
        Specification<AgentDetails> spec = null;

        try {
            // ALL SPECS removed for heirachy purpose.
            // TODO : Need Refactoring!
           /* spec = (agentDetails, mq, mb) -> mb.equal(mb.literal(1), 1);
            if (Roles.ROLE_CLIENT_DIST.toString().equals(role)) {
                spec = spec.and((agentDetails, mq, mb) -> mb.equal(agentDetails.get("programHashId"), programHashId));
            }
            if (Roles.ROLE_DISTRIBUTOR.toString().equals(role)) {
                spec = spec.and((agentDetails, mq, mb) -> mb.equal(agentDetails.get("distributorHashId"), loginUserHashId));
            }
            if (Roles.ROLE_AGENT.toString().equals(role)) {
                spec = spec.and((agentDetails, mq, mb) -> mb.equal(agentDetails.get("userHashId"), loginUserHashId));
            }*/
        } catch (Exception e) {
            throw new BizException(BizErrors.APPLICATION_ERROR.getValue(), "Internal server error. Please try again after sometimes");
        }
        return spec;
    }

    @Override
    public Object getAgentById(String id, String programUrl) {

        if (id != null) {
            long agentId = Long.parseLong(id);

            log.info("** Fetching agent details by " + id);
            Optional<AgentDetails> agentDetails = agentDetailsRepository.findById(agentId);
            log.info("** Agent fetched : " + agentDetails);

            if (agentDetails.isPresent()) {
                log.info("** Agent Fetched: " + agentDetails.get());
                return agentDetails.get();
            } else {
                log.error("** Agent not found. ");
                throw new BadRequestException(BizErrors.BAD_REQUEST_ERROR.getValue(), "could not found agent details");
            }
        } else {
            throw new BadRequestException(BizErrors.BAD_REQUEST_ERROR.getValue(), "Invalid agent id.");
        }

    }

    @Override
    public AgentAndDistDetailsResponseDTO getAgentAndDistributorDetailsByAgentId(String id, String programUrl) {

        if (id != null) {
            long agentId = Long.parseLong(id);

            log.info("** Fetching agent details by " + id);
            AgentDetails agentDetails = agentDetailsRepository.findByAgentId(agentId);
            log.info("** Agent fetched : " + agentDetails);

            log.info("** Fetching distributor details by " + id);
            DistributorEntity distributorEntity = distributorRepository.findByUserHashId(agentDetails.getDistributorHashId());
            log.info("** distributor fetched : " + agentDetails);

            AgentAndDistDetailsResponseDTO agentAndDistDetailsResponseDTO = new AgentAndDistDetailsResponseDTO();
            agentAndDistDetailsResponseDTO.setAgentId(agentDetails.getId());
            agentAndDistDetailsResponseDTO.setAgentName(agentDetails.getFullName());
            agentAndDistDetailsResponseDTO.setDistributorId(distributorEntity.getId());
            agentAndDistDetailsResponseDTO.setDistributorName(distributorEntity.getCompanyName());

            return agentAndDistDetailsResponseDTO;

        } else {
            throw new BadRequestException(BizErrors.BAD_REQUEST_ERROR.getValue(), "Invalid agent id.");
        }

    }

    @Override
    public Object getAgentByEmailId(final String emailId) {
        log.info("*** Inside get agent by email id :{}", emailId);

        AgentDetails agentDetails = agentDetailsRepository.findByEmail(emailId);
        if (agentDetails == null) {
            log.error("*** Agent details not found for {}", emailId);
            throw new BadRequestException(BizErrors.BAD_REQUEST_ERROR.getValue(), "could not found agent details");
        }
        return agentDetails;
    }

    @Override
    public Object allRetailer() {
        List<AgentDetails> agentEntites = null;

        log.info("** Inside getRetailerListwithoutHashId " );
        try {
            agentEntites = agentDetailsRepository.findAll();
            return agentEntites;
        } catch (Exception e) {
            log.error("error while fetching all distributor " + e.getMessage());
            throw new BizException("Error while fetching distributor");
        }
    }
}
