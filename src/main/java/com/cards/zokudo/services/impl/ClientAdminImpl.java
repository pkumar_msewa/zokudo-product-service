package com.cards.zokudo.services.impl;

import com.cards.zokudo.dto.request.APIRequestDto;
import com.cards.zokudo.entities.Program;
import com.cards.zokudo.entities.Role;
import com.cards.zokudo.entities.Users;
import com.cards.zokudo.entities.UsersHasProgram;
import com.cards.zokudo.enums.BizErrors;
import com.cards.zokudo.enums.Roles;
import com.cards.zokudo.enums.Status;
import com.cards.zokudo.exceptions.BadRequestException;
import com.cards.zokudo.exceptions.BizException;
import com.cards.zokudo.repositories.ProgramRepository;
import com.cards.zokudo.repositories.RoleRepository;
import com.cards.zokudo.repositories.UsersHasProgramRepository;
import com.cards.zokudo.repositories.UsersRepository;
import com.cards.zokudo.response.ApiResponse;
import com.cards.zokudo.services.ClientAdminInf;
import com.cards.zokudo.util.CustomBcryptPasswordEncoder;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.glassfish.jersey.internal.guava.Sets;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.util.Set;
import java.util.UUID;


@Service
@Slf4j
public class ClientAdminImpl implements ClientAdminInf {


    private final UsersRepository usersRepository;
    private final UsersHasProgramRepository usersHasProgramRepository;
    private final ProgramRepository programRepository;
    private final RoleRepository roleRepository;

    @Autowired
    public ClientAdminImpl(final UsersRepository usersRepository, UsersHasProgramRepository usersHasProgramRepository, ProgramRepository programRepository,
                           RoleRepository roleRepository) {
        this.usersRepository = usersRepository;
        this.usersHasProgramRepository = usersHasProgramRepository;
        this.programRepository = programRepository;
        this.roleRepository = roleRepository; 
    }


    @Override
    public ApiResponse<Object> addClientAdmin(APIRequestDto apiRequestDto, HttpServletRequest request, String programUrl) {


        // validation
        validateClientAdminRequest(apiRequestDto);

        // implementation
        Users users = addUserForProgramLogin(apiRequestDto);

        boolean b = assignRoles(users);

        assignPrograms(users, apiRequestDto.getProgram());


        return addAdminResponse(b);
    }

    private ApiResponse<Object> addAdminResponse(boolean b) {

        if (!b)
            return ApiResponse.builder().body("client admin created! we are facing issues in role assignment.").build();

        return ApiResponse.builder().body("client admin created successfully!").build();
    }

    private boolean assignRoles(Users users) {

        try {
            Role role = roleRepository.findByName(String.valueOf(Roles.ROLE_CLIENT_ADMIN));
            Set roleSet = Sets.newHashSet();
            roleSet.add(role);
            users.setUserRoles(roleSet);
            usersRepository.save(users);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    private void assignPrograms(Users users, Program program) {
        try {
            UsersHasProgram usersHasProgram = new UsersHasProgram();
            usersHasProgram.setUsers(users);
            usersHasProgram.setProgram(program);
            usersHasProgram.setStatus(Status.ACTIVE);
            usersHasProgramRepository.save(usersHasProgram);

        } catch (Exception e) {
            e.printStackTrace();
            throw new BizException(BizErrors.APPLICATION_ERROR.getValue(), "error!while assigning program");
        }


    }

    private Users addUserForProgramLogin(APIRequestDto apiRequestDto) {

        try {
            Users users = new Users();
            users.setStatus(Status.ACTIVE);
            users.setUserRoles(null);
            users.setUserHashId(UUID.randomUUID().toString());
            users.setBlocked(false);
            users.setMobile(apiRequestDto.getMobile());
            users.setPassword(CustomBcryptPasswordEncoder.getBcryptPasswordEncoder().encode(apiRequestDto.getPassword()));
            users.setChannelId(apiRequestDto.getProgram().getProgramHashId());
            users.setEmail(apiRequestDto.getEmail());
            users.setFullName(apiRequestDto.getFirst_name());
           // users.setLastName(apiRequestDto.getLast_name());
            users.setUsername(apiRequestDto.getEmail());
            usersRepository.save(users);
            return users;
        } catch (Exception e) {
            throw new BizException(BizErrors.APPLICATION_ERROR.getValue(), "could not insert user");
        }
    }

    private void validateClientAdminRequest(APIRequestDto apiRequestDto) {

        validateParams(apiRequestDto);

        validateUserAndProgram(apiRequestDto);
    }

    private void validateUserAndProgram(APIRequestDto apiRequestDto) {

        Program program = programRepository.findByHostUrl(apiRequestDto.getHostUrl());

        if (program == null)
            throw new BadRequestException(BizErrors.BAD_REQUEST_ERROR.getValue(), "invalid program!");

        Users user = usersRepository.findByUsername(apiRequestDto.getEmail(), program.getHostUrl());
        if (user != null)
            throw new BadRequestException(BizErrors.BAD_REQUEST_ERROR.getValue(), "user already exist with this email");
        apiRequestDto.setProgram(program);

    }

    private void validateParams(APIRequestDto apiRequestDto) {

        if (StringUtils.isBlank(apiRequestDto.getFirst_name()))
            throw new BadRequestException(BizErrors.BAD_REQUEST_ERROR.getValue(), "first name should not be empty");

        if (StringUtils.isBlank(apiRequestDto.getLast_name()))
            throw new BadRequestException(BizErrors.BAD_REQUEST_ERROR.getValue(), "last name should not be empty");

        if (StringUtils.isBlank(apiRequestDto.getEmail()))
            throw new BadRequestException(BizErrors.BAD_REQUEST_ERROR.getValue(), "email should not be empty");

        if (StringUtils.isBlank(apiRequestDto.getMobile()))
            throw new BadRequestException(BizErrors.BAD_REQUEST_ERROR.getValue(), "mobile number should not be empty");

        if (StringUtils.isBlank(apiRequestDto.getPassword()))
            throw new BadRequestException(BizErrors.BAD_REQUEST_ERROR.getValue(), "password should not be empty");

        if (StringUtils.isBlank(apiRequestDto.getHostUrl()))
            throw new BadRequestException(BizErrors.BAD_REQUEST_ERROR.getValue(), "please select program");
    }
}
