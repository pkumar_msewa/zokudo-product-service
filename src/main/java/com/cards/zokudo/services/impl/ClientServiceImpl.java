package com.cards.zokudo.services.impl;

import com.cards.zokudo.dto.request.APIRequestDto;
import com.cards.zokudo.dto.request.ClientOnboardDTO;
import com.cards.zokudo.dto.request.OnboardingOtpRequestDTO;
import com.cards.zokudo.dto.response.ClientOnboardResponseDTO;
import com.cards.zokudo.dto.response.CommonServiceResponse;
import com.cards.zokudo.entities.*;
import com.cards.zokudo.enums.*;
import com.cards.zokudo.exceptions.BizException;
import com.cards.zokudo.exceptions.CustomRuntimeException;
import com.cards.zokudo.repositories.*;
import com.cards.zokudo.response.ApiResponse;
import com.cards.zokudo.services.ClientService;
import com.cards.zokudo.services.WalletService;
import com.cards.zokudo.util.AppConstants;
import com.cards.zokudo.util.CommonUtil;
import com.cards.zokudo.util.Constants;
import com.cards.zokudo.util.CustomBcryptPasswordEncoder;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.apache.commons.validator.routines.EmailValidator;
import org.codehaus.jettison.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.http.HttpStatus;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

import static com.cards.zokudo.util.CommonUtil.*;
import static com.cards.zokudo.util.Constants.DEFAULT_FILE_SIZE_IN_MB;

@Service
@Slf4j
public class ClientServiceImpl implements ClientService {

    private final UsersRepository usersRepository;
    private final ClientDetailsRepository clientDetailsRepository;
    private final ProgramRepository programRepository;
    private final RoleRepository roleRepository;
    private final CommercialsRepository commercialsRepository;
    private final WalletService walletService;
    private final AgentDetailsRepository agentDetailsRepository;

    @Autowired
    public ClientServiceImpl(final UsersRepository usersRepository,
                             final ClientDetailsRepository clientDetailsRepository,
                             final ProgramRepository programRepository,
                             final RoleRepository roleRepository,
                             final CommercialsRepository commercialsRepository,
                             final WalletService walletService,
                             final AgentDetailsRepository agentDetailsRepository) {
        this.usersRepository = usersRepository;
        this.clientDetailsRepository = clientDetailsRepository;
        this.programRepository = programRepository;
        this.roleRepository = roleRepository;
        this.commercialsRepository = commercialsRepository;
        this.walletService = walletService;
        this.agentDetailsRepository = agentDetailsRepository;
    }

    @Override
    @Transactional
    public ApiResponse<Object> addClient(ClientOnboardDTO clientOnboardDTO, HttpServletRequest request, String programUrl) {

        Program programDetails = programRepository.findByHostUrl(programUrl);

        validateFields(clientOnboardDTO);

        Validations(clientOnboardDTO, request);

        Users users = UpdateUser(clientOnboardDTO, request, programDetails);

        ClientDetails result = UpdateClient(clientOnboardDTO, users);
        log.info("Result - " + result);

        /*Save Client Hash in User table*/
        updateClientHashInUser(users, result);

        ClientOnboardResponseDTO clientOnboardResponseDTO = new ClientOnboardResponseDTO();
        clientOnboardResponseDTO.setUserHashId(users.getUserHashId());
        clientOnboardResponseDTO.setClientId(result.getId());
        log.info("ClientRespo - " + clientOnboardResponseDTO);
        ApiResponse<Object> response = ApiResponse.builder()
                .code("200")
                .message("Success")
                .body(clientOnboardResponseDTO)
                .build();

        return response;
    }

    private void validateFields(ClientOnboardDTO clientOnboardDTO) {
        if (StringUtils.isBlank(clientOnboardDTO.getEntityName()))
            throw new BizException(BizErrors.APPLICATION_ERROR.getValue(), "please enter Entity Name ");
        if (StringUtils.isBlank(clientOnboardDTO.getFirstName()))
            throw new BizException(BizErrors.APPLICATION_ERROR.getValue(), "please enter first name ");
        if (StringUtils.isBlank(clientOnboardDTO.getMobile()) || !isValidMobileNumber(clientOnboardDTO.getMobile()))
            throw new BizException(BizErrors.APPLICATION_ERROR.getValue(), "please enter proper sender Mobile number");
        /*if (StringUtils.isBlank(clientOnboardDTO.getLastName()) || !isAlpha(clientOnboardDTO.getLastName()))
            throw new BizException(BizErrors.APPLICATION_ERROR.getValue(), "please enter last name");*/
        if (StringUtils.isBlank(clientOnboardDTO.getEmail()))
            throw new BizException(BizErrors.APPLICATION_ERROR.getValue(), "please enter the email");
        if (StringUtils.isBlank(String.valueOf(clientOnboardDTO.getClientLogo())))
            throw new BizException(BizErrors.APPLICATION_ERROR.getValue(), "please enter client remarks");

        /*if (StringUtils.isBlank(clientOnboardDTO.getPocEmail()) || !EmailValidator.getInstance().isValid(clientOnboardDTO.getPocEmail()))
            throw new BizException(BizErrors.INVALID_VALUE.getValue(), "Incorrect POC emailId");
        if (StringUtils.isBlank(clientOnboardDTO.getPocMobile()) || !isValidMobileNumber(clientOnboardDTO.getPocMobile()))
            throw new BizException(BizErrors.INVALID_VALUE.getValue(), "please enter proper POC Mobile number");
        if (StringUtils.isBlank(clientOnboardDTO.getPocEmail()) || !EmailValidator.getInstance().isValid(clientOnboardDTO.getPocEmail()))
            throw new BizException(BizErrors.INVALID_VALUE.getValue(), "Incorrect POC emailId");
        if (StringUtils.isNotBlank(clientOnboardDTO.getOptionalPocEmail()) && !EmailValidator.getInstance().isValid(clientOnboardDTO.getOptionalPocEmail()))
            throw new BizException(BizErrors.INVALID_VALUE.getValue(), "Incorrect Optional POC emailId");
         if (StringUtils.isBlank(clientOnboardDTO.getPocMobile()) || !isValidMobileNumber(clientOnboardDTO.getPocMobile()))
            throw new BizException(BizErrors.INVALID_VALUE.getValue(), "please enter proper POC Mobile number");
            */
        if (StringUtils.isBlank(clientOnboardDTO.getPassword()))
            throw new BizException(BizErrors.INVALID_VALUE.getValue(), "Incorrect Password");

       /* if (StringUtils.isNotBlank(clientOnboardDTO.getOptionalPocMobile()) && !isValidMobileNumber(clientOnboardDTO.getOptionalPocMobile()))
            throw new BizException(BizErrors.INVALID_VALUE.getValue(), "please enter proper Optional POC Mobile number");*/
        if (StringUtils.isBlank(clientOnboardDTO.getNameOfDirector()))
            throw new BizException(BizErrors.INVALID_VALUE.getValue(), "please enter Valid Director name ");
        if (StringUtils.isBlank(clientOnboardDTO.getNameOfDirector()))
            throw new BizException(BizErrors.INVALID_VALUE.getValue(), "please enter Valid Dorector name ");
        if (StringUtils.isBlank(clientOnboardDTO.getCitizenship()))
            throw new BizException(BizErrors.INVALID_VALUE.getValue(), "please enter Valid Citizenship");
        if (StringUtils.isBlank(clientOnboardDTO.getDobOfDirector()) || !isValidDate(clientOnboardDTO.getDobOfDirector()))
            throw new BizException(BizErrors.INVALID_VALUE.getValue(), "please enter Valid DOB of Director");

        /*if (Objects.isNull(clientOnboardDTO.getCopyOfDirectorFront()) || clientOnboardDTO.getCopyOfDirectorFront().getSize() > DEFAULT_FILE_SIZE_IN_MB)
            throw new BizException(BizErrors.APPLICATION_ERROR.getValue(), "copyOfDirectorFront is Required and size should be less than 2MB");
        if (Objects.isNull(clientOnboardDTO.getCopyOfDirectorBack()) || clientOnboardDTO.getCopyOfDirectorBack().getSize() > DEFAULT_FILE_SIZE_IN_MB)
            throw new BizException(BizErrors.APPLICATION_ERROR.getValue(), "copyOfDirectorBack is Required and size should be less than 2MB");
        if (Objects.isNull(clientOnboardDTO.getDirectorAddressProofFront()) || clientOnboardDTO.getDirectorAddressProofFront().getSize() > DEFAULT_FILE_SIZE_IN_MB)
            throw new BizException(BizErrors.APPLICATION_ERROR.getValue(), "directorBackAddressProofFront is Required and size should be less than 2MB");
        if (Objects.isNull(clientOnboardDTO.getDirectorAddressProofBack()) || clientOnboardDTO.getDirectorAddressProofBack().getSize() > DEFAULT_FILE_SIZE_IN_MB)
            throw new BizException(BizErrors.APPLICATION_ERROR.getValue(), "directorAddressProofBack is Required and size should be less than 2MB");
        if (Objects.isNull(clientOnboardDTO.getDirectorAddressProofBack()) || clientOnboardDTO.getDirectorAddressProofBack().getSize() > DEFAULT_FILE_SIZE_IN_MB)
            throw new BizException(BizErrors.APPLICATION_ERROR.getValue(), "directorAddressProofBack is Required and size should be less than 2MB");
        if (Objects.isNull(clientOnboardDTO.getGstCertificate()) || clientOnboardDTO.getGstCertificate().getSize() > DEFAULT_FILE_SIZE_IN_MB)
            throw new BizException(BizErrors.APPLICATION_ERROR.getValue(), "gstCertificate size should be less than 2MB");
        if (Objects.isNull(clientOnboardDTO.getCompanyPan()) || clientOnboardDTO.getCompanyPan().getSize() > DEFAULT_FILE_SIZE_IN_MB)
            throw new BizException(BizErrors.APPLICATION_ERROR.getValue(), "companyPan size should be less than 2MB");
        if (Objects.nonNull(clientOnboardDTO.getAgreementSOW()) && clientOnboardDTO.getAgreementSOW().getSize() > DEFAULT_FILE_SIZE_IN_MB)
            throw new BizException(BizErrors.APPLICATION_ERROR.getValue(), "agreementSOW size should be less than 2MB");
       /* if (Objects.nonNull(clientOnboardDTO.getProjectionCommitment()) && clientOnboardDTO.getProjectionCommitment().getSize() > DEFAULT_FILE_SIZE_IN_MB)
            throw new BizException(BizErrors.APPLICATION_ERROR.getValue(), "projectionCommitment size should be less than 2MB");
        if (Objects.isNull(clientOnboardDTO.getIncorporationCertificate()) || clientOnboardDTO.getIncorporationCertificate().getSize() > DEFAULT_FILE_SIZE_IN_MB)
            throw new BizException(BizErrors.APPLICATION_ERROR.getValue(), "incorporationCertificate size should be less than 2MB");*/
    }

    private void Validations(ClientOnboardDTO clientOnboardDTO, HttpServletRequest request) {

        //ValidateUser(clientOnboardDTO); // check if there is any client with same email or mobile is there already
        //Validateclient(clientOnboardDTO); // same for the client

        //validate user
        Users users = usersRepository.findByEmail(clientOnboardDTO.getEmail());
        if (users != null) {
            throw new BizException(BizErrors.BAD_REQUEST_ERROR.getValue(), "email already in use, please use other emailid");
        }

        users = usersRepository.findByMobile(clientOnboardDTO.getMobile());
        if (users != null) {
            throw new BizException(BizErrors.BAD_REQUEST_ERROR.getValue(), "contact number already present");
        }

        ClientDetails clientDetails2 = clientDetailsRepository.findByEntityName(clientOnboardDTO.getEntityName());
        if (clientDetails2 != null) {
            throw new BizException(BizErrors.BAD_REQUEST_ERROR.getValue(), "entity name already present");
        }

        //validate client
        ClientDetails clientDetails = clientDetailsRepository.findByEmail(clientOnboardDTO.getEmail());
        if (clientDetails != null) {
            throw new BizException(BizErrors.BAD_REQUEST_ERROR.getValue(), "email id already present, please enter other email");
        }

        ClientDetails clientDetails1 = clientDetailsRepository.findByMobile(clientOnboardDTO.getMobile());
        if (clientDetails1 != null) {
            throw new BizException(BizErrors.BAD_REQUEST_ERROR.getValue(), "contact number already present");
        }
    }

    /*private void ValidateUser(ClientOnboardDTO clientOnboardDTO) {

        Users users = usersRepository.findByEmail(clientOnboardDTO.getEmail());
        if (users != null) {
            throw new BizException(BizErrors.BAD_REQUEST_ERROR.getValue(), "email already in use, please use other emailid");
        }

        users = usersRepository.findByMobile(clientOnboardDTO.getMobile());
        if (users != null) {
            throw new BizException(BizErrors.BAD_REQUEST_ERROR.getValue(), "contact number already present");
        }

        ClientDetails clientDetails2 = clientDetailsRepository.findByEntityName(clientOnboardDTO.getEntityName());
        if (clientDetails2 != null) {
            throw new BizException(BizErrors.BAD_REQUEST_ERROR.getValue(), "entity name already present");
        }
    }

    private void Validateclient(ClientOnboardDTO clientOnboardDTO) {
        ClientDetails clientDetails = clientDetailsRepository.findByEmail(clientOnboardDTO.getEmail());
        if (clientDetails != null) {
            throw new BizException(BizErrors.BAD_REQUEST_ERROR.getValue(), "email id already present, please enter other email");
        }

        ClientDetails clientDetails1 = clientDetailsRepository.findByMobile(clientOnboardDTO.getMobile());
        if (clientDetails1 != null) {
            throw new BizException(BizErrors.BAD_REQUEST_ERROR.getValue(), "contact number already present");
        }

    }*/

    private Users UpdateUser(ClientOnboardDTO clientOnboardDTO, HttpServletRequest request, Program programDetails) {
        try {

            Users users = new Users();
            users.setFullName(clientOnboardDTO.getFirstName());
            // users.setLastName(clientOnboardDTO.getLastName());
            users.setMobile(clientOnboardDTO.getMobile());
            users.setUsername(clientOnboardDTO.getEmail());
            users.setEmail(clientOnboardDTO.getEmail());
            users.setChannelId(programDetails.getProgramHashId());
            users.setUserHashId(randomString(10, "USER"));
            users.setMobileToken(generateSixDigitNumericString());
            users.setStatus(Status.ACTIVE);
            users.setCreatedBy(programDetails.getCreatedBy());
            users.setOtpGenerationTime(new Date(System.currentTimeMillis()));
            users.setPassword(CustomBcryptPasswordEncoder.getBcryptPasswordEncoder().encode(clientOnboardDTO.getPassword()));

            Role role = roleRepository.findByName(String.valueOf(Roles.ROLE_CLIENT));
            Set<Role> roles = Sets.newHashSet();
            roles.add(role);
            users.setUserRoles(roles);
            usersRepository.save(users);
            return users;
        } catch (Exception e) {
            throw new BizException(BizErrors.BAD_REQUEST_ERROR.getValue(), "failed to update the client in user table");
        }
    }

    private void updateClientHashInUser(Users users, ClientDetails clientDetails) {
        users.setProductType(UserType.CLIENT_ADMIN);
        users.setProductCode(clientDetails.getClientHashId());
        usersRepository.save(users);
    }


    private ClientDetails UpdateClient(ClientOnboardDTO clientOnboardDTO, Users users) {
        try {
            log.info("Updating client details");
            ClientDetails clientDetails = new ClientDetails();
            clientDetails.setAddress(clientOnboardDTO.getAddress());
            clientDetails.setEmail(clientOnboardDTO.getEmail());
            clientDetails.setEntityName(clientOnboardDTO.getEntityName());
            clientDetails.setMobile(clientOnboardDTO.getMobile());
            clientDetails.setUsers(users);
            clientDetails.setClientHashId(UUID.randomUUID().toString());
            clientDetails.setCreatedBy(users.getCreatedBy());
            clientDetails.setAddress(clientOnboardDTO.getAddress());
            clientDetails.setClientLogo(uploadImage(clientOnboardDTO.getClientLogo(), users.getUserHashId() + S3FilePostfix.CLIENT_LOGO, Constants.CLINET_LOGO_DIR));

            clientDetails.setPocEmail(clientOnboardDTO.getPocEmail());
            clientDetails.setPocMobile(clientOnboardDTO.getPocMobile());
            clientDetails.setOptionalPocEmail(clientOnboardDTO.getOptionalPocEmail());
            clientDetails.setOptionalPocMobile(clientOnboardDTO.getOptionalPocMobile());
            clientDetails.setNameOfDirector(clientOnboardDTO.getNameOfDirector());
            clientDetails.setCitizenship(clientOnboardDTO.getCitizenship());
            clientDetails.setDobOfDirector(parseDate(clientOnboardDTO.getDobOfDirector()));
            clientDetails.setCopyOfDirectorFront(uploadImage(clientOnboardDTO.getCopyOfDirectorFront(), users.getUserHashId() + S3FilePostfix.DIRECTOR_FRONT, Constants.CLINET_DOCUMENT_DIR));
            clientDetails.setCopyOfDirectorBack(uploadImage(clientOnboardDTO.getCopyOfDirectorBack(), users.getUserHashId() + S3FilePostfix.DIRECTOR_BACK, Constants.CLINET_DOCUMENT_DIR));
            clientDetails.setDirectorAddressProofFront(uploadImage(clientOnboardDTO.getDirectorAddressProofFront(), users.getUserHashId() + S3FilePostfix.DIRECTOR_ADDRESS_FRONT, Constants.CLINET_DOCUMENT_DIR));
            clientDetails.setDirectorAddressProofBack(uploadImage(clientOnboardDTO.getDirectorAddressProofBack(), users.getUserHashId() + S3FilePostfix.DIRECTOR_ADDRESS_FRONT, Constants.CLINET_DOCUMENT_DIR));
            clientDetails.setGstCertificate(uploadImage(clientOnboardDTO.getGstCertificate(), users.getUserHashId() + S3FilePostfix.GST, Constants.CLINET_DOCUMENT_DIR));
            clientDetails.setBoardResolutionFormSigned(uploadImage(clientOnboardDTO.getBoardResolutionFormSigned(), users.getUserHashId() + S3FilePostfix.BOARD_RESOLUTION_FORM, Constants.CLINET_DOCUMENT_DIR));

            clientDetails.setCompanyPan(uploadImage(clientOnboardDTO.getCompanyPan(), users.getUserHashId() + S3FilePostfix.COMPANY_PAN, Constants.CLINET_DOCUMENT_DIR));
            clientDetails.setAgreementSOW(uploadImage(clientOnboardDTO.getAgreementSOW(), users.getUserHashId() + S3FilePostfix.AGREEMENT_SOW, Constants.CLINET_DOCUMENT_DIR));
            clientDetails.setProgramOnboardingPlan(uploadImage(clientOnboardDTO.getProgramOnboardingPlan(), users.getUserHashId() + S3FilePostfix.PROGRAM_ONBOARDING_PLAN, Constants.CLINET_DOCUMENT_DIR));
            //clientDetails.setProjectionCommitment(uploadImage(clientOnboardDTO.getProjectionCommitment(), users.getUserHashId() + S3FilePostfix.PROJECTION_COMMITMENT, Constants.CLINET_DOCUMENT_DIR));
            //clientDetails.setIncorporationCertificate(uploadImage(clientOnboardDTO.getIncorporationCertificate(), users.getUserHashId() + S3FilePostfix.INCORPORATION_CERTIFICATE, Constants.CLINET_DOCUMENT_DIR));
            clientDetails.setPenaltyClause(uploadImage(clientOnboardDTO.getPenaltyClause(), users.getUserHashId() + S3FilePostfix.PENALTY_CLAUSE, Constants.CLINET_DOCUMENT_DIR));
            //*/
            clientDetails.setStatus(Status.ACTIVE);
            clientDetails.setOnboardingState(OnboardingState.CLIENT);
            return clientDetailsRepository.save(clientDetails);
        } catch (ParseException e) {
            log.error(ExceptionUtils.getStackTrace(e));
            usersRepository.delete(users);
            throw new BizException(BizErrors.BAD_REQUEST_ERROR.getValue(), "Invalid dobOfDirector");
        } catch (Exception e) {
            log.error(ExceptionUtils.getStackTrace(e));
            usersRepository.delete(users);
            throw new BizException(BizErrors.BAD_REQUEST_ERROR.getValue(), "failed to update the client details");
        }
    }

    /*
    get client API is for showing client list by page
    and for Add Client Admin module without page and size
     */
    @Override
    public Object getclient(String role, String page, String size) {
        if (Roles.ROLE_ADMIN.toString().equals(role)) {
            try {
                Object clientDetails = null;
                if (page != null && size != null) {
                    Sort sort = new Sort(Sort.Direction.DESC, "createdAt");
                    Pageable pageable = PageRequest.of(Integer.parseInt(page), Integer.parseInt(size), sort);
                    clientDetails = clientDetailsRepository.findAll(pageable);
                } else {
                    clientDetails = clientDetailsRepository.findAll();
                }
                Map<String, Object> map = Maps.newHashMap();
                map.put("clientDetailsList", clientDetails);
                return map;
            } catch (Exception e) {
                log.error(e.getMessage());
                throw new BizException(BizErrors.BAD_REQUEST_ERROR.getValue(), "error in getting client list");
            }
        }
        return null;
    }

    @Override
    public ApiResponse<Object> blockUnblockClient(ClientOnboardDTO clientOnboardDTO, String role, String programUrl) {

        Users user = usersRepository.findById(Long.parseLong(clientOnboardDTO.getUsersId()));

        try {
            if (Roles.ROLE_ADMIN.toString().equals(role)) {
                if (Status.ACTIVE.equals(user.getStatus())) {
                    user.setStatus(Status.BLOCKED);
                    user.setBlocked(true);
                    usersRepository.save(user);
                } else {
                    user.setStatus(Status.ACTIVE);
                    user.setBlocked(false);
                    usersRepository.save(user);
                }
                return ApiResponse.builder().build();
            }
            throw new BizException(BizErrors.BAD_REQUEST_ERROR.getValue(), "only admin have the access to change the status");
        } catch (Exception e) {
            e.printStackTrace();
            throw new BizException((BizErrors.APPLICATION_ERROR.getValue()), "error! in update client status");
        }
    }

    @Override
    public Object getusers(HttpServletRequest request) {
        String role = request.getHeader("role");
        if (Roles.ROLE_ADMIN.toString().equals(role)) {
            try {
                int page = Integer.parseInt(request.getHeader("page"));
                int size = Integer.parseInt(request.getHeader("size"));
                Page<Program> corporateProcessors;
                Sort sort = new Sort(Sort.Direction.DESC, "createdAt");
                Pageable pageable = PageRequest.of(page, size, sort);
                Page<Users> usersPage = usersRepository.findAll(pageable);
                return usersPage;
            } catch (Exception e) {
                throw new BizException(BizErrors.BAD_REQUEST_ERROR.getValue(), "error in getting users list");
            }
        }
        return null;
    }

    @Override
    public Object getFilteredUsers(HttpServletRequest request, HttpServletResponse response, APIRequestDto apiRequestDto) {
        String role = request.getHeader("role");
        if (!Roles.ROLE_ADMIN.toString().equals(role))
            throw new BizException(BizErrors.BAD_REQUEST_ERROR.getValue(), "role header is missing!");

        try {
            int page = Integer.parseInt(request.getHeader("page"));
            int size = Integer.parseInt(request.getHeader("size"));
            Page<Users> userList;
            Sort sort = new Sort(Sort.Direction.DESC, "createdAt");
            Pageable pageable = PageRequest.of(page, size, sort);
            if (StringUtils.isNotBlank(apiRequestDto.getDateRange())) {
                String[] dateArray = apiRequestDto.getDateRange().split("-");
                String startDateStr = CommonUtil.dateTimeFromatter.format(CommonUtil.dateFormatterSlashWithTime.parse(dateArray[0] + CommonUtil.startTime));
                String endDateStr = CommonUtil.dateTimeFromatter.format(CommonUtil.dateFormatterSlashWithTime.parse(dateArray[1] + CommonUtil.endTime));
                Date startDate = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").parse(startDateStr);
                Date endDate = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").parse(endDateStr);
                userList = usersRepository.findAllByCreatedAtBetween(startDate, endDate, pageable);
                return userList;
            } else {
                Specification<Users> specification = getUserSpecification(apiRequestDto);
                userList = usersRepository.findAll(specification, pageable);
                return userList;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    private Specification<Users> getUserSpecification(APIRequestDto apiRequestDto) {
        Specification<Users> specification =
                (corporateProcessorRoot, criteriaQuery, criteriaBuilder) -> criteriaBuilder.equal(criteriaBuilder.literal(1), 1);
        try {
            if (StringUtils.isNotBlank(apiRequestDto.getEmail())) {
                specification = specification.and((root, query, criteriaBuilder) ->
                        criteriaBuilder.equal(root.get("email"), apiRequestDto.getEmail())
                );
            }
            if (StringUtils.isNotBlank(apiRequestDto.getMobile())) {
                specification = specification.and((root, query, criteriaBuilder) ->
                        criteriaBuilder.equal(root.get("mobile"), apiRequestDto.getMobile())
                );
            }
            if (StringUtils.isNotBlank(apiRequestDto.getUserHashId())) {
                specification = specification.and((root, query, criteriaBuilder) ->
                        criteriaBuilder.equal(root.get("userHashId"), apiRequestDto.getUserHashId())
                );
            }
            if (StringUtils.isNotBlank(apiRequestDto.getFullName())) {
                specification = specification.and((root, query, criteriaBuilder) ->
                        criteriaBuilder.equal(root.get("fullName"), apiRequestDto.getFullName())
                );
            }
        } catch (Exception e) {
            log.error(e.getMessage());
            throw new BizException(BizErrors.BAD_REQUEST_ERROR.getValue(), "Internal server error. Please try again after sometimes");
        }
        return specification;
    }

    @Override
    public ApiResponse<Object> blockUnblockUsers(ClientOnboardDTO clientOnboardDTO, String role, String programUrl) {

        Users user = usersRepository.findById(Long.parseLong(clientOnboardDTO.getUsersId()));

        try {
            if (Roles.ROLE_ADMIN.toString().equals(role)) {
                if (Status.ACTIVE.equals(user.getStatus())) {
                    user.setStatus(Status.BLOCKED);
                    user.setBlocked(true);
                    usersRepository.save(user);
                } else {
                    user.setStatus(Status.ACTIVE);
                    user.setBlocked(false);
                    usersRepository.save(user);
                }
                return ApiResponse.builder().build();
            }
            throw new BizException(BizErrors.BAD_REQUEST_ERROR.getValue(), "only admin have the access to change the status");
        } catch (Exception e) {
            e.printStackTrace();
            throw new BizException((BizErrors.APPLICATION_ERROR.getValue()), "error! in update client status");
        }
    }

    @Override
    public ApiResponse<Object> verifyOnboardingOTP(OnboardingOtpRequestDTO onboardingOtpRequestDTO, HttpServletRequest request, String programUrl) {

        validateFields(onboardingOtpRequestDTO);

        Commercials commercials = commercialsRepository.findByHashId(onboardingOtpRequestDTO.getCommercialHashId());

        try {

            if (commercials == null)
                throw new BizException(BizErrors.BAD_REQUEST_ERROR.getValue(), "commercial not found to verify");

            if (!onboardingOtpRequestDTO.getOtp().equals(commercials.getOtp()))
                throw new BizException(BizErrors.BAD_REQUEST_ERROR.getValue(), "Incorrect OTP");

            if (Status.ACTIVE.equals(commercials.getStatus()))
                throw new BizException(BizErrors.BAD_REQUEST_ERROR.getValue(), "request already processed!");

            commercials.setStatus(Status.ACTIVE);
            commercialsRepository.save(commercials);

            /*Program program = commercials.getProgram();
            program.setStatus(Status.ACTIVE);
            programRepository.save(program);*/

            Users users = usersRepository.findByUserHashId(onboardingOtpRequestDTO.getUserHashId());
            users.setStatus(Status.ACTIVE);
            usersRepository.save(users);
            ClientDetails clientDetails = clientDetailsRepository.findByUsers(users);
            clientDetails.setOnboardingState(OnboardingState.CLEAR);
            clientDetailsRepository.save(clientDetails);

            sendEmailToManagement(CommonUtil.getLoggedInUser(request, usersRepository, programUrl), users);
            //sendEmailToOnboardingClient(users, program.getHostUrl());
        } catch (Exception ee) {
            log.error(ee.getMessage());
            ee.printStackTrace();
            throw new BizException(BizErrors.APPLICATION_ERROR.getValue(), "error in otp verification");
        }
        return ApiResponse.builder().body(true).build();
    }

    private void validateFields(OnboardingOtpRequestDTO onboardingOtpRequestDTO) {

        if (StringUtils.isBlank(onboardingOtpRequestDTO.getOtp()) || !isNumeric(onboardingOtpRequestDTO.getOtp()))
            throw new BizException(BizErrors.BAD_REQUEST_ERROR.getValue(), "OTP is missing");
        if (StringUtils.isBlank(onboardingOtpRequestDTO.getCommercialHashId()))
            throw new BizException(BizErrors.BAD_REQUEST_ERROR.getValue(), "commercialHashId is missing");
    }

    private void sendEmailToManagement(Users loggedInUser, Users cretedUser) {
        String body = constructBodyOfEmail();
        sendFromGMail(cretedUser.getEmail(), Constants.EMAIL_ID_PASSWORD_FOR_ACTIVATION_AND_OTP
                , Constants.RECIPIENTS_FOR_OTP_ACTIVATION, Constants.SUBJECT_FOR_OTP_ACTIVATION, body);
    }

    private String constructBodyOfEmail() {

        return null;
    }

    private static void sendFromGMail(String from, String pass, String[] to, String subject, String body) {
        Properties props = System.getProperties();
        String host = "smtp.falconide.com";
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.smtp.host", host);
        props.put("mail.smtp.user", from);
        props.put("mail.smtp.password", pass);
        props.put("mail.smtp.port", "587");
        props.put("mail.smtp.auth", "true");

        Session session = Session.getInstance(props);
        MimeMessage message = new MimeMessage(session);

        try {
            message.setFrom(new InternetAddress(Constants.SET_FROM));
            InternetAddress[] toAddress = new InternetAddress[to.length];

            // To get the array of addresses
            for (int i = 0; i < to.length; i++) {
                toAddress[i] = new InternetAddress(to[i]);
            }

            for (int i = 0; i < toAddress.length; i++) {
                message.addRecipient(Message.RecipientType.TO, toAddress[i]);
            }

            message.setSubject(subject);
            message.setText(body, "UTF-8", "html");
            Transport transport = session.getTransport("smtp");
            transport.connect(host, from, pass);
            transport.sendMessage(message, message.getAllRecipients());
            transport.close();
        } catch (AddressException ae) {
            ae.printStackTrace();
        } catch (MessagingException me) {
            me.printStackTrace();
        }

    }

    @Override
    public ApiResponse<Object> updateClientDetails(ClientOnboardDTO clientOnboardDTO, HttpServletRequest request, String programUrl) {
        try {
            Optional<ClientDetails> clientDetailsOptional = clientDetailsRepository.findById(clientOnboardDTO.getClientId());
            ClientDetails clientDetails;
            if (!clientDetailsOptional.isPresent()) {
                log.error("Client Details with Id: " + clientOnboardDTO.getClientId() + " not Found");
                throw new BizException(BizErrors.BAD_REQUEST_ERROR.getValue(), "Client Details with Id: " + clientOnboardDTO.getClientId() + " not Found");
            } else {
                clientDetails = clientDetailsOptional.get();
            }

            clientDetails.setAddress(clientOnboardDTO.getAddress());
            clientDetails.setEmail(clientOnboardDTO.getEmail());
            clientDetails.setEntityName(clientOnboardDTO.getEntityName());
            clientDetails.setMobile(clientOnboardDTO.getMobile());
            clientDetails.setAddress(clientOnboardDTO.getAddress());

            clientDetails.setPocEmail(clientOnboardDTO.getPocEmail());
            clientDetails.setPocMobile(clientOnboardDTO.getPocMobile());
            //clientDetails.setOptionalPocEmail(clientOnboardDTO.getOptionalPocEmail());
            //clientDetails.setOptionalPocMobile(clientOnboardDTO.getOptionalPocMobile());
            clientDetails.setNameOfDirector(clientOnboardDTO.getNameOfDirector());
            clientDetails.setCitizenship(clientOnboardDTO.getCitizenship());
            clientDetails.setDobOfDirector(parseDate(clientOnboardDTO.getDobOfDirector()));
            if (clientOnboardDTO.getClientLogo() != null) {
                clientDetails.setClientLogo(uploadImage(clientOnboardDTO.getClientLogo(), clientDetails.getUsers().getUserHashId() + S3FilePostfix.CLIENT_LOGO, Constants.CLINET_LOGO_DIR));
            }
            if (clientOnboardDTO.getCopyOfDirectorFront() != null) {
                clientDetails.setCopyOfDirectorFront(uploadImage(clientOnboardDTO.getCopyOfDirectorFront(), clientDetails.getUsers().getUserHashId() + S3FilePostfix.DIRECTOR_FRONT, Constants.CLINET_DOCUMENT_DIR));
            }
            if (clientOnboardDTO.getCopyOfDirectorBack() != null) {
                clientDetails.setCopyOfDirectorBack(uploadImage(clientOnboardDTO.getCopyOfDirectorBack(), clientDetails.getUsers().getUserHashId() + S3FilePostfix.DIRECTOR_BACK, Constants.CLINET_DOCUMENT_DIR));
            }
            /*if (clientOnboardDTO.getDirectorAddressProofFront() != null) {
                clientDetails.setDirectorAddressProofFront(uploadImage(clientOnboardDTO.getDirectorAddressProofFront(), clientDetails.getUsers().getUserHashId() + S3FilePostfix.DIRECTOR_ADDRESS_FRONT, Constants.CLINET_DOCUMENT_DIR));
            }*/
           /* if (clientOnboardDTO.getDirectorAddressProofBack() != null) {
                clientDetails.setDirectorAddressProofBack(uploadImage(clientOnboardDTO.getDirectorAddressProofBack(), clientDetails.getUsers().getUserHashId() + S3FilePostfix.DIRECTOR_ADDRESS_FRONT, Constants.CLINET_DOCUMENT_DIR));
            }*/
            if (clientOnboardDTO.getGstCertificate() != null) {
                clientDetails.setGstCertificate(uploadImage(clientOnboardDTO.getGstCertificate(), clientDetails.getUsers().getUserHashId() + S3FilePostfix.GST, Constants.CLINET_DOCUMENT_DIR));
            }
            if (clientOnboardDTO.getBoardResolutionFormSigned() != null) {
                clientDetails.setBoardResolutionFormSigned(uploadImage(clientOnboardDTO.getBoardResolutionFormSigned(), clientDetails.getUsers().getUserHashId() + S3FilePostfix.BOARD_RESOLUTION_FORM, Constants.CLINET_DOCUMENT_DIR));
            }

            if (clientOnboardDTO.getCompanyPan() != null) {
                clientDetails.setCompanyPan(uploadImage(clientOnboardDTO.getCompanyPan(), clientDetails.getUsers().getUserHashId() + S3FilePostfix.COMPANY_PAN, Constants.CLINET_DOCUMENT_DIR));
            }
            /*if (clientOnboardDTO.getAgreementSOW() != null) {
                clientDetails.setAgreementSOW(uploadImage(clientOnboardDTO.getAgreementSOW(), clientDetails.getUsers().getUserHashId() + S3FilePostfix.AGREEMENT_SOW, Constants.CLINET_DOCUMENT_DIR));
            }*/
            /*if (clientOnboardDTO.getProgramOnboardingPlan() != null) {
                clientDetails.setProgramOnboardingPlan(uploadImage(clientOnboardDTO.getProgramOnboardingPlan(), clientDetails.getUsers().getUserHashId() + S3FilePostfix.PROGRAM_ONBOARDING_PLAN, Constants.CLINET_DOCUMENT_DIR));
            }*/
            /*if (clientOnboardDTO.getProjectionCommitment() != null) {
                clientDetails.setProjectionCommitment(uploadImage(clientOnboardDTO.getProjectionCommitment(), clientDetails.getUsers().getUserHashId() + S3FilePostfix.PROJECTION_COMMITMENT, Constants.CLINET_DOCUMENT_DIR));
            }
            if (clientOnboardDTO.getIncorporationCertificate() != null) {
                clientDetails.setIncorporationCertificate(uploadImage(clientOnboardDTO.getIncorporationCertificate(), clientDetails.getUsers().getUserHashId() + S3FilePostfix.INCORPORATION_CERTIFICATE, Constants.CLINET_DOCUMENT_DIR));
            }*/
            /*if (clientOnboardDTO.getPenaltyClause() != null) {
                clientDetails.setPenaltyClause(uploadImage(clientOnboardDTO.getPenaltyClause(), clientDetails.getUsers().getUserHashId() + S3FilePostfix.PENALTY_CLAUSE, Constants.CLINET_DOCUMENT_DIR));
            }*/
            clientDetailsRepository.save(clientDetails);
            return ApiResponse.builder().body(true).build();
        } catch (ParseException e) {
            log.error(ExceptionUtils.getStackTrace(e));
            throw new BizException(BizErrors.BAD_REQUEST_ERROR.getValue(), "Invalid dobOfDirector");
        } catch (Exception e) {
            log.error(e.getMessage());
            throw new BizException(BizErrors.BAD_REQUEST_ERROR.getValue(), "failed to update the client details");
        }
    }

    /*@Override
    public Object getclientById(String role, String id) {

        try {
            Optional<ClientDetails> clientDetailsOptional = clientDetailsRepository.findById(Long.parseLong(id));
            if (clientDetailsOptional.isPresent()) {
                return clientDetailsOptional.get();
            }
            else {
                throw new BizException(BizErrors.BAD_REQUEST_ERROR.getValue(), "failed to fetch the client");
            }
        } catch (Exception e) {
            throw new BizException(BizErrors.BAD_REQUEST_ERROR.getValue(), "failed to fetch the client");
        }

    }*/

    @Override
    public Object getclientById(String id) {

        try {
            Optional<ClientDetails> clientDetailsOptional = clientDetailsRepository.findById(Long.parseLong(id));
            if (clientDetailsOptional.isPresent()) {
                return clientDetailsOptional.get();
            } else {
                throw new BizException(BizErrors.BAD_REQUEST_ERROR.getValue(), "failed to fetch the client");
            }
        } catch (Exception e) {
            throw new BizException(BizErrors.BAD_REQUEST_ERROR.getValue(), "failed to fetch the client");
        }
    }

    private void sendEmailToOnboardingClient(Users user, String hostUrl) {
        String body = constructBodyForClientOnboarding(user.getFullName(), null, user.getEmail(), user.getMobile(), hostUrl);
        String[] to = {user.getEmail()};
        sendFromGMail(Constants.EMAIL_ID_FOR_ACTIVATION_AND_OTP, Constants.EMAIL_ID_PASSWORD_FOR_ACTIVATION_AND_OTP, to, "Login details", body);
    }

    private String constructBodyForClientOnboarding(String firstName, String lastName, String email, String password, String hostUrl) {
        StringBuffer header = new StringBuffer(
                "<!DOCTYPE html>\n" +
                        "<html>\n" +
                        "<head>\n" +
                        "</head>\n" +
                        "<body>\n" +
                        "Dear " + firstName + " " + lastName + ",<br>" +
                        "Registration successfully done in autobots 2.0. Please find the Login details below.<br>" +
                        "Username : " + email + "<br>" +
                        "Password : " + password + "<br>" +
                        "Host URL : https://autobots2.com:8443/autobots-ui/" + hostUrl + "/auth/program<br>" +
                        "Regards,<br>" +
                        "Team Autobots.\n" +
                        "</body></html>"
        );
        return header.toString();
    }

    @Override
    public Object getClientList(String role) {
        if (Roles.ROLE_ADMIN.toString().equals(role)) {
            try {
                List<ClientDetails> clientDetails = clientDetailsRepository.findAll();
                Map<String, Object> map = Maps.newHashMap();

                List<Object> objectList = new ArrayList<>();
                for (ClientDetails details : clientDetails) {
                    Map<String, Object> map1 = Maps.newHashMap();
                    map1.put("id", details.getId());
                    map1.put("name", details.getEntityName());

                    objectList.add(map1);
                }

                map.put("clientList", objectList);
                return map;
            } catch (Exception e) {
                log.error(e.getMessage());
                throw new BizException(BizErrors.BAD_REQUEST_ERROR.getValue(), "error in getting client list");
            }
        }

        return null;
    }

    @Override
    public Object getClientOnfilter(APIRequestDto apiRequestDto) {
        try {
            Page<ClientDetails> clientDetails = null;
            Sort sort = new Sort(Sort.Direction.DESC, "createdAt");
            Pageable pageable = PageRequest.of(Integer.parseInt(apiRequestDto.getPage()), Integer.parseInt(apiRequestDto.getSize()), sort);

            if (!StringUtils.isEmpty(apiRequestDto.getMobile()) || !StringUtils.isEmpty(apiRequestDto.getEmail()) || !StringUtils.isEmpty(apiRequestDto.getEntityName())) {

                clientDetails = clientDetailsRepository.findAll(clientListSpecification(apiRequestDto), pageable);
                Map<String, Page<ClientDetails>> map = Maps.newHashMap();
                map.put("clients", clientDetails);
                return map;
            }

            if (!StringUtils.isEmpty(apiRequestDto.getDateRange())) {
                String dateArray[] = null;
                dateArray = apiRequestDto.getDateRange().split("-");
                try {
                    String startDate1;
                    startDate1 = CommonUtil.dateTimeFromatter.format(CommonUtil.dateFormatterSlashWithTime.parse(dateArray[0] + CommonUtil.startTime));
                    Date startDate = new SimpleDateFormat("yyyy-MM-dd ").parse(startDate1);
                    String endtDate1 = CommonUtil.dateTimeFromatter.format(CommonUtil.dateFormatterSlashWithTime.parse(dateArray[1] + CommonUtil.endTime));
                    Date endDate = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").parse(endtDate1);
                    clientDetails = clientDetailsRepository.findAllByDate(startDate, endDate, pageable);
                } catch (ParseException e) {
                    throw new BizException(BizErrors.APPLICATION_ERROR.getValue(), "Error while fetching client details with date filter.");
                }
                Map<String, Page<ClientDetails>> map = Maps.newHashMap();
                map.put("clients", clientDetails);
                return map;
            }
        } catch (Exception e) {
            throw new BizException(BizErrors.APPLICATION_ERROR.getValue(), "Failed while fetching client info.");
        }
        throw new BizException(BizErrors.APPLICATION_ERROR.getValue(), "Failed while fetching client info.");
    }

    private Specification<ClientDetails> clientListSpecification(APIRequestDto apiRequestDto) {
        Specification<ClientDetails> spec = null;
        try {
            spec = (ClientDetails, mq, mb) -> mb.equal(mb.literal(1), 1);
            if (!StringUtils.isEmpty(apiRequestDto.getMobile()) && apiRequestDto.getMobile() != "") {
                spec = spec.and((ClientDetails, mq, mb) -> mb.equal(ClientDetails.get("mobile"), apiRequestDto.getMobile()));
            }
            if (!StringUtils.isEmpty(apiRequestDto.getEmail()) && apiRequestDto.getEmail() != "") {
                spec = spec.and((ClientDetails, mq, mb) -> mb.equal(ClientDetails.get("email"), apiRequestDto.getEmail()));
            }
            if (!StringUtils.isEmpty(apiRequestDto.getEntityName()) && apiRequestDto.getEntityName() != "") {
                spec = spec.and((ClientDetails, mq, mb) -> mb.equal(ClientDetails.get("entityName"), apiRequestDto.getEntityName()));
            }
        } catch (Exception e) {
            throw new BizException(BizErrors.APPLICATION_ERROR.getValue(), "Failed fetching client info.");
        }
        return spec;
    }

    @Override
    public ClientDetails fetchClientByClientCode(final String clientHashId, final boolean throwException) {
        final ClientDetails clientDetail = clientDetailsRepository.getByClientHashId(clientHashId);
        if (clientDetail == null) {
            if (throwException) {
                throw new BizException("Invalid client hashId");
            }
        }
        return clientDetail;
    }

    @Override
    public Object fetchDataForCustomerOnboarding(String loggedInUserHashId, String programUrl) {
        log.info("*** initiating fetchDataForCustomerOnboarding() for logggedinUser:  "+loggedInUserHashId);
        Map<String, Object> details = new HashMap<>();
        Program program = programRepository.findByHostUrl(programUrl);

        if (program == null)
            throw new BizException(BizErrors.BAD_REQUEST_ERROR.getValue(), "invalid program found!");

        // if program has mapped with distributor means, registration is through agent
        if (ProgramPlans.DISTRIBUTOR.equals(program.getProgramPlan())) {
            log.info("*** getting agent details for logggedinUser: "+loggedInUserHashId);
            AgentDetails agentDetails = agentDetailsRepository.findByUserHashId(loggedInUserHashId);

            if (agentDetails == null)
                throw new BizException(BizErrors.BAD_REQUEST_ERROR.getValue(), "invalid logged in user!");
            details.put("agentHashId", agentDetails.getUserHashId());
            details.put("agentCompanyName", agentDetails.getCompanyName());
            details.put("agentName", agentDetails.getFullName());
            details.put("distributorHashId", agentDetails.getDistributorHashId());
            details.put("id", agentDetails.getId());

            log.info("*** Agent details for logggedinUser: "+details);
        }

        details.put("programId", program.getId());
        details.put("programHasMapped", program.getProgramPlan());

        return ApiResponse.builder().body(details).build();
    }

    @Override
    public Object getClientByEmailId(final String emailId) {
        log.info("*** inside get client by emailId : {}", emailId);
        try {
            ClientDetails clientDetails = clientDetailsRepository.findByEmail(emailId);
            if (clientDetails == null) {
                throw new BizException(BizErrors.BAD_REQUEST_ERROR.getValue(), "failed to fetch the client");
            }
            return clientDetails;
        } catch (Exception e) {
            throw new BizException(BizErrors.BAD_REQUEST_ERROR.getValue(), "failed to fetch the client");
        }
    }

    @Override
    public Object encryptPassword(String password) {
        String encryptPass = CustomBcryptPasswordEncoder.getBcryptPasswordEncoder().encode(password);
        return ApiResponse.builder().body(encryptPass).build();
    }

	@Override
    public Object verifyPassword(String encPassword, String rawPassword) {
        BCryptPasswordEncoder bCryptPasswordEncoder = new BCryptPasswordEncoder();
        if (bCryptPasswordEncoder.matches(rawPassword, encPassword)) {
            return ApiResponse.builder().body("Entered password is correct!").build();
        } else {
            throw new BizException(BizErrors.BAD_REQUEST_ERROR.getValue(), "password did not match!");
        }
    }

        @Override
        public Object clientListByProgramPlan(String role) {
        if (Roles.ROLE_ADMIN.toString().equals(role)) {
            try {
                ArrayList program_plan = new ArrayList();
                program_plan.add("DEFAULT");
                program_plan.add("LENDING");
                List<ClientDetails> clientDetails = clientDetailsRepository.clientUniqueListByProgramPlan(program_plan);
                Map<String, Object> map = Maps.newHashMap();
                map.put("clientListByProgramPlan", clientDetails);
                return map;
            } catch (Exception e) {
                log.error(e.getMessage());
                throw new BizException(BizErrors.BAD_REQUEST_ERROR.getValue(), "error in getting client list");
            }
        }
        return null;
    }
}

