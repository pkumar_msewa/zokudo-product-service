package com.cards.zokudo.services.impl;

import com.cards.zokudo.entities.Program;
import com.cards.zokudo.exceptions.BizException;
import com.cards.zokudo.interceptors.Interaction;
import com.cards.zokudo.repositories.ProgramRepository;
import com.cards.zokudo.services.SORService;
import com.cards.zokudo.util.*;
import lombok.extern.slf4j.Slf4j;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.Response;

@Slf4j
@Service
public class SORServiceImpl implements SORService {

    private final Client client;
    private final UrlMetaData urlMetaData;
    private final SecurityUtil securityUtil;
    private final ProgramRepository programRepository;

    @Autowired
    public SORServiceImpl(final Client client,
                          final UrlMetaData urlMetaData,
                          final SecurityUtil securityUtil,
                          final ProgramRepository programRepository){

        this.client = client;
        this.urlMetaData = urlMetaData;
        this.securityUtil = securityUtil;
        this.programRepository = programRepository;
    }
    @Override
    public void createProgram(Program program,String programUrl,String programType) {

            try{
                JSONObject request = new JSONObject();
                request.put("programId", program.getId());
                request.put("programHashId", program.getProgramHashId());
                request.put("programName", program.getProgramName());
                request.put("programPlan", program.getProgramPlan());
                request.put("programType", programType);

                String createdAt = CommonUtil.dateTimeFromatter.format(program.getCreatedAt());
                String updatedAt = CommonUtil.dateTimeFromatter.format(program.getUpdatedAt());
                request.put("createdAt",createdAt);
                request.put("updatedAt",updatedAt);
                log.info("** Request BODY for SOR Program creation, programId {} | programHashID {}" +
                        "| programName{} | createdAt {} | updatedAT {} ",program.getId(),program.getProgramPlan(),
                        program.getProgramName(),createdAt,updatedAt);
                String str = urlMetaData.CREATE_SOR_PROGRAM.replaceAll("\\{\\}", programUrl);
                Response response = client.target(str)
                        .request()
                        .accept(MediaType.APPLICATION_JSON_VALUE)
                        .header(AppConstants.AUTH_HEADER, securityUtil.getAuthorizationHeader())
                        .post(Entity.entity(request.toString(), MediaType.APPLICATION_JSON_VALUE));

                final String jsonResp = response.readEntity(String.class);

                if (response.getStatus() == 200) {
                    log.info("** Program created Successfully.");
                } else {
                    log.error("** Unable to create program ");
                }

            }catch (Exception e){
                log.error("** Unable to add Program Hash ID {} To Sor-Service ",program.getProgramHashId());
                log.error(e.getMessage(),e);
                throw new BizException("Error while saving Program to SOR Service. ",e.getMessage());
            }
    }
}
