package com.cards.zokudo.services.impl;

import com.cards.zokudo.dto.request.APIRequestDto;
import com.cards.zokudo.dto.response.CustomerApiResponseDto;
import com.cards.zokudo.entities.Program;
import com.cards.zokudo.entities.Role;
import com.cards.zokudo.entities.Users;
import com.cards.zokudo.entities.UsersHasProgram;
import com.cards.zokudo.enums.BizErrors;
import com.cards.zokudo.enums.Roles;
import com.cards.zokudo.enums.Status;
import com.cards.zokudo.enums.UserType;
import com.cards.zokudo.exceptions.BadRequestException;
import com.cards.zokudo.exceptions.BizException;
import com.cards.zokudo.repositories.ProgramRepository;
import com.cards.zokudo.repositories.RoleRepository;
import com.cards.zokudo.repositories.UsersHasProgramRepository;
import com.cards.zokudo.repositories.UsersRepository;
import com.cards.zokudo.response.ApiResponse;
import com.cards.zokudo.services.CustomerInf;
import com.cards.zokudo.util.CustomBcryptPasswordEncoder;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.glassfish.jersey.internal.guava.Sets;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

@Service
@Slf4j
public class CustomerImpl implements CustomerInf {

    private final UsersRepository usersRepository;
    private final UsersHasProgramRepository usersHasProgramRepository;
    private final ProgramRepository programRepository;
    private final RoleRepository roleRepository;

    @Autowired
    public CustomerImpl(final UsersRepository usersRepository, UsersHasProgramRepository usersHasProgramRepository, ProgramRepository programRepository,
                           RoleRepository roleRepository) {
        this.usersRepository = usersRepository;
        this.usersHasProgramRepository = usersHasProgramRepository;
        this.programRepository = programRepository;
        this.roleRepository = roleRepository;
    }

    @Override
    public ApiResponse<Object> addCustomer(APIRequestDto apiRequestDto, HttpServletRequest request, String programUrl) {
        log.info("*** Inside Add New Customer");
        // Validate Request and User Program
        validateCustomerRequest(apiRequestDto);

        // implementation
        Users users = addUserForProgramLogin(apiRequestDto);

        boolean b = assignRoles(users);

        assignPrograms(users, apiRequestDto.getProgram());

        return addUserResponse(b, users);
    }

    private void validateCustomerRequest(APIRequestDto apiRequestDto) {
        log.info("*** inside validate customer request");

        validateParams(apiRequestDto);

        validateUserAndProgram(apiRequestDto);
    }

    private void validateUserAndProgram(APIRequestDto apiRequestDto) {
        log.info("*** Inside Validate User and Program");

        Program program = programRepository.findByHostUrl(apiRequestDto.getHostUrl());
        if (program == null){
            log.error("Invalid program for hostUrl :{}", apiRequestDto.getHostUrl());
            throw new BadRequestException(BizErrors.BAD_REQUEST_ERROR.getValue(), "invalid program!");
        }

        Users user = usersRepository.findByUsername(apiRequestDto.getEmail(), program.getHostUrl());
        if (user != null){
            log.error("User already exist for :{}", apiRequestDto.getEmail());
            throw new BadRequestException(BizErrors.BAD_REQUEST_ERROR.getValue(), "user already exist with this email");
        }

        apiRequestDto.setProgram(program);
    }

    private void validateParams(APIRequestDto apiRequestDto) {
        log.info("Validate Parameters");

        if (StringUtils.isBlank(apiRequestDto.getFirst_name()))
            throw new BadRequestException(BizErrors.BAD_REQUEST_ERROR.getValue(), "first name should not be empty");

        if (StringUtils.isBlank(apiRequestDto.getLast_name()))
            throw new BadRequestException(BizErrors.BAD_REQUEST_ERROR.getValue(), "last name should not be empty");

        if (StringUtils.isBlank(apiRequestDto.getEmail()))
            throw new BadRequestException(BizErrors.BAD_REQUEST_ERROR.getValue(), "email should not be empty");

        if (StringUtils.isBlank(apiRequestDto.getMobile()))
            throw new BadRequestException(BizErrors.BAD_REQUEST_ERROR.getValue(), "mobile number should not be empty");

//        if (StringUtils.isBlank(apiRequestDto.getPassword()))
//            throw new BadRequestException(BizErrors.BAD_REQUEST_ERROR.getValue(), "password should not be empty");

        if (StringUtils.isBlank(apiRequestDto.getHostUrl()))
            throw new BadRequestException(BizErrors.BAD_REQUEST_ERROR.getValue(), "please select program");

        log.info("validated");
    }

    private void assignPrograms(Users users, Program program) {
        log.info("Inside assign programs for UserId :{}, and programId :{}", users.getId(), program.getId());
        try {
            UsersHasProgram usersHasProgram = new UsersHasProgram();
            usersHasProgram.setUsers(users);
            usersHasProgram.setProgram(program);
            usersHasProgram.setStatus(Status.ACTIVE);
            usersHasProgramRepository.save(usersHasProgram);
            log.info("Program has been assgined to User :{}", users.getId());
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            log.error("Exception occurred while assigning the program");
            throw new BizException(BizErrors.APPLICATION_ERROR.getValue(), "error!while assigning program");
        }
    }

    private Users  addUserForProgramLogin(APIRequestDto apiRequestDto) {
        try {
            Users users = new Users();
            users.setStatus(Status.ACTIVE);
            users.setUserRoles(null);
            users.setUserHashId(UUID.randomUUID().toString());
            users.setBlocked(false);
            users.setMobile(apiRequestDto.getMobile());
            users.setPassword(CustomBcryptPasswordEncoder.getBcryptPasswordEncoder().encode(apiRequestDto.getMobile()));
            users.setChannelId(apiRequestDto.getProgram().getProgramHashId());
            users.setEmail(apiRequestDto.getEmail());
            users.setFullName(apiRequestDto.getFirst_name());
           // users.setLastName(apiRequestDto.getLast_name());
            users.setUsername(apiRequestDto.getEmail());
            users.setProductType(UserType.CUSTOMER_ADMIN);
            return usersRepository.save(users);
        } catch (Exception e) {
            throw new BizException(BizErrors.APPLICATION_ERROR.getValue(), "could not insert user");
        }
    }

    private ApiResponse<Object> addUserResponse(boolean b, Users users) {

        if (!b)
            return ApiResponse.builder().body("Customer created! we are facing issues in role assignment.").build();

        CustomerApiResponseDto apiResponseDto = new CustomerApiResponseDto();
        apiResponseDto.setUserId(users.getId());
        apiResponseDto.setUserHashId(users.getUserHashId());
        apiResponseDto.setMessage("Customer added successfully");

//        return ApiResponse.builder().body("Customer created successfully!").build();
        return ApiResponse.builder().body(apiResponseDto).build();
    }

    private boolean assignRoles(Users users) {
        log.info("*** inside assign roles for userId :{}", users.getId());

        try {
            Role role = roleRepository.findByName(String.valueOf(Roles.ROLE_CUSTOMER));
            log.info("Role :{}", role);
            Set roleSet = Sets.newHashSet();
            log.info("Role set :{}", roleSet);
            roleSet.add(role);
            users.setUserRoles(roleSet);
            usersRepository.save(users);
            log.info("Role has been set for :{}", users);
            return true;
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return false;
        }
    }
}
