package com.cards.zokudo.services.impl;

import com.cards.zokudo.entities.PasswordHistory;
import com.cards.zokudo.entities.Users;
import com.cards.zokudo.enums.BizErrors;
import com.cards.zokudo.enums.Status;
import com.cards.zokudo.exceptions.BadRequestException;
import com.cards.zokudo.exceptions.BizException;
import com.cards.zokudo.repositories.PasswordHistoryRepository;
import com.cards.zokudo.repositories.UsersRepository;
import com.cards.zokudo.request.UpdatePasswordRequest;
import com.cards.zokudo.response.ApiResponse;
import com.cards.zokudo.services.UpdatePasswordService;
import com.cards.zokudo.util.Constants;
import com.cards.zokudo.util.CustomBcryptPasswordEncoder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UpdatePasswordServiceImpl implements UpdatePasswordService {

    @Autowired
    UsersRepository usersRepository;

    @Autowired
    PasswordHistoryRepository passwordHistoryRepository;

    @Override
    public ApiResponse<Object> execute(UpdatePasswordRequest updatePasswordRequest,String programUrl) {

        Users user = usersRepository.findByUsername(updatePasswordRequest.getUserName(),programUrl , Status.ACTIVE);

        validateOtpAndPassword(updatePasswordRequest, user);

        updatePassword(updatePasswordRequest, user);


        return ApiResponse.builder().build();
    }


    private void validateOtpAndPassword(UpdatePasswordRequest updatePasswordRequest, Users user) {

        if (user == null)
            throw new BadRequestException(BizErrors.BAD_REQUEST_ERROR.getValue(), "user does not exist");

        if (!user.getMobileToken().equals(updatePasswordRequest.getOtp()) || updatePasswordRequest.getOtp().isEmpty())
            throw new BadRequestException(BizErrors.BAD_REQUEST_ERROR.getValue(), "Please enter valid Otp");

        if (updatePasswordRequest.getNewPassword() == null || updatePasswordRequest.getNewPassword().length() < 8)
            throw new BadRequestException(BizErrors.BAD_REQUEST_ERROR.getValue(), "Please enter minimum 8 character length password");

        if (!updatePasswordRequest.getNewPassword().equals(updatePasswordRequest.getConfirmPassword()))
            throw new BadRequestException(BizErrors.BAD_REQUEST_ERROR.getValue(), "new password and confirm password mismatch");

        if (updatePasswordRequest.getNewPassword().length() > 20)
            throw new BadRequestException(BizErrors.BAD_REQUEST_ERROR.getValue(), "Max length of password should not exceed 20 characters");

        if (!Constants.PASSPATTERN.matcher(updatePasswordRequest.getNewPassword()).matches())
            throw new BadRequestException(BizErrors.BAD_REQUEST_ERROR.getValue(), "Password must be alphanumeric with lowercase ,uppercase and special character");

        List<PasswordHistory> history = passwordHistoryRepository.findByUsersOrderByCreatedAtDesc(user);
        if (history != null && history.size() > 0) {
            for (PasswordHistory passwordHistory : history) {
                if (CustomBcryptPasswordEncoder.getBcryptPasswordEncoder().matches(updatePasswordRequest.getNewPassword(), passwordHistory.getPassword())) {
                    throw new BizException(BizErrors.BAD_REQUEST_ERROR.getValue(), "Please enter the password that is unique from last 3 passwords");
                }
            }
        }
    }

    private void updatePassword(UpdatePasswordRequest updatePasswordRequest, Users user) {
        user.setPassword(CustomBcryptPasswordEncoder.getBcryptPasswordEncoder().encode(updatePasswordRequest.getConfirmPassword()));
        usersRepository.save(user);
        savePasswordHistory(user);
    }

    private void savePasswordHistory(Users user) {
        try {
            List<PasswordHistory> history = passwordHistoryRepository.findByUsersOrderByCreatedAtDesc(user);
            if (history != null && history.size() > 2) {
                passwordHistoryRepository.delete(history.get(history.size() - 1));
            }
            PasswordHistory password = new PasswordHistory();
            password.setPassword(user.getPassword());
            password.setUsers(user);
            passwordHistoryRepository.save(password);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
