package com.cards.zokudo.services.impl;

import com.cards.zokudo.entities.CardType;
import com.cards.zokudo.entities.ClientDetails;
import com.cards.zokudo.entities.Program;
import com.cards.zokudo.enums.BizErrors;
import com.cards.zokudo.enums.Roles;
import com.cards.zokudo.exceptions.BizException;
import com.cards.zokudo.repositories.ClientDetailsRepository;
import com.cards.zokudo.repositories.ProgramRepository;
import com.cards.zokudo.services.*;
import com.cards.zokudo.util.CommonUtil;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.*;

@Slf4j
@Service
public class DownloadServiceImpl implements DownloadService {

    private final ProductService productService;
    private final ProgramRepository programRepository;
    private final ClientDetailsRepository clientDetailsRepository;


    @Autowired
    public DownloadServiceImpl(ProductService productService, ProgramRepository programRepository,
                               ClientDetailsRepository clientDetailsRepository) {
        this.productService = productService;
        this.programRepository = programRepository;
        this.clientDetailsRepository = clientDetailsRepository;
    }

    @Override
    public void downloadProgramList(HttpServletRequest request, HttpServletResponse response, String programUrl, Map<String, String> requestParams) {
        final String role = requestParams.get("role");
        String processorCardType = null;
        Program programDetails = productService.getProductDetailsByHostUrl(programUrl);

        Object object = fetchProgramList(programDetails, role);
        if (object instanceof List<?>) {
            String resultantFileName = "Program_List.xlsx";
            final List<Program> programList = (List<Program>) object;
            if (programList.size() == 0) {
                throw new BizException("No Program list found!");
            }
            final List<Map<String, String>> resultantData = new ArrayList<Map<String, String>>();
            final List<String> headers = new ArrayList<String>();
            headers.add("CREATED DATE");
            headers.add("PROGRAM NAME");
            headers.add("CURRENCY");
            headers.add("HOST URL");
            headers.add("PROGRAM HASH ID");
            headers.add("NOTIFICATION URL");
            headers.add("BALANCE");
            headers.add("CORPORATE PROCESSOR");
            headers.add("PROCESSOR NAME");
            headers.add("PROCESSOR BALANCE TYPE");
            headers.add("PROCESSOR CARD TYPE");
            headers.add("STATUS");

            for (Program programData : programList) {
                final Map<String, String> dataMap = new HashMap<>();
                final String createdAt = StringUtils.isNotEmpty(String.valueOf(programData.getCreatedAt())) ? String.valueOf(programData.getCreatedAt()) : "NA";
                final String programName = StringUtils.isNotEmpty(programData.getProgramName()) ? programData.getProgramName() : "NA";
                final String currency = StringUtils.isNotEmpty(programData.getCurrency()) ? programData.getCurrency() : "NA";
                final String host = StringUtils.isNotEmpty(programData.getHostUrl()) ? programData.getHostUrl() : "NA";
                final String pid = StringUtils.isNotEmpty(programData.getProgramName()) ? programData.getProgramName() : "NA";
                final String notification = StringUtils.isNotEmpty(programData.getNotificationBaseUrl()) ? programData.getNotificationBaseUrl() : "NA";
                final String balance = StringUtils.isNotEmpty(String.valueOf(programData.getBalance())) ? String.valueOf(programData.getBalance()) : "NA";
                final String corporateProcessor = StringUtils.isNotEmpty(programData.getCorporateProcessor().getName()) ? programData.getCorporateProcessor().getName() : "NA";
                final String processor = StringUtils.isNotEmpty(programData.getCorporateProcessor().getCardProcessor().getName()) ? programData.getCorporateProcessor().getCardProcessor().getName() : "NA";
                final String processorBal = StringUtils.isNotEmpty(programData.getCorporateProcessor().getCardProcessor().getBalanceTypeAtProcessor().getValue()) ? programData.getCorporateProcessor().getCardProcessor().getBalanceTypeAtProcessor().getValue() : "NA";
                Set<CardType> cardType = programData.getCorporateProcessor().getCardProcessor().getProcessorCardType();
                for (CardType cardTypeObj : cardType) {
                    String cardTypName = cardTypeObj.getName();
                    processorCardType = StringUtils.isNotEmpty(cardTypName) ? cardTypName : "NA";
                }
                final String status = StringUtils.isNotEmpty(String.valueOf(programData.getStatus())) ? String.valueOf(programData.getStatus()) : "NA";

                dataMap.put("CREATED DATE", createdAt);
                dataMap.put("PROGRAM NAME", programName);
                dataMap.put("CURRENCY", currency);
                dataMap.put("HOST URL", host);
                dataMap.put("PROGRAM HASH ID", pid);
                dataMap.put("NOTIFICATION URL", notification);
                dataMap.put("BALANCE", balance);
                dataMap.put("CORPORATE PROCESSOR", corporateProcessor);
                dataMap.put("PROCESSOR NAME", processor);
                dataMap.put("PROCESSOR BALANCE TYPE", processorBal);
                dataMap.put("PROCESSOR CARD TYPE", processorCardType);
                dataMap.put("STATUS", status);
                resultantData.add(dataMap);
            }
            CommonUtil.generateExcelSheet(headers, resultantData, resultantFileName, response);
        }
    }

    private Object fetchProgramList(Program programDetails, String role) {
        final List<Program> programList = programRepository.findAll(programSearchSpecs(programDetails, role));
        log.info("# of programs: {}", programList.size());
        return programList;
    }

    private Specification<Program> programSearchSpecs(Program programDetails, String role) {
        Specification<Program> spec = null;

        try {
            spec = (Program, mq, mb) -> mb.equal(mb.literal(1), 1);
            if(!Roles.ROLE_ADMIN.toString().equals(role)  &&  programDetails.getProgramHashId()!=null){
                try {
                    long prId =  programDetails.getId();
                    spec = spec.and((Program, mq, mb) -> mb.equal(Program.get("id"), prId));

                }catch (Exception e){
                    throw new BizException(BizErrors.APPLICATION_ERROR.getValue (), "Internal server error. Please try again after sometimes");
                }
            }

        } catch (Exception e) {

            throw new BizException(BizErrors.APPLICATION_ERROR.getValue(), "Internal server error. Please try again after sometimes");
        }
        return spec;
    }

    @Override
    public void downloadClientList(HttpServletRequest request, HttpServletResponse response, String programUrl, Map<String, String> requestParams) {
        final String role = requestParams.get("role");
        Program programDetails = productService.getProductDetailsByHostUrl(programUrl);

        Object object = fetchClientList(programDetails, role);
        if (object instanceof List<?>) {
            String resultantFileName = "Client_List.xlsx";
            final List<ClientDetails> clientDetailsList = (List<ClientDetails>) object;
            if (clientDetailsList.size() == 0) {
                throw new BizException("No Client list found!");
            }
            final List<Map<String, String>> resultantData = new ArrayList<Map<String, String>>();
            final List<String> headers = new ArrayList<String>();
            headers.add("FIRST NAME");
            headers.add("LAST NAME");
            headers.add("MOBILE NUMBER");
            headers.add("EMAIL");
            headers.add("ADDRESS");
            headers.add("CREATED DATE");
            headers.add("ID TYPE");
            headers.add("ID NUMBER");
            headers.add("CLIENT ID");
            headers.add("STATUS");

            for (ClientDetails clientDetails : clientDetailsList) {
                final Map<String, String> dataMap = new HashMap<String, String>();
                final String firstName = StringUtils.isNotEmpty(clientDetails.getUsers().getFullName()) ? clientDetails.getUsers().getFullName() : "NA";
                //final String lastName = StringUtils.isNotEmpty(clientDetails.getUsers().getLastName()) ? clientDetails.getUsers().getLastName() : "NA";
                final String mobile = StringUtils.isNotEmpty(clientDetails.getMobile()) ? clientDetails.getMobile() : "NA";
                final String email = StringUtils.isNotEmpty(clientDetails.getEmail()) ? clientDetails.getEmail() : "NA";
                final String address = StringUtils.isNotEmpty(clientDetails.getAddress()) ? clientDetails.getAddress() : "NA";
                final String createdAt = StringUtils.isNotEmpty(String.valueOf(clientDetails.getCreatedAt())) ? String.valueOf(clientDetails.getCreatedAt()) : "NA";
                final String idType = StringUtils.isNotEmpty(clientDetails.getIdType()) ? clientDetails.getIdType() : "NA";
                final String userHash = StringUtils.isNotEmpty(clientDetails.getUsers().getUserHashId()) ? clientDetails.getUsers().getUserHashId() : "NA";
                final String status = StringUtils.isNotEmpty(clientDetails.getUsers().getStatus().getValue()) ? clientDetails.getUsers().getStatus().getValue() : "NA";

                dataMap.put("FIRST NAME", firstName);
                //dataMap.put("LAST NAME", lastName);
                dataMap.put("MOBILE NUMBER", mobile);
                dataMap.put("EMAIL", email);
                dataMap.put("ADDRESS", address);
                dataMap.put("CREATED DATE", createdAt);
                dataMap.put("ID TYPE", idType);
                dataMap.put("CLIENT ID", userHash);
                dataMap.put("STATUS", status);
                resultantData.add(dataMap);
            }
            CommonUtil.generateExcelSheet(headers, resultantData, resultantFileName, response);
        }
    }

    private Object fetchClientList(Program programDetails, String role) {
        final List<ClientDetails> clientDetails = clientDetailsRepository.findAll(clientSearchSpecs(programDetails, role));
        log.info("# of clients: {}", clientDetails.size());
        return clientDetails;
    }

    private Specification<ClientDetails> clientSearchSpecs(Program programDetails, String role) {
        Specification<ClientDetails> spec = null;
        try {
            spec = (ClientDetails, mq, mb) -> mb.equal(mb.literal(1), 1);
            if(!Roles.ROLE_ADMIN.toString().equals(role)  &&  programDetails.getProgramHashId()!=null){
                try {
                    long prId =  programDetails.getId();
                    spec = spec.and((Program, mq, mb) -> mb.equal(Program.get("id"), prId));

                }catch (Exception e){
                    throw new BizException(BizErrors.APPLICATION_ERROR.getValue (), "Internal server error. Please try again after sometimes");
                }
            }
        } catch (Exception e) {

            throw new BizException(BizErrors.APPLICATION_ERROR.getValue(), "Internal server error. Please try again after sometimes");
        }
        return spec;
    }
}
