package com.cards.zokudo.services.impl;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import com.cards.zokudo.dto.request.CreateRoleDto;
import com.cards.zokudo.entities.Privilege;
import com.cards.zokudo.entities.Role;
import com.cards.zokudo.enums.Roles;
import com.cards.zokudo.enums.Status;
import com.cards.zokudo.exceptions.BizException;
import com.cards.zokudo.exceptions.CustomRuntimeException;
import com.cards.zokudo.repositories.PrivilegeRepository;
import com.cards.zokudo.repositories.RoleRepository;
import com.cards.zokudo.services.RoleService;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class RoleServiceImpl implements RoleService {

	PrivilegeRepository privilegeRepo;
	RoleRepository roleRepo;

	@Autowired
	public RoleServiceImpl(PrivilegeRepository privilegeRepo,RoleRepository roleRepo) {
		this.privilegeRepo = privilegeRepo;
		this.roleRepo = roleRepo;
	}
	
	@Override
	public ResponseEntity<?> getPrivileges(String role) {

		log.info("Inside getPrivileges(): with role "+role);
		role = role.toUpperCase();
		List<Privilege> privileges = null;
		try {
		Role roleEntity = roleRepo.findByName(role);
		log.info("role : "+role);
		 privileges = roleEntity.getPrivileges().stream().collect(Collectors.toList());
		log.info("Privileges : "+privileges.toString());	
		}catch(Exception e) {
			log.error("Error while fetching privileges : "+e.getMessage());
			throw new BizException(e.getMessage());
		}
		return new ResponseEntity<>(privileges, HttpStatus.OK);
	}

	@Override
	public ResponseEntity<?> validateRoleName(String roleName) {
		
		if(null != roleName) {
			roleName = "_"+roleName.toUpperCase();
			if(roleRepo.findByRoleNameIgnoreCase(roleName) > 0)
				return new ResponseEntity<>("Matched",HttpStatus.OK);
		}
		return new ResponseEntity<>("Not Matched",HttpStatus.OK);
	}

	@Override
	public ResponseEntity<?> addRole(CreateRoleDto dto,String createdRole) {

		log.info("Inside addRole method with values: "+dto);
		String roleName = dto.getRoleName();
		String errorMsg = "";
		boolean errorFlag = false;

		if(roleName == null) {
			errorMsg = "Role name should not be empty.";
			errorFlag = true;
		}
		if(dto.getPrivileges() == null) {
			errorMsg = "Atleast one privilege should be selected.";
			errorFlag = true;
		}
		if(roleRepo.findByRoleNameIgnoreCase(roleName) >0) {
			errorMsg = "Role already Exists.";
			errorFlag = true;
		}
		if(errorFlag) {
			log.error(errorMsg);
			throw new CustomRuntimeException(errorMsg, HttpStatus.BAD_REQUEST);
		}
		try {
			
			
			Role role = new Role();
			
			if(createdRole.equals(Roles.ROLE_CLIENT.toString())) {
				roleName = "ROLE_SUB_CLIENT_"+roleName.toUpperCase();
				role.setCreatedBy("Client");
				role.setUpdatedBy("Client");
				role.setCategory(Roles.valueOf(createdRole));
			}
			else if(createdRole.equals(Roles.ROLE_DISTRIBUTOR.toString())) {
				roleName = "ROLE_SUB_DIST_"+roleName.toUpperCase();
				role.setCreatedBy("Distributor");
				role.setUpdatedBy("Distributor");
				role.setCategory(Roles.valueOf(createdRole));
			}
			List<Privilege> privilegeList = privilegeRepo.getPrivileges(dto.getPrivileges());
			log.info("Privilege list : "+privilegeList);
			Set<Privilege> privilegeSet = privilegeList.stream().collect(Collectors.toSet());
			log.info("Privilege Set : "+privilegeSet);
			
			role.setPrivileges(privilegeSet);
			role.setName(roleName);
			role.setStatus(Status.UNASSIGNED);
			roleRepo.save(role);
		}catch(Exception e) {
			log.info("Exception while adding role: "+e.getMessage());
			throw new BizException("Exception while adding role");
		}
		return new ResponseEntity<>("Role Added", HttpStatus.OK);
	}

	/**	
	 * List<String> is returned instead of List<Roles> to avoid unnecessary data Eagerly fetched by role entity
	 * 
	 *
	 * return example : 
	    ["1:ROLE_CLIENT",
    	"2:ROLE_ADMIN",
    	"3:ROLE_CLIENT_ADMIN"]
	
	 */
	@Override
	public ResponseEntity<?> getRoles(String portalRole) {
		log.info("Inside getRoles(): ");
		try {
			String createdBy = "";
			List<Role> roles = null;
			if(portalRole.equals(Roles.ROLE_DISTRIBUTOR.toString())) {
				 createdBy = "Distributor";
				 roles =  roleRepo.findAllRolesByCreatedBy(createdBy);
			}else if(portalRole.equals(Roles.ROLE_CLIENT.toString())) {
				createdBy = "Client";
				roles =  roleRepo.findAllRolesByCreatedBy(createdBy);
			}else {
				roles = roleRepo.findAll();
			}
			
			log.info("Fetched roles: "+roles.size());
			List<String> roleResponse = new ArrayList<>();
			for (Role role : roles) {
				roleResponse.add(role.getId()+":"+role.getName());
			}
			return new ResponseEntity<>(roleResponse,HttpStatus.OK);
		}catch(Exception e) {
			log.error(e.getMessage());
			throw new BizException("Exception while fetching role");
		}
	}
	

}
