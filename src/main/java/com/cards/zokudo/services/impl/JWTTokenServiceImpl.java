package com.cards.zokudo.services.impl;

import com.cards.zokudo.dto.response.AuthenticationAndAuthorizationDTO;
import com.cards.zokudo.exceptions.BadRequestException;
import com.cards.zokudo.exceptions.BizException;
import com.cards.zokudo.exceptions.ForbiddenException;
import com.cards.zokudo.response.TokenResponseDTO;
import com.cards.zokudo.services.TokenService;
import com.cards.zokudo.services.UserService;
import com.cards.zokudo.util.AESEncryption;
import com.cards.zokudo.util.CustomBcryptPasswordEncoder;
import com.cards.zokudo.util.SecurityUtil;
import com.google.common.collect.Maps;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.crypto.spec.SecretKeySpec;
import javax.servlet.http.HttpServletRequest;
import javax.validation.constraints.NotBlank;
import io.jsonwebtoken.*;

import java.nio.charset.StandardCharsets;
import java.util.*;
import io.jsonwebtoken.SignatureAlgorithm;
import org.joda.time.LocalDateTime;

@Service
@Slf4j
public class JWTTokenServiceImpl implements TokenService {

    private final String AUTHORIZATION_TOKEN_PREFIX = "Bearer ";
    private static final SignatureAlgorithm signatureAlgorithm = SignatureAlgorithm.HS256;
    private static final int TOKEN_EXPIRY_DURATION_IN_MIN = 30*2*4;
    private static final String JWT_TOKEN_ISSUER = "ODINMO";
    private static final String AUTHORIZATION_SUBJECT = "Authorization Token";
    private final String secretKey;
    private final SecretKeySpec secretKeySpec;
    private final String applicationLevelUserName;
    private final UserService userService;

    @Autowired
    public JWTTokenServiceImpl(@Value("${spring.security.user.password}") final String secretKey,@Value("${applicationLevel.user.name}") final String applicationLevelUserName,
                               final UserService userService){
        this.secretKey = Base64.getEncoder().encodeToString(secretKey.getBytes());
        this.secretKeySpec = new SecretKeySpec(secretKey.getBytes(StandardCharsets.UTF_8), signatureAlgorithm.getJcaName());
        this.applicationLevelUserName = applicationLevelUserName;
        this.userService = userService;
    }
    @Override
    public TokenResponseDTO createToken(HttpServletRequest request) {
        log.info("Token creation initated ");

        final String authorizationHeader = request.getHeader("Authorization");
        final String programUrl  = request.getHeader("programUrl");
        if(StringUtils.isEmpty(authorizationHeader)){
            log.error("Authorization Header cannot be empty.");
            throw new BadRequestException("Authorization Header is Mandatory.");
        }
        if(StringUtils.isEmpty(programUrl)){
            log.error("Program URL is mandatory");
            throw new BadRequestException("Program URL is Mandatory.");
        }
        final String[] tokenSecrete = getUserNamePassword(authorizationHeader);
        if (tokenSecrete.length != 2) {
            String error = "Either Username or Password is Missing";
            log.error(error);
            throw new BizException("Username and password is mandatory!");
        }
        final String username = tokenSecrete[0];
        final String password = tokenSecrete[1];
        //TODO : add program url.
        List<AuthenticationAndAuthorizationDTO> usernameDTOs = userService.getUserAuthDetailsInternal(username,programUrl);
        if(usernameDTOs.isEmpty()){
            log.error("Username :{} is invalid ",username);
            throw new ForbiddenException("Invalid username!");
        }
        if (!CustomBcryptPasswordEncoder.getBcryptPasswordEncoder().matches(password, usernameDTOs.get(0).getPassword())) {
            log.error("Password is invalid for username :{} ",username);
            throw new ForbiddenException("invalid credentials!");
        }
        Map<String,Object> payload = new HashMap<>();
        //payload.put("username",);
        payload.put("username", AESEncryption.encrypt(username,AESEncryption.secretKey));
        payload.put("password", AESEncryption.encrypt(password,AESEncryption.secretKey));

        return  TokenResponseDTO.builder()
               .authorizationToken(generateJwtToken(username, Maps.newHashMap()))
               .build();
    }

    public String generateJwtToken(@NotBlank(message = "username is mandatory") final String username, final Map<String, Object> payload) {
        try {
            return AUTHORIZATION_TOKEN_PREFIX.concat(Jwts
                    .builder()
                    .setHeader(getJwtHeader(signatureAlgorithm.getValue()))
                    .setId(UUID.randomUUID().toString())
                    .setSubject(AUTHORIZATION_SUBJECT)
                    .setIssuedAt(new Date())
                    .setIssuer(JWT_TOKEN_ISSUER)
                    .setAudience(username)
                    .setExpiration(getJwtExpiryDate())
                    .addClaims(getPayload(payload))
                    .signWith(signatureAlgorithm,secretKey)
                    .compact());

        } catch (final BizException e) {
            log.error(e.getMessage(), e);
            throw e;
        } catch (final Exception e) {
            log.error(e.getMessage(), e);
            throw new BizException("Unable to generate token", e.getMessage());
        }
    }

    private Map<String, Object> getJwtHeader(final String algorithm) {
        final HashMap<String, Object> headers = Maps.newHashMap();
        headers.put("typ", "JWT");
        headers.put("alg", algorithm);
        return headers;
    }

    private Date getJwtExpiryDate() {
        return LocalDateTime.now().plusMinutes(TOKEN_EXPIRY_DURATION_IN_MIN).toDate();
    }

    private Map<String, Object> getPayload(final Map<String, Object> payload) {
        if (Objects.isNull(payload)) {
            return Maps.newHashMap();
        }
        return payload;
    }

    private String[] getUserNamePassword(String authorizationHeader) {
        if (StringUtils.isEmpty(authorizationHeader)) {
            throw new BizException("Unauthorized access!");
        }
        return (new String(Base64.getDecoder().decode((authorizationHeader.replaceAll("Basic ", ""))))).split(":");

    }

}
