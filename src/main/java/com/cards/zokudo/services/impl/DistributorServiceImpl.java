package com.cards.zokudo.services.impl;

import static com.cards.zokudo.util.CommonUtil.generateSixDigitNumericString;
import static com.cards.zokudo.util.CommonUtil.parseDate;
import static com.cards.zokudo.util.CommonUtil.randomString;
import static com.cards.zokudo.util.CommonUtil.uploadImage;

import java.text.ParseException;
import java.util.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cards.zokudo.exceptions.ApiException;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.validator.routines.EmailValidator;
import org.codehaus.jettison.json.JSONObject;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import com.cards.zokudo.dto.request.DistributorDTO;
import com.cards.zokudo.dto.response.ClientOnboardResponseDTO;
import com.cards.zokudo.entities.DistributorEntity;
import com.cards.zokudo.entities.Program;
import com.cards.zokudo.entities.Role;
import com.cards.zokudo.entities.Users;
import com.cards.zokudo.entities.UsersHasProgram;
import com.cards.zokudo.enums.BizErrors;
import com.cards.zokudo.enums.Roles;
import com.cards.zokudo.enums.S3FilePostfix;
import com.cards.zokudo.enums.Status;
import com.cards.zokudo.enums.UserType;
import com.cards.zokudo.exceptions.BizException;
import com.cards.zokudo.repositories.DistributorRepository;
import com.cards.zokudo.repositories.ProgramRepository;
import com.cards.zokudo.repositories.RoleRepository;
import com.cards.zokudo.repositories.UsersHasProgramRepository;
import com.cards.zokudo.repositories.UsersRepository;
import com.cards.zokudo.response.ApiResponse;
import com.cards.zokudo.services.DistributorService;
import com.cards.zokudo.services.WalletService;
import com.cards.zokudo.util.CommonUtil;
import com.cards.zokudo.util.Constants;
import com.cards.zokudo.util.CustomBcryptPasswordEncoder;
import com.google.common.collect.Sets;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class DistributorServiceImpl implements DistributorService {

    private final ProgramRepository programRepository;
    private final RoleRepository roleRepository;
    private final UsersRepository usersRepository;
    private final DistributorRepository distributorRepository;
    private final UsersHasProgramRepository userHasProgramRepository;
    private final WalletService walletService;

    public DistributorServiceImpl(ProgramRepository programRepository,
                                  RoleRepository roleRepository,
                                  UsersRepository usersRepository,
                                  DistributorRepository distributorRepository,
                                  UsersHasProgramRepository userHasProgramRepository,
                                  WalletService walletService) {
        this.programRepository = programRepository;
        this.roleRepository = roleRepository;
        this.usersRepository = usersRepository;
        this.distributorRepository = distributorRepository;
        this.userHasProgramRepository = userHasProgramRepository;
        this.walletService = walletService;
    }

    @Override
    public ApiResponse<Object> onBoardDistributor(DistributorDTO dto, HttpServletRequest request, String programUrl) {



        validateDistributor(dto);
        distributorOnboardingProcess(dto, programUrl, request);
        ApiResponse<Object> response = ApiResponse.builder()
                .code("200")
                .message("Success")
                .body(dto.getFullName())
                .build();
        return response;
    }

    /**	
     * @param: dateRange should be in dd/MM/yyyy- dd/MM/yyyy format
     * @param : role from Role Enums are allowed.
     */
    @Override
    public Object distributorList(DistributorDTO dto,String role,String programUrl) {
        Page<DistributorEntity> distributorEntities = null;
        try {
            String dateRange=dto.getDateRange();
            String page = dto.getPage();
            String size =dto.getSize();
            Program program = programRepository.findByHostUrl(programUrl);
            Date startDate = null, endDate = null;
            if (!StringUtils.isBlank(dateRange)) {
                String[] dateArray = dateRange.split("-");

                startDate = CommonUtil.dateFormatterSlashWithTime.parse(dateArray[0] + CommonUtil.startTime);
                endDate = CommonUtil.dateFormatterSlashWithTime.parse(dateArray[1] + CommonUtil.endTime);
            }
            Sort sort = new Sort(Sort.Direction.DESC, "createdAt");
            Pageable pageable = PageRequest.of(Integer.parseInt(page), Integer.parseInt(size), sort);
            distributorEntities = distributorRepository.findAll(distributorSpecification(dto,program.getProgramHashId(), role, startDate, endDate), pageable);
           

        } catch (Exception e) {
            log.error(e.getMessage());
            throw new BizException("Unable to fetch list distributor");
        }
        return distributorEntities;
    }




    private Specification<DistributorEntity> distributorSpecification(DistributorDTO dto,String programHashId, String role, Date startDate, Date endDate) {
        Specification<DistributorEntity> spec = null;

        try {
            spec = (distDetails, mq, mb) -> mb.equal(mb.literal(1), 1);
            if (startDate != null) {
                spec = spec.and((distDetails, cq, cb) -> cb.between(distDetails.get("createdAt"), startDate, endDate));
            }
            if (Roles.ROLE_CLIENT_DIST.toString().equals(role)) {
                spec = spec.and((distDetails, mq, mb) -> mb.equal(distDetails.get("programHashId"), programHashId));
            }
            if(StringUtils.isNotEmpty(dto.getCompanyName())){
                spec =spec.and((distDetails, mq, mb) ->mb.like(distDetails.get("companyName"),dto.getCompanyName()+"%"));
            }
            if(StringUtils.isNotEmpty(dto.getMobile())){
                spec =spec.and((distDetails, mq, mb) ->mb.equal(distDetails.get("mobile"),dto.getMobile()));
            }
            if(StringUtils.isNotEmpty(dto.getEmail())){
                spec =spec.and((distDetails, mq, mb) ->mb.like(distDetails.get("email"),dto.getEmail()+"%"));
            }

        } catch (Exception e) {
            throw new BizException(BizErrors.APPLICATION_ERROR.getValue(), "Internal server error. Please try again after sometimes");
        }
        return spec;
    }

    private void distributorOnboardingProcess(DistributorDTO dto, String programUrl, HttpServletRequest request) {
        Program programDetails = programRepository.findByHostUrl(programUrl);

        if (programDetails == null)
            if (StringUtils.isBlank(dto.getAddress()))
                throw new BizException(BizErrors.APPLICATION_ERROR.getValue(), "invalid program!");


        Users user = createUser(dto, request, programDetails);

        DistributorEntity distEntity = createDistributor(dto, user, programDetails.getProgramHashId());
        log.info("distributor created : " + distEntity);
        assignProgramToUser(user,programDetails);
        log.info("User hash ID and program assigned to user successfully!");
        log.info("Setting up default commission for distributor! ");
        walletService.addDefaultComm(distEntity.getId(),programDetails.getId(), programUrl);
        log.info("default comm added successfully");
    }
    
	private void validateDistributor(DistributorDTO dto) {

        if (StringUtils.isBlank(dto.getAddress()))
            throw new BizException(BizErrors.APPLICATION_ERROR.getValue(), "address should not be empty");

        if (StringUtils.isBlank(dto.getEmail()))
            throw new BizException(BizErrors.APPLICATION_ERROR.getValue(), "email should not be empty");

        if (!EmailValidator.getInstance().isValid(dto.getEmail()))
            throw new BizException(BizErrors.APPLICATION_ERROR.getValue(), "enter valid email id.");


        if (StringUtils.isBlank(dto.getCompanyName()))
            throw new BizException(BizErrors.APPLICATION_ERROR.getValue(), "company name should not be empty");

        if (StringUtils.isBlank(dto.getMobile()))
            throw new BizException(BizErrors.APPLICATION_ERROR.getValue(), "mobile number should not be empty");


        if (!CommonUtil.isValidMobileNumber(dto.getMobile()))
            throw new BizException(BizErrors.INVALID_VALUE.getValue(), "enter valid mobile number");

        if (StringUtils.isBlank(dto.getNameOfDirector()))
            throw new BizException(BizErrors.INVALID_VALUE.getValue(), "name of director should not be empty!");

        if (StringUtils.isBlank(dto.getCitizenship()))
            throw new BizException(BizErrors.INVALID_VALUE.getValue(), "enter Citizenship!");

        if (StringUtils.isBlank(dto.getDobOfDirector()))
            throw new BizException(BizErrors.INVALID_VALUE.getValue(), "dob should not be empty!");

        Users users = usersRepository.findByEmail(dto.getEmail());
        if (users != null) {
            throw new BizException(BizErrors.BAD_REQUEST_ERROR.getValue(), "email already exist, please use other email id");
        }

        users = usersRepository.findByMobile(dto.getMobile());
        if (users != null) {
            throw new BizException(BizErrors.BAD_REQUEST_ERROR.getValue(), "contact number already present");
        }

        DistributorEntity distributorEntity = distributorRepository.findByCompanyName(dto.getCompanyName());
        if (distributorEntity != null) {
            throw new BizException(BizErrors.BAD_REQUEST_ERROR.getValue(), "entity name already present");
        }

         distributorEntity = distributorRepository.findByEmail(dto.getEmail());

        if (distributorEntity != null) {
            throw new BizException(BizErrors.BAD_REQUEST_ERROR.getValue(), "email id already present, please enter other email");
        }

        distributorEntity = distributorRepository.findByMobile(dto.getMobile());
        if (distributorEntity != null) {
            throw new BizException(BizErrors.BAD_REQUEST_ERROR.getValue(), "contact number already present");
        }
    }

    private Users createUser(DistributorDTO dto, HttpServletRequest request, Program programDetails) {
        try {

            Users users = new Users();
            users.setFullName(dto.getFullName());
            users.setMobile(dto.getMobile());
            users.setUsername(dto.getEmail());
            users.setEmail(dto.getEmail());
            users.setChannelId(programDetails.getProgramHashId());
            users.setUserHashId(randomString(10, "DIST"));
            users.setMobileToken(generateSixDigitNumericString());
            users.setStatus(Status.ACTIVE);
            users.setCreatedBy(programDetails.getCreatedBy());
            users.setOtpGenerationTime(new Date(System.currentTimeMillis()));
            users.setPassword(CustomBcryptPasswordEncoder.getBcryptPasswordEncoder().encode(dto.getPassword()));
            users.setProductType(UserType.DISTRIBUTOR);
            Role role = roleRepository.findByName(String.valueOf(Roles.ROLE_DISTRIBUTOR));
            Set<Role> roles = Sets.newHashSet();
            roles.add(role);
            users.setUserRoles(roles);
            usersRepository.save(users);
            return users;
        } catch (Exception e) {
            throw new BizException(BizErrors.BAD_REQUEST_ERROR.getValue(), "failed to update the distributor in user table");
        }
    }

    private DistributorEntity createDistributor(DistributorDTO dto, Users users, String programHashId) {
        try {
            log.info("Updating distributor details");
            DistributorEntity distributorObj = new DistributorEntity();
            distributorObj.setAddress(dto.getAddress());
            distributorObj.setEmail(dto.getEmail());
            distributorObj.setCompanyName(dto.getCompanyName());
            distributorObj.setMobile(dto.getMobile());
            distributorObj.setUserHashId(users.getUserHashId());
            distributorObj.setCreatedBy(users.getCreatedBy());
            distributorObj.setProgramHashId(programHashId);
            distributorObj.setAddress(dto.getAddress());
            distributorObj.setNameOfDirector(dto.getNameOfDirector());
            distributorObj.setCitizenship(dto.getCitizenship());
            distributorObj.setDobOfDirector(parseDate(dto.getDobOfDirector()));
            distributorObj.setGstCertificate(uploadImage(dto.getGstCertificate(), dto.getMobile() + S3FilePostfix.GST, Constants.DIST_DOCUMENT_DIR));
            distributorObj.setCompanyPan(uploadImage(dto.getCompanyPan(), dto.getMobile() + S3FilePostfix.COMPANY_PAN, Constants.DIST_DOCUMENT_DIR));
            distributorObj.setCopyOfDirectorBack(uploadImage(dto.getCopyOfDirectorBack(), dto.getMobile() + S3FilePostfix.COMPANY_PAN, Constants.DIST_DOCUMENT_DIR));
            distributorObj.setCopyOfDirectorFront(uploadImage(dto.getCopyOfDirectorFront(), dto.getMobile() + S3FilePostfix.COMPANY_PAN, Constants.DIST_DOCUMENT_DIR));
            distributorObj.setProjectionCommitment(uploadImage(dto.getProjectionCommitment(), dto.getMobile() + S3FilePostfix.COMPANY_PAN, Constants.DIST_DOCUMENT_DIR));
            distributorObj.setStatus(Status.ACTIVE);
            distributorObj.setProgramHashId(programHashId);
            distributorObj.setDistributorHashId(UUID.randomUUID().toString());
            return distributorRepository.save(distributorObj);
        } catch (ParseException e) {
            log.error(e.getMessage());
            usersRepository.delete(users);
            throw new BizException(BizErrors.BAD_REQUEST_ERROR.getValue(), "Invalid dobOfDirector");
        } catch (Exception e) {
            log.error(e.getMessage());
            usersRepository.delete(users);
            throw new BizException(BizErrors.BAD_REQUEST_ERROR.getValue(), "failed to update the distributor details");
        }
    }

    private void assignProgramToUser(Users user, Program programDetails) {
    	try {
    		//log.info("Assign program to user: "+user+" "+programDetails);
        	UsersHasProgram userHasProgram = new UsersHasProgram();
        	userHasProgram.setProgram(programDetails);
        	userHasProgram.setUsers(user);
        	userHasProgram.setStatus(Status.ACTIVE);
        	userHasProgram.setCreatedBy("mssadmin");
        	userHasProgram.setUpdatedBy("mssadmin");
        	log.info("userHasProgram : "+userHasProgram);
        	userHasProgramRepository.save(userHasProgram);
        	log.info("program assigned successfully"+userHasProgram);
    	}catch(Exception e) {
    		log.error("Error while assigning program to the user. Reason: "+e.getMessage());
    		throw new BizException("Error during program assignment.", e.getMessage());
    	}
	}

	@Override
	public Object getDistributorById(String distributorId) {
		try {
			log.info("Inside distributorById ; Id"+distributorId);
			if(distributorId == null || distributorId == "") {
				log.error("Missing Distributor's ID ");
				throw new BizException("Distributor ID not found ");
			}
			Long id = Long.parseLong(distributorId);
			return processOptionalDistributor(distributorRepository.findById(id));
			
		}catch(Exception e) {
			log.error("Error while fetching distributor By Id ");
			throw new BizException("Unable to fetch Distributor By id: "+distributorId);
		}
	}

	@Override
	public ApiResponse<Object> updateDistributor(DistributorDTO dto, String programUrl) {
		try {
			
	
			log.info("Initiating Entity creation for distributor DTO Object: ");
			DistributorEntity distributor = processOptionalDistributor(distributorRepository.findById(dto.getId()));

			distributor.setAddress(dto.getAddress());
			distributor.setCompanyName(dto.getCompanyName());
			distributor.setMobile(dto.getMobile());
			distributor.setAddress(dto.getAddress());
			distributor.setNameOfDirector(dto.getNameOfDirector());
			distributor.setCitizenship(dto.getCitizenship());
			distributor.setDobOfDirector(parseDate(dto.getDobOfDirector()));
			distributor.setCopyOfDirectorFront(uploadImage(dto.getCopyOfDirectorFront(), dto.getMobile() + S3FilePostfix.DIRECTOR_FRONT, Constants.DIST_DOCUMENT_DIR));
			distributor.setCopyOfDirectorBack(uploadImage(dto.getCopyOfDirectorBack(), dto.getMobile() + S3FilePostfix.DIRECTOR_BACK, Constants.DIST_DOCUMENT_DIR));
			distributor.setGstCertificate(uploadImage(dto.getGstCertificate(), dto.getMobile() + S3FilePostfix.GST, Constants.DIST_DOCUMENT_DIR));
			distributor.setCompanyPan(uploadImage(dto.getCompanyPan(), dto.getMobile() + S3FilePostfix.COMPANY_PAN, Constants.DIST_DOCUMENT_DIR));
			distributor.setProjectionCommitment(uploadImage(dto.getProjectionCommitment(), dto.getMobile() + S3FilePostfix.COMPANY_PAN, Constants.DIST_DOCUMENT_DIR));
			distributor.setStatus(Status.ACTIVE);
			log.info("Distributor Updated Successfully ");
			distributorRepository.save(distributor);
			
			 ClientOnboardResponseDTO clientOnboardResponseDTO = new ClientOnboardResponseDTO();
		        clientOnboardResponseDTO.setUserHashId(distributor.getUserHashId());
		        clientOnboardResponseDTO.setClientId(distributor.getId());
		        log.info("ClientRespo - " + clientOnboardResponseDTO);
		        ApiResponse<Object> response = ApiResponse.builder()
		                .code("200")
		                .message("Success")
		                .body(clientOnboardResponseDTO)
		                .build();
		        
			return response;
		}catch(Exception e) {
			log.error("Error while updating distributor ! "+e.getMessage());
			throw new BizException("Error occurred during distributor onboarding. ");
		}
	}
	
	private DistributorEntity processOptionalDistributor(Optional<DistributorEntity> distributor) {
		log.info("Inside processOptionalDistributor(): "); 
		
		if(distributor.isPresent())
			return distributor.get();
		else {
			log.error("Distributor Not Found");
			throw new BizException("Distributor Not Found");
		}
	}

	@Override
	public Object getDistributorByProgramHash(String programUrl) {
		Program programDetails = programRepository.findByHostUrl(programUrl);
		log.info("Inside getDistributorByProgramHashId");
		try {
			List<DistributorEntity> distributors = distributorRepository.findByProgramHashId(programDetails.getProgramHashId());
			return distributors;
		}catch(Exception e) {
			log.error("error while fetching distributor by program hash"+e.getMessage());
			throw new BizException("Error while fetching distributor");
		}
	}

	@Override
	public Object getDistributorList(String programUrl,String role,String loginUserHashId) {
		
		List<DistributorEntity> distributorEntities = null;
		Program programDetails = programRepository.findByHostUrl(programUrl);
		log.info("Inside getDistributorList");
		try {
			distributorEntities = distributorRepository.findAll(distributorSpecificationForAllRoles(programDetails.getProgramHashId(),role,loginUserHashId));
			return distributorEntities;
		}catch(Exception e) {
			log.error("error while fetching distributor by program hash"+e.getMessage());
			throw new BizException("Error while fetching distributor");
		}
	}

	@Override
	public DistributorEntity getDistribtorByUserHashId(String userHashId) {
		log.info("Inside getDistribtorByUserHashId() : userhash id"+userHashId);
		try {
			DistributorEntity distributor = distributorRepository.findByUserHashId(userHashId);
			return distributor;
		}catch(Exception e) {
			throw new  BizException("Error while fetching distributor by user hash id");
		}
	}

    @Override
    public void distributorListForDownload(Map<String, String> requestParams, HttpServletRequest request, HttpServletResponse response, String programUrl) {

        List<DistributorEntity> distributorEntities = null;
        try{
            final String role         = requestParams.get("role");
            final String companyName = requestParams.get("companyName");
            final  String mobile      = requestParams.get("mobile");
            final  String email       = requestParams.get("email");
            final String dateRange    = requestParams.get("dateRange");
            Program program = programRepository.findByHostUrl(programUrl);
            Date startDate = null, endDate = null;
            if (!StringUtils.isBlank(dateRange)) {
                String[] dateArray = dateRange.split("-");

                startDate = CommonUtil.dateFormatterSlashWithTime.parse(dateArray[0] + CommonUtil.startTime);
                endDate = CommonUtil.dateFormatterSlashWithTime.parse(dateArray[1] + CommonUtil.endTime);
            }
            DistributorDTO dto =new DistributorDTO();
            dto.setCompanyName(companyName);
            dto.setMobile(mobile);
            dto.setEmail(email);
            distributorEntities = distributorRepository.findAll(distributorSpecification(dto,program.getProgramHashId(), role, startDate, endDate));
            processForDistributorReportDownload(distributorEntities,response);
        }catch(Exception e){
            log.error(e.getMessage());
            throw new BizException("Unable to fetch list distributor "+ e.getMessage());
        }
    }

    private void processForDistributorReportDownload(List<DistributorEntity> distributorEntities, HttpServletResponse response) {
        if(distributorEntities!=null){
            String resultantFileName = "Distributor_Report.xlsx";
            log.info("--> Filename : " + resultantFileName + " Initiated..");
            final List<DistributorEntity> distributorEntitiesList =(List<DistributorEntity>) distributorEntities;
            if(distributorEntitiesList.size()==0){
             //   throw new BizException("NO distributor list");
                throw new ApiException(String.valueOf(HttpStatus.NOT_FOUND.value()), "NO distributor list", HttpStatus.NOT_FOUND);

            }
            final List <Map<String,String>> resultantData = new ArrayList<Map<String,String>>();
            final List<String> headers =new ArrayList<String>();
            headers.add("Company Name");
            headers.add("Mobile Number");
            headers.add("Email");
            headers.add("Name Of Director");
            headers.add("Citizenship");
            headers.add("Dob Of Director");
            headers.add("Status");
            for(DistributorEntity distributorEntitie :distributorEntitiesList) {
                final Map<String, String> dataMap = new HashMap<String, String>();
                final String companyName = StringUtils.isNotEmpty(distributorEntitie.getCompanyName()) ? distributorEntitie.getCompanyName() : "NA";
                dataMap.put("Company Name", companyName);
                dataMap.put("Mobile Number", StringUtils.isNotEmpty(distributorEntitie.getMobile()) ? distributorEntitie.getMobile() : "NA");
                dataMap.put("Email", StringUtils.isNotEmpty(distributorEntitie.getEmail())?distributorEntitie.getEmail() : "NA");
                dataMap.put("Name Of Director",StringUtils.isNotEmpty(distributorEntitie.getNameOfDirector())?distributorEntitie.getNameOfDirector():"NA");
                dataMap.put("Citizenship", StringUtils.isNotEmpty(distributorEntitie.getCitizenship())?distributorEntitie.getCitizenship():"NA");
                dataMap.put("Dob Of Director", StringUtils.isNotEmpty(String.valueOf(distributorEntitie.getDobOfDirector())) ? String.valueOf(distributorEntitie.getDobOfDirector()) : "NA");
                dataMap.put("Status", StringUtils.isNotEmpty(String.valueOf(distributorEntitie.getStatus())) ? String.valueOf(distributorEntitie.getStatus()) : "NA");
                resultantData.add(dataMap);
            }
            log.info("--> Headers Added For excel added : " + headers + " DataMap : " + resultantData);
            CommonUtil.generateExcelSheet(headers, resultantData, resultantFileName, response);
        }
    }


    private Specification<DistributorEntity> distributorSpecificationForAllRoles(String programHashId, String role,String loginUserHashId) {
        Specification<DistributorEntity> spec = null;

        try {
            spec = (distDetails, mq, mb) -> mb.equal(mb.literal(1), 1);
            if (Roles.ROLE_CLIENT_DIST.toString().equals(role) || Roles.ROLE_CLIENT_DIST.toString().equals(role)) {
                spec = spec.and((distDetails, mq, mb) -> mb.equal(distDetails.get("programHashId"), programHashId));
            }
            if (Roles.ROLE_DISTRIBUTOR.toString().equals(role)) {
                spec = spec.and((distDetails, mq, mb) -> mb.equal(distDetails.get("userHashId"), loginUserHashId));
            }
            /*if (Roles.ROLE_AGENT.toString().equals(role)) {
                spec = spec.and((distDetails, mq, mb) -> mb.equal(distDetails.get("userHashId"), loginUserHashId));
            }*/

        } catch (Exception e) {
            throw new BizException(BizErrors.APPLICATION_ERROR.getValue(), "Internal server error. Please try again after sometimes");
        }
        return spec;
    }
	
}
