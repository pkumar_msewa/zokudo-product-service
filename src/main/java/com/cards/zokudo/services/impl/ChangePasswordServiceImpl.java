package com.cards.zokudo.services.impl;

import com.cards.zokudo.entities.PasswordHistory;
import com.cards.zokudo.entities.Users;
import com.cards.zokudo.enums.BizErrors;
import com.cards.zokudo.exceptions.BadRequestException;
import com.cards.zokudo.exceptions.BizException;
import com.cards.zokudo.repositories.PasswordHistoryRepository;
import com.cards.zokudo.repositories.URLIdentifierRepository;
import com.cards.zokudo.repositories.UsersRepository;
import com.cards.zokudo.request.ChangePasswordRequest;
import com.cards.zokudo.response.ApiResponse;
import com.cards.zokudo.services.ChangePasswordService;
import com.cards.zokudo.util.AESEncryption;
import com.cards.zokudo.util.CommonUtil;
import com.cards.zokudo.util.Constants;
import com.cards.zokudo.util.CustomBcryptPasswordEncoder;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.client.Client;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

@Service
public class ChangePasswordServiceImpl implements ChangePasswordService {

    @Autowired
    URLIdentifierRepository urlIdentifierRepository;
    @Autowired
    UsersRepository usersRepository;

    @Autowired
    PasswordHistoryRepository passwordHistoryRepository;

    @Autowired
    @Qualifier(value = "client")
    Client client;


    public ChangePasswordServiceImpl(URLIdentifierRepository urlIdentifierRepository,
                                     UsersRepository usersRepository, PasswordHistoryRepository passwordHistoryRepository) {
        this.urlIdentifierRepository = urlIdentifierRepository;
        this.usersRepository = usersRepository;
        this.passwordHistoryRepository = passwordHistoryRepository;
    }

    @Override
    public ApiResponse execute(ChangePasswordRequest changePasswordRequest, String programUrl, HttpServletRequest request,String loggedInUserHashId) {


        changePasswordRequest.setNewPassword(AESEncryption.getDecodedPassword(changePasswordRequest.getNewPassword()));
        changePasswordRequest.setConfirmPassword(AESEncryption.getDecodedPassword(changePasswordRequest.getConfirmPassword()));
        changePasswordRequest.setExistingPassword(AESEncryption.getDecodedPassword(changePasswordRequest.getExistingPassword()));

        Users user = usersRepository.findByUserHashId(loggedInUserHashId);

        validateExistingPassword(changePasswordRequest, user);

        updatePassword(changePasswordRequest, user);

        return ApiResponse.builder().build();

    }

    private void updatePassword(ChangePasswordRequest changePasswordRequest, Users user) {
        user.setPassword(CustomBcryptPasswordEncoder.getBcryptPasswordEncoder().encode(changePasswordRequest.getConfirmPassword()));
        usersRepository.save(user);
        savePasswordHistory(user);
    }


    private void validateExistingPassword(ChangePasswordRequest changePasswordRequest, Users user) {

        if(StringUtils.isEmpty(changePasswordRequest.getMobileToken())) {
            throw new BadRequestException(BizErrors.BAD_REQUEST_ERROR.getValue(), "Otp not found");
        }

        if(!changePasswordRequest.getMobileToken().equals(user.getMobileToken())) {
            throw new BadRequestException(BizErrors.BAD_REQUEST_ERROR.getValue(), "Incorrect Otp");
        }

        if (changePasswordRequest.getNewPassword() == null || changePasswordRequest.getNewPassword().length() < 8)
            throw new BadRequestException(BizErrors.BAD_REQUEST_ERROR.getValue(), "Please enter minimum 8 character length password");

        if (!changePasswordRequest.getNewPassword().equals(changePasswordRequest.getConfirmPassword()))
            throw new BadRequestException(BizErrors.BAD_REQUEST_ERROR.getValue(), "new password and confirm password mismatch");

        if (!CustomBcryptPasswordEncoder.getBcryptPasswordEncoder().matches(changePasswordRequest.getExistingPassword(), (user.getPassword())))
            throw new BadRequestException(BizErrors.BAD_REQUEST_ERROR.getValue(), "Old password is not correct");

        if (changePasswordRequest.getNewPassword().length() > 20)
            throw new BadRequestException(BizErrors.BAD_REQUEST_ERROR.getValue(), "Max length of password should not exceed 20 characters");

        if (!Constants.PASSPATTERN.matcher(changePasswordRequest.getNewPassword()).matches())
            throw new BadRequestException(BizErrors.BAD_REQUEST_ERROR.getValue(), "Password must be alphanumeric with lowercase ,uppercase and special character");

        List<PasswordHistory> history = passwordHistoryRepository.findByUsersOrderByCreatedAtDesc(user);
        if (history != null && history.size() > 0) {
            for (PasswordHistory passwordHistory : history) {
                if (CustomBcryptPasswordEncoder.getBcryptPasswordEncoder().matches(changePasswordRequest.getNewPassword(), passwordHistory.getPassword())) {
                    throw new BizException(BizErrors.BAD_REQUEST_ERROR.getValue(), "Please enter the password that is unique from last 3 passwords");
                }
            }
        }
        if (!validCaptcha(changePasswordRequest.getCaptchaResponse()))
            throw new BadRequestException(BizErrors.BAD_REQUEST_ERROR.getValue(), "invalid captcha");

    }

    private void savePasswordHistory(Users newUser) {
        try {
            List<PasswordHistory> history = passwordHistoryRepository.findByUsersOrderByCreatedAtDesc(newUser);
            if (history != null && history.size() > 2) {
                passwordHistoryRepository.delete(history.get(history.size() - 1));
            }
            PasswordHistory password = new PasswordHistory();
            password.setPassword(newUser.getPassword());
            password.setUsers(newUser);
            passwordHistoryRepository.save(password);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private boolean validCaptcha(String captchaValue) {
        try {
            Response response = client.target("https://www.google.com/recaptcha/api/siteverify")
                    .queryParam("secret", "6Leg9OAUAAAAAC80sj48H6A2F8XTY4bwTr1SeI4F")
                    .queryParam("response", captchaValue).request().header("accept", MediaType.APPLICATION_JSON).get();

            if (response.getStatus() == 200)
                return true;

        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

}









