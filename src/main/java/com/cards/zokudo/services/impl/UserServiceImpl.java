package com.cards.zokudo.services.impl;

import com.cards.zokudo.dto.request.LoginDTO;
import com.cards.zokudo.dto.request.UserDTO;
import com.cards.zokudo.dto.response.ApiError;
import com.cards.zokudo.dto.response.AuthenticationAndAuthorizationDTO;
import com.cards.zokudo.dto.response.PageAndPrivilegeDTO;
import com.cards.zokudo.entities.LoginLogs;
import com.cards.zokudo.entities.PageIdentifier;
import com.cards.zokudo.entities.Program;
import com.cards.zokudo.entities.Role;
import com.cards.zokudo.entities.Users;
import com.cards.zokudo.entities.UsersHasProgram;
import com.cards.zokudo.enums.BizErrors;
import com.cards.zokudo.enums.Roles;
import com.cards.zokudo.enums.Status;
import com.cards.zokudo.enums.UserType;
import com.cards.zokudo.exceptions.BadRequestException;
import com.cards.zokudo.exceptions.BizException;
import com.cards.zokudo.repositories.*;
import com.cards.zokudo.response.ApiResponse;
import com.cards.zokudo.response.UserDetailsResponse;
import com.cards.zokudo.services.UserService;
import com.cards.zokudo.util.AESEncryption;
import com.cards.zokudo.util.CommonUtil;
import com.cards.zokudo.util.Constants;

import com.cards.zokudo.util.CustomBcryptPasswordEncoder;
import com.google.common.collect.Lists;

import lombok.extern.slf4j.Slf4j;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.client.Client;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.*;

@Slf4j
@Service
public class UserServiceImpl implements UserService {

    private final ProgramRepository programRepository;
    private final URLIdentifierRepository urlIdentifierRepository;
    private final PageIdentifierRepository pageIdentifierRepository;
    private final UsersRepository usersRepository;
    private final LoginLogsRepository loginLogsRepository;
    private final RoleRepository roleRepository;
    private final UsersHasProgramRepository usersHasProgramRepository;
    
    @Autowired
    @Qualifier(value = "client")
    Client client;



    @Autowired
    public UserServiceImpl(ProgramRepository programRepository,
                           URLIdentifierRepository urlIdentifierRepository, PageIdentifierRepository pageIdentifierRepository,
                           UsersRepository usersRepository, LoginLogsRepository loginLogsRepository,RoleRepository roleRepository,
                           UsersHasProgramRepository usersHasProgramRepository) {
        this.programRepository = programRepository;
        this.urlIdentifierRepository = urlIdentifierRepository;
        this.pageIdentifierRepository = pageIdentifierRepository;
        this.usersRepository = usersRepository;
        this.loginLogsRepository = loginLogsRepository;
        this.roleRepository = roleRepository;
        this.usersHasProgramRepository = usersHasProgramRepository;
    }


    @Override
    @Cacheable(cacheNames = "authenticationauthorizationdata")
    public List<AuthenticationAndAuthorizationDTO> getUserAuthDetails(HttpServletRequest request, HttpServletResponse response) {

        final List<Object[]> dataByUserName = urlIdentifierRepository.getUserAuthDetails(request.getHeader("username"), request.getHeader("program_url"));


        final List<AuthenticationAndAuthorizationDTO> dtos = Lists.newArrayList();

        dataByUserName.forEach(eachDto -> {
            AuthenticationAndAuthorizationDTO dto = new AuthenticationAndAuthorizationDTO();
            dto.setUserId(eachDto[0] != null ? eachDto[0].toString() : "");
            dto.setUserName(eachDto[1] != null ? eachDto[1].toString() : "");
            dto.setPassword(eachDto[2] != null ? eachDto[2].toString() : "");
            dto.setRoleId(eachDto[3] != null ? eachDto[3].toString() : "");
            dto.setRoleName(eachDto[4] != null ? eachDto[4].toString() : "");
            dto.setPrivilegeId(eachDto[5] != null ? eachDto[5].toString() : "");
            dto.setPrivilegeName(eachDto[6] != null ? eachDto[6].toString() : "");
            dto.setUrlId(eachDto[7] != null ? eachDto[7].toString() : "");
            dto.setUrl(eachDto[8] != null ? eachDto[8].toString() : "");
            dto.setProgramName(eachDto[9] != null ? eachDto[9].toString() : "");
            dto.setHostUrl(eachDto[10] != null ? eachDto[10].toString() : "");
            dtos.add(dto);
        });
        return dtos;
    }

    @Override
    public Object getPageDetails(HttpServletRequest request, HttpServletResponse response) {
        List<PageIdentifier> pageIdentifiers = pageIdentifierRepository.findAll();
        if (pageIdentifiers.isEmpty()) {
            throw new BizException("Page details not found!");
        }
        List<PageAndPrivilegeDTO> pageAndPrivilegeDTOList = new ArrayList<PageAndPrivilegeDTO>();
        for (PageIdentifier eachPageIdentifier : pageIdentifiers) {
            String pageName = eachPageIdentifier.getPageName();
            String privilegeName = (eachPageIdentifier.getPrivilege().getName()).replaceAll("ROLE_", "");

            PageAndPrivilegeDTO pageAndPrivilegeDTO = new PageAndPrivilegeDTO();
            pageAndPrivilegeDTO.setPageName(pageName);
            pageAndPrivilegeDTO.setPrivilegeName(privilegeName);

            pageAndPrivilegeDTOList.add(pageAndPrivilegeDTO);
        }
        return pageAndPrivilegeDTOList;
    }

    @Override
    public ResponseEntity<?> getUserOtpByUsername(LoginDTO loginDTO, HttpServletRequest request, HttpServletResponse response , String programUrl) {

        String decodedPass = AESEncryption.getRsaDecryptedData(loginDTO.getPassword());
        if (StringUtils.isBlank(loginDTO.getUserName()) || StringUtils.isBlank(decodedPass))
            throw new BadRequestException(BizErrors.BAD_REQUEST_ERROR.getValue(), "invalid username and password!");
/*if (!validCaptcha(loginDTO.getCaptchaResponse()))
                throw new BadRequestException(BizErrors.BAD_REQUEST_ERROR.getValue(), "invalid captcha");*/

        Users user = usersRepository.findByUsername(loginDTO.getUserName(), programUrl, Status.ACTIVE);
        if (user == null)
            throw new BadRequestException(BizErrors.BAD_REQUEST_ERROR.getValue(), "User not found!invalid username");
        if (user.isBlocked())
            throw new BadRequestException(BizErrors.BAD_REQUEST_ERROR.getValue(), "User is blocked");
        if (!Status.ACTIVE.equals(user.getStatus()))
            throw new BadRequestException("User is not active");


        BCryptPasswordEncoder bCryptPasswordEncoder = CustomBcryptPasswordEncoder.getBcryptPasswordEncoder();
        if (bCryptPasswordEncoder.matches(decodedPass, user.getPassword())) {
            // user.setMobileToken(CommonUtil.generateSixDigitNumericString());
            user.setMobileToken("123456");
            user.setOtpGenerationTime(new Date());
            usersRepository.save(user);
//                notificationService.customerServiceEmail(user.getUsername(), user.getFirstName(),
//                        user.getLastName(), "", EmailTemplate.CARD_LOGIN_OTP_EMAIL, user.getMobileToken(),
//                        commonErrorDTO);
//                notificationService.sendUserOTPSMS(user.getMobile(), user.getMobileToken());

            updateLogsStatusOnSuccessfullLogin(user);
            return new ResponseEntity<>(new ApiError(HttpStatus.OK, "One Time Passowrd(OTP) Sent<br>Please enter it to complete verification."), HttpStatus.OK);
        }
        //insert data into loginLog table for this user
        // check no of failed attempts , should not be more than 5 over all.
        // in autbots_config.user table , block field should be marked as true/1;
        accountLockout(user);
        throw new BadRequestException(BizErrors.BAD_REQUEST_ERROR.getValue(), "Invalid credentials!");
    }

    private void updateLogsStatusOnSuccessfullLogin(Users user) {
        List<LoginLogs> loginLogsOfUser = loginLogsRepository.findByUserIdAndStatus(user.getId(), Status.FAILED);
        loginLogsOfUser.forEach(loginLogs -> loginLogs.setStatus(Status.DELETED));
        loginLogsRepository.saveAll(loginLogsOfUser);
    }

    private void accountLockout(Users user) {

        LoginLogs loginLogs = new LoginLogs(user.getId(), Status.FAILED);
        loginLogsRepository.save(loginLogs);
        List<LoginLogs> loginLogsOfUser = loginLogsRepository.findByUserIdAndStatus(user.getId(), Status.FAILED);
        if(loginLogsOfUser.size() >= 4){
            user.setBlocked(true);
            user.setStatus(Status.BLOCKED);
            usersRepository.save(user);
        }

    }

    @Override
    public Users findByUsername(HttpServletRequest request, HttpServletResponse response) {

        Users users = usersRepository.findByUsername(request.getHeader("username") , request.getHeader("program_url") , Status.ACTIVE);

        return users ;
    }

    @Override
    public ResponseEntity<?> forgetPassword(LoginDTO loginDTO, HttpServletRequest request) {
        if (StringUtils.isBlank(loginDTO.getUserName())) {
            throw new BizException("User name  is mandatory!");
        }
        Users user = usersRepository.findByEmail(loginDTO.getUserName());
        if (user == null)
            throw new BadRequestException(BizErrors.BAD_REQUEST_ERROR.getValue(), "User not found!invalid username");
        if (user.isBlocked())
            throw new BadRequestException(BizErrors.BAD_REQUEST_ERROR.getValue(), "User is blocked");
        if (!Status.ACTIVE.equals(user.getStatus()))
            throw new BadRequestException("User is not active");
        if(user!=null) {
            user.setMobileToken(CommonUtil.generateSixDigitNumericString());
            user.setOtpGenerationTime(new Date());
            usersRepository.save(user);
            sendEmailToUser(user);
            return new ResponseEntity<>(new ApiError(HttpStatus.OK, "One Time Passowrd(OTP) Sent<br>Please enter it to complete verification."), HttpStatus.OK);
        }
        throw new BadRequestException(BizErrors.BAD_REQUEST_ERROR.getValue(), "Invalid credentials!");

    }

    @Override
    public Object getUserByHashId(String userHashId) {
        Users user = usersRepository.findByUserHashId(userHashId);
        if(Objects.isNull(user)){
            throw new BadRequestException(BizErrors.BAD_REQUEST_ERROR.getValue(), "User Not Found With Given Hash Id!");
        }else {
            user.setPassword(null);
            user.setMobileToken(null);
            user.setOtpGenerationTime(null);
            user.setUserRoles(null);
            return user;
        }
    }

    private boolean validCaptcha(String captchaValue) {
        try {
            Response response = client.target("https://www.google.com/recaptcha/api/siteverify")
                    .queryParam("secret", "6Leg9OAUAAAAAC80sj48H6A2F8XTY4bwTr1SeI4F")
                    .queryParam("response", captchaValue).request().header("accept" , MediaType.APPLICATION_JSON).get();

            if (response.getStatus() == 200)
                return true;

        }catch (Exception e){
            e.printStackTrace();
        }
        return false;
    }

    private void sendEmailToUser(Users user) {
        String body = constructBodyForOtp(user.getFullName(),null,user.getMobileToken());
        String[] to = {user.getEmail()};
        sendFromGMail(Constants.EMAIL_ID_FOR_ACTIVATION_AND_OTP, Constants.EMAIL_ID_PASSWORD_FOR_ACTIVATION_AND_OTP,to,"OTP Details", body);
    }

    private String constructBodyForOtp(String firstName, String lastName, String mobileToken) {
        StringBuffer header = new StringBuffer(
                "<!DOCTYPE html>\n" +
                        "<html>\n" +
                        "<head>\n" +
                        "</head>\n" +
                        "<body>\n" +
                        "Dear "+firstName+" "+lastName+",<br>" +
                        "Your One Time Password for verification is "+mobileToken+".<br>"+
                        "Regards,<br>"+
                        "Team Autobots.\n"+
                        "</body></html>"
        );
        return  header.toString();
    }

    private static void sendFromGMail(String from, String pass, String[] to, String subject, String body) {
        Properties props = System.getProperties();
        String host = "smtp.gmail.com";
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.smtp.host", host);
        props.put("mail.smtp.user", from);
        props.put("mail.smtp.password", pass);
        props.put("mail.smtp.port", "587");
        props.put("mail.smtp.auth", "true");

        Session session = Session.getInstance(props);
        MimeMessage message = new MimeMessage(session);

        try {
            message.setFrom(new InternetAddress(from));
            InternetAddress[] toAddress = new InternetAddress[to.length];

            // To get the array of addresses
            for (int i = 0; i < to.length; i++) {
                toAddress[i] = new InternetAddress(to[i]);
            }

            for (int i = 0; i < toAddress.length; i++) {
                message.addRecipient(Message.RecipientType.TO, toAddress[i]);
            }

            message.setSubject(subject);
            message.setText(body, "UTF-8", "html");
            Transport transport = session.getTransport("smtp");
            transport.connect(host, from, pass);
            transport.sendMessage(message, message.getAllRecipients());
            transport.close();
        } catch (AddressException ae) {
            ae.printStackTrace();
        } catch (MessagingException me) {
            me.printStackTrace();
        }

    }


    @Transactional
	@Override
	public ResponseEntity<?> createUser(UserDTO dto, String programUrl) {
    	
    	log.info("Inside createUser(): ");
    	try {
    		Program programDetails = programRepository.findByHostUrl(programUrl);
    		String errorMsg = "";
    		boolean errorFlag = false;
    		if(dto.getRoleName() == null) {
    			errorMsg = "Role Name cannot be null";
    			errorFlag = true;
    		}
    		if(errorFlag)
    			throw new BizException(errorMsg);
    		
    		Role role = roleRepository.findByName(dto.getRoleName().toUpperCase());
    		log.info("Role Fetched: "+role);
    		role.setStatus(Status.ACTIVE);
    		roleRepository.save(role);
    		
    		Users users = new Users();
    		Set<Role> roleSet = new HashSet<>();
    		roleSet.add(role);
    		users.setUserRoles(roleSet);
    		users.setBlocked(false);
    		users.setMobile(dto.getMobileNo());
    		users.setPassword(CustomBcryptPasswordEncoder.getBcryptPasswordEncoder().encode(dto.getMobileNo()));
    		users.setFullName(dto.getFullName());
    		users.setStatus(Status.ACTIVE);
    		users.setEmail(dto.getEmail());
    		users.setUsername(dto.getEmail());
    		users.setUserHashId(UUID.randomUUID().toString());
    		users.setCreatedBy(dto.getCreatedBy());
    		users.setProductCode(dto.getClientHashId());
    		if(dto.getRole().equals(Roles.ROLE_CLIENT.toString()))
    			users.setProductType(UserType.SUB_CLIENT);
    		else if(dto.getRole().equals(Roles.ROLE_DISTRIBUTOR.toString()))
    			users.setProductType(UserType.DISTRIBUTOR);
    		else if(dto.getRole().equals(Roles.ROLE_AGENT.toString()))
    			users.setProductType(UserType.AGENT);
    		else
    			users.setProductType(UserType.CLIENT_ADMIN);
    		users.setChannelId(dto.getLoggedInUser());
    		log.info("Saving users: "+users);
    		usersRepository.save(users);
    		assignPrograms(users,programDetails);	
    	}catch(Exception e) {
    		log.error("Exception : "+e.getMessage()+" "+e.getStackTrace());
    		throw new BizException("Error while creating user");
    	}
		return new ResponseEntity<>("User created Successfully", HttpStatus.OK);
	}
    
    private void assignPrograms(Users users, Program program) {
        log.info("Inside assign programs for UserId :{}, and programId :{}", users.getId(), program.getId());
        try {
            UsersHasProgram usersHasProgram = new UsersHasProgram();
            usersHasProgram.setUsers(users);
            usersHasProgram.setProgram(program);
            usersHasProgram.setStatus(Status.ACTIVE);
            usersHasProgramRepository.save(usersHasProgram);
            log.info("Program has been assgined to User :{}", users.getId());
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            log.error("Exception occurred while assigning the program");
            throw new BizException(BizErrors.APPLICATION_ERROR.getValue(), "error!while assigning program");
        }
    }

    public List<AuthenticationAndAuthorizationDTO> getUserAuthDetailsInternal(String username, String programUrl) {

        final List<Object[]> dataByUserName = urlIdentifierRepository.getUserAuthDetails(username, programUrl);

        final List<AuthenticationAndAuthorizationDTO> dtos = Lists.newArrayList();

        dataByUserName.forEach(eachDto -> {
            AuthenticationAndAuthorizationDTO dto = new AuthenticationAndAuthorizationDTO();
            dto.setUserId(eachDto[0] != null ? eachDto[0].toString() : "");
            dto.setUserName(eachDto[1] != null ? eachDto[1].toString() : "");
            dto.setPassword(eachDto[2] != null ? eachDto[2].toString() : "");
            dto.setRoleId(eachDto[3] != null ? eachDto[3].toString() : "");
            dto.setRoleName(eachDto[4] != null ? eachDto[4].toString() : "");
            dto.setPrivilegeId(eachDto[5] != null ? eachDto[5].toString() : "");
            dto.setPrivilegeName(eachDto[6] != null ? eachDto[6].toString() : "");
            dto.setUrlId(eachDto[7] != null ? eachDto[7].toString() : "");
            dto.setUrl(eachDto[8] != null ? eachDto[8].toString() : "");
            dto.setProgramName(eachDto[9] != null ? eachDto[9].toString() : "");
            dto.setHostUrl(eachDto[10] != null ? eachDto[10].toString() : "");
            dtos.add(dto);
        });
        return dtos;
    }



}
