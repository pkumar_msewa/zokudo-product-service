package com.cards.zokudo.services.impl;

import com.cards.zokudo.dto.request.IPWhiteListingDTO;
import com.cards.zokudo.dto.request.WebhookUrlDto;
import com.cards.zokudo.entities.*;
import com.cards.zokudo.enums.BizErrors;
import com.cards.zokudo.enums.ProgramTyps;
import com.cards.zokudo.enums.Roles;
import com.cards.zokudo.enums.Status;
import com.cards.zokudo.exceptions.BadRequestException;
import com.cards.zokudo.exceptions.BizException;
import com.cards.zokudo.repositories.*;
import com.cards.zokudo.response.ApiResponse;
import com.cards.zokudo.services.ProductService;
import com.cards.zokudo.services.WalletService;
import com.cards.zokudo.util.CommonUtil;
import com.google.common.collect.Maps;
import lombok.extern.slf4j.Slf4j;
import org.codehaus.jettison.json.JSONObject;

import org.apache.commons.lang3.StringUtils;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.util.MultiValueMap;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.servlet.http.HttpServletRequest;
import java.util.*;

@Slf4j
@Service
public class ProductServiceImpl implements ProductService {

    private final ProgramRepository programRepository;
    private final EntityManager entityManager;
    private final UsersRepository usersRepository;
    private final IPWhiteListingRepository ipWhiteListingRepository;
    private final BinRangeRepository binRangeRepository;
    private final SubBinRangeRepository subBinRangeRepository;
    private final WalletService walletService;
    private final ProgramHasBinRepository programHasBinRepository;


    public ProductServiceImpl(final ProgramRepository programRepository,
                              final EntityManager entityManager,
                              final UsersRepository usersRepository,
                              final IPWhiteListingRepository ipWhiteListingRepository,
                              final BinRangeRepository binRangeRepository,
                              final SubBinRangeRepository subBinRangeRepository,
                              final WalletService walletService,
                              final ProgramHasBinRepository programHasBinRepository) {
        this.programRepository = programRepository;
        this.entityManager = entityManager;
        this.usersRepository = usersRepository;
        this.ipWhiteListingRepository = ipWhiteListingRepository;
        this.binRangeRepository = binRangeRepository;
        this.subBinRangeRepository = subBinRangeRepository;
        this.walletService = walletService;
        this.programHasBinRepository = programHasBinRepository;
    }

    @Override
    public Program getProductDetailsByHostUrl(String program_url) {
        log.info("*** program_url :{}", program_url);
        Program program = programRepository.findByHostUrl(program_url);
        if (program == null)
            throw new BadRequestException(BizErrors.BAD_REQUEST_ERROR.getValue(), "invalid program url");
        return program;
    }

    @Override
    public Object updateProgramBalance(HttpServletRequest request, String programUrl) {

        String programHashId = request.getHeader("programHashId");
        String balance = request.getHeader("balance");

        Program program = programRepository.findByProgramHashId(programHashId);

        if (program == null)
            throw new BadRequestException(BizErrors.BAD_REQUEST_ERROR.getValue(), "invalid program hash id");
        try {
            program.setBalance(Double.parseDouble(balance));
            programRepository.save(program);
        } catch (NumberFormatException e) {
            log.error("invalid balance! could not update ");
        }
        Map<String, String> map = new HashMap<>();
        map.put("balance", balance);
        return map;
    }

    @Override
    public Object getProductDetailsById(String programUrl, String program_id) {
        return programRepository.findById(Long.parseLong(program_id));
    }

    @Override
    public ApiResponse getProgramBalance(String programId, String loggedInUserRole, String loggedInUserHashId) {

//        double balance = getBalanceOfProgram(programId, loggedInUserRole);
        double balance = getClientAccountBalance(programId, loggedInUserRole, loggedInUserHashId);

        return ApiResponse.builder().body(balance).build();
    }

    private double getClientAccountBalance(String programId, String loggedInUserRole, String loggedInUserHashId) {
        log.info("Inside Get Client Account Balance for programId :{}, loggedInUserRole :{}", programId, loggedInUserRole);
        Program program = programRepository.findById(Long.parseLong(programId));
        if (program == null) {
            return 0;
        }

        double balances = 0;
        if (Roles.ROLE_ADMIN.toString().equals(loggedInUserRole)) {
            balances = walletService.getClientAccountBalances("All", program.getHostUrl());
        } else if (Roles.ROLE_CLIENT.toString().equals(loggedInUserRole)) {
            balances = walletService.getClientAccountBalances(String.valueOf(program.getClientId()), program.getHostUrl());
        }
        if ((Roles.ROLE_CLIENT_DIST.toString().equals(loggedInUserRole) ||
                Roles.ROLE_DISTRIBUTOR.toString().equals(loggedInUserRole) ||
                Roles.ROLE_AGENT.toString().equals(loggedInUserRole)) &
                loggedInUserHashId != null) {

            balances = walletService.getDistributorAccountBalances(String.valueOf(program.getClientId()), program.getHostUrl(), loggedInUserRole, loggedInUserHashId);
        }

        return balances;
    }

    private double getBalanceOfProgram(String programId, String loggedInUserRole) {
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Double> query = criteriaBuilder.createQuery(Double.class);
        Root<Program> program = query.from(Program.class);
        query.select(criteriaBuilder.sum(program.get("balance")));
        Predicate equal = criteriaBuilder.equal(criteriaBuilder.literal(1), criteriaBuilder.literal(1));
        Predicate programHashId1 = !Roles.ROLE_ADMIN.toString().equals(loggedInUserRole) ? criteriaBuilder.equal(program.get("id"), programId) : null;

        query = programHashId1 != null ? query.where(equal, programHashId1) : query.where(equal);

        TypedQuery<Double> typedQuery = entityManager.createQuery(query);
        return typedQuery.getSingleResult();
    }

    public Object getProgramList() {
        try {
            List<Program> programs = programRepository.findAll();

            Map<String, List> map = Maps.newHashMap();
            map.put("programsList", programs);

            return map;
        } catch (Exception e) {
            throw new BizException(BizErrors.BAD_REQUEST_ERROR.getValue(), "error in geting program list");
        }
    }

    @Override
    public Object getProductById(String programHashId) {
        return programRepository.findByProgramHashId(programHashId);
    }

    @Override
    public ApiResponse<Object> ipWhiteList(IPWhiteListingDTO ipWhiteListingDTO, String programUrl) {

        validateRequest(ipWhiteListingDTO);

        whiteListIpAddress(ipWhiteListingDTO);

        return ApiResponse.builder().body("IP: " + ipWhiteListingDTO.getIpAddress() + " whitelisted successfully!").build();
    }

    @Override
    public ApiResponse<Object> getIPList(String page, String size) {

        Sort sort = new Sort(Sort.Direction.DESC, "createdAt");
        Pageable pageable = PageRequest.of(Integer.parseInt(page), Integer.parseInt(size), sort);
        Page<IPWhiteListing> ipWhiteListings = ipWhiteListingRepository.findAll(pageable);


        return ApiResponse.builder().body(ipWhiteListings).build();
    }

    @Override
    public ApiResponse<Object> getIpDetails(String ipAddress) {

        IPWhiteListing ipWhiteListing = ipWhiteListingRepository.findByIpAddress(ipAddress);
        if (ipWhiteListing == null)
            throw new BadRequestException(BizErrors.BAD_REQUEST_ERROR.getValue(), "ip address " + ipAddress + " not whitelisted");

        return ApiResponse.builder().body(ipWhiteListing).build();
    }

    @Override
    public ApiResponse<Object> ipBlackList(IPWhiteListingDTO ipWhiteListingDTO) {

        IPWhiteListing ipWhiteListing = ipWhiteListingRepository.findById(ipWhiteListingDTO.getId());
        if (ipWhiteListing == null)
            throw new BizException(BizErrors.APPLICATION_ERROR.getValue(), "invalid request!no data found");

        ipWhiteListing.setStatus(Status.getEnum(ipWhiteListingDTO.getStatus()));
        ipWhiteListingRepository.save(ipWhiteListing);

        return ApiResponse.builder().build();
    }


    private void whiteListIpAddress(IPWhiteListingDTO ipWhiteListingDTO) {
        try {
            IPWhiteListing ipWhiteListing = new IPWhiteListing();
            ipWhiteListing.setUserId(ipWhiteListingDTO.getUserId());
            ipWhiteListing.setIpAddress(ipWhiteListingDTO.getIpAddress());
            ipWhiteListing.setStatus(Status.ACTIVE);
            ipWhiteListing.setUserEmail(ipWhiteListingDTO.getUser().getUsername());
            ipWhiteListing.setContact(ipWhiteListingDTO.getUser().getMobile());
            ipWhiteListingRepository.save(ipWhiteListing);

        } catch (DataIntegrityViolationException e) {
            log.error(e.getMessage());
            throw new BizException(BizErrors.APPLICATION_ERROR.getValue(), "IP: " + ipWhiteListingDTO.getIpAddress() + "  already exist in Autobots System!");
        }

    }

    private void validateRequest(IPWhiteListingDTO ipWhiteListingDTO) {
        Users users = usersRepository.findById(ipWhiteListingDTO.getUserId());
        if (users == null)
            throw new BadRequestException(BizErrors.BAD_REQUEST_ERROR.getValue(), "invalid client selection");

        if (!CommonUtil.isValidInet4Address(ipWhiteListingDTO.getIpAddress()))
            throw new BadRequestException(BizErrors.BAD_REQUEST_ERROR.getValue(), "invalid ip address!please enter valid inetv4 address");
        ipWhiteListingDTO.setUser(users);
    }

    @Override
    public ApiResponse<Object> getBinNumber(String reloadableCardType) {
        BinRange binRange = binRangeRepository.findByProgramType(reloadableCardType);
        String bin = binRange.getBin();
        return ApiResponse.builder().body(bin).build();
    }

    @Override
    public ApiResponse<Object> getSubBinNumber() {
        log.info("fetching subBin range entries for GPR, GC and GC1 program BIN");

        try {
            //get subBin from subBin table
            List<SubBinRange> subBinRangeList = subBinRangeRepository.findAll();

            List<String> subBinRangeListForGPR = new ArrayList<>();
            List<String> subBinRangeListForGC = new ArrayList<>();
            List<String> subBinRangeListForGC1 = new ArrayList<>();
            Map<String, List<String>> subBinRangeMap = new HashMap<>();

            //get BIN entry for GPR and GC programs
            String binForGPR = binRangeRepository.findBinByProgramType("GPR");
            String binForGC = binRangeRepository.findBinByProgramType("GC");
            String binForGC1 = binRangeRepository.findBinByProgramType("GC1");

            //get list of assigned sub bin range for GPR type BIN
            List<String> asignedBinRangeForGPR = programHasBinRepository.findByProgramType(binForGPR);

            //get list of assigned sub bin range for GC type BIN
            List<String> asignedBinRangeForGC = programHasBinRepository.findByProgramType(binForGC);

            //get list of assigned sub bin range for GC1 type BIN
            List<String> asignedBinRangeForGC1 = programHasBinRepository.findByProgramType(binForGC1);

            for (SubBinRange subBinRange : subBinRangeList) {
                //add unassigned subBin to the subBinRangeListForGPR & subBinRangeListForGC list
                if (subBinRange.getStatus().equalsIgnoreCase("UNASSIGNED")) {
                    subBinRangeListForGPR.add(subBinRange.getSubBin());
                    subBinRangeListForGC.add(subBinRange.getSubBin());
                    subBinRangeListForGC1.add(subBinRange.getSubBin());
                } else {
                    //check the assigned subBin w.r.t BIN and add subBin which is not assigned for GPR BIN
                    String gprBinRangeToMatch = binForGPR + subBinRange.getSubBin();
                    boolean isGPRSubBinOccupied = isSubBinPresentInGPRBinHasProgram(asignedBinRangeForGPR, gprBinRangeToMatch);
                    if (!isGPRSubBinOccupied)
                        subBinRangeListForGPR.add(subBinRange.getSubBin());

                    //check the assigned subBin w.r.t BIN and add subBin which is not assigned for GC
                    String gcBinRangeToMatch = binForGC + subBinRange.getSubBin();
                    boolean isGCSubBinOccupied = isSubBinPresentInGCBinHasProgram(asignedBinRangeForGC, gcBinRangeToMatch);
                    if (!isGCSubBinOccupied)
                        subBinRangeListForGC.add(subBinRange.getSubBin());

                    //check the assigned subBin w.r.t BIN and add subBin which is not assigned for GC1
                    String gc1BinRangeToMatch = binForGC1 + subBinRange.getSubBin();
                    boolean isGC1SubBinOccupied = isSubBinPresentInGC1BinHasProgram(asignedBinRangeForGC1, gc1BinRangeToMatch);
                    if (!isGC1SubBinOccupied)
                        subBinRangeListForGC1.add(subBinRange.getSubBin());
                }

            }
            subBinRangeMap.put("SubBinRangeForGPR", subBinRangeListForGPR);
            subBinRangeMap.put("SubBinRangeForGC", subBinRangeListForGC);
            subBinRangeMap.put("SubBinRangeForGC1", subBinRangeListForGC1);

            log.info("subBIN range fetched successfully");
            log.info("subBinRangeListForGPR: size:{} \n AvailableSubBinsForGPRBin: {}", subBinRangeListForGPR.size(), subBinRangeListForGPR);
            log.info("subBinRangeListForGC: size:{}\n AvailableSubBinsForGCBin: {}" + subBinRangeListForGC.size(), subBinRangeListForGC);
            log.info("subBinRangeListForGC1: size:{}\n AvailableSubBinsForGC1Bin: {}" + subBinRangeListForGC1.size(), subBinRangeListForGC1);

            return ApiResponse.builder().body(subBinRangeMap).build();
        } catch (Exception e) {
            log.error("Error occurred while fetching sub bin range");
            throw new BadRequestException(BizErrors.BAD_REQUEST_ERROR.getValue(), "Problem in getting sub bin ranges" + e.getMessage());
        }
    }

    private boolean isSubBinPresentInGCBinHasProgram(List<String> asignedBinRangeForGC, String binRangeToMatch) {
        for (String binRange : asignedBinRangeForGC) {
            if (binRange.equals(binRangeToMatch)) {
                return true;
            }
        }
        return false;
    }

    private boolean isSubBinPresentInGC1BinHasProgram(List<String> asignedBinRangeForGC1, String binRangeToMatch) {
        for (String binRange : asignedBinRangeForGC1) {
            if (binRange.equals(binRangeToMatch)) {
                return true;
            }
        }
        return false;
    }

    private boolean isSubBinPresentInGPRBinHasProgram(List<String> asignedBinRangeForGPR, String binRangeToMatch) {
        for (String binRange : asignedBinRangeForGPR) {
            if (binRange.equals(binRangeToMatch)) {
                return true;
            }
        }
        return false;
    }

    @Override
    public ApiResponse<Object> getChannelDetails(String loggedInUserHashId, String programUrl) {

        Users users = usersRepository.findByUserHashId(loggedInUserHashId);

        if (users == null)
            throw new BizException(BizErrors.BAD_REQUEST_ERROR.getValue(), "invalid logged user!");

        return ApiResponse.builder().body(users.getChannelId()).build();


    }

    @Override
    public ApiResponse<Object> getSubBinByProgram(String programId) {
        try {
            log.info("** Fetch Bin by program ID ", programId);
            ProgramHasBin programHasBin = programHasBinRepository.findByProgramId(Long.parseLong(programId));
            log.info("** Bin Range fetched from DB : " + programHasBin);
            return ApiResponse.builder().body(programHasBin).build();
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            throw new BizException("Unable to fetch bin by program. " + e.getMessage());
        }
    }

    @Override
    public ApiResponse<Object> webhookUrlConfigure(WebhookUrlDto webhookUrlDto) {
        if (StringUtils.isBlank(webhookUrlDto.getWebhookUrl())) {
            throw new BizException("webhook url should not be empty.");
        }
        Program program = programRepository.findById(webhookUrlDto.getProgramId());
        if (program == null) {
            throw new BizException("program details not found to configure webhook url");
        }

        program.setWebhookUrl(webhookUrlDto.getWebhookUrl());
        programRepository.save(program);

        return ApiResponse.builder().body("Webhook Url configured successfully!").build();
    }

    @Override
    public ApiResponse getProgramBalancePartnerApi(String programUrl,String loggedInUserRole,String loginUserHashId ) {

        double balance = getProgramBalanceForPartner(programUrl,loggedInUserRole,loginUserHashId);
        return ApiResponse.builder().body(balance).build();
    }

    private Double getProgramBalanceForPartner(String programUrl,String loggedInUserRole, String loggedInUserHashId) {
           log.info("** Inside getProgramBalancePartnerApi method **");
        double balances = 0;
           try {
               Program program = getProductDetailsByHostUrl(programUrl);
               if ((Roles.ROLE_CLIENT_DIST.toString().equals(loggedInUserRole) ||
                       Roles.ROLE_DISTRIBUTOR.toString().equals(loggedInUserRole) ||
                       Roles.ROLE_AGENT.toString().equals(loggedInUserRole)) &
                       loggedInUserHashId != null) {
                   balances = walletService.getDistributorAccountBalances(String.valueOf(program.getClientId()), program.getHostUrl(), loggedInUserRole, loggedInUserHashId);
               }else if (Roles.ROLE_CLIENT.toString().equals(loggedInUserRole)) {
                   balances = walletService.getClientAccountBalances(String.valueOf(program.getClientId()), program.getHostUrl());
               }
           }catch (Exception e){
               log.error(e.getMessage(),e);
               throw new BizException("Invalid program login. "+e.getMessage());
           }
           return balances;
    }

}
