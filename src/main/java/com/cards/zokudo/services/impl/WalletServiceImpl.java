package com.cards.zokudo.services.impl;

import com.cards.zokudo.dto.response.CommonServiceResponse;
import com.cards.zokudo.exceptions.CustomRuntimeException;
import com.cards.zokudo.exceptions.ValidationException;
import com.cards.zokudo.interceptors.Interaction;
import com.cards.zokudo.repositories.ProgramRepository;
import com.cards.zokudo.services.WalletService;
import com.cards.zokudo.services.program.persist.ProgramRequestDTO;
import com.cards.zokudo.util.AppConstants;
import com.cards.zokudo.util.MessageConstants;
import com.cards.zokudo.util.SecurityUtil;
import com.cards.zokudo.util.UrlMetaData;
import lombok.extern.slf4j.Slf4j;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.Response;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@Service
@Slf4j
public class WalletServiceImpl implements WalletService {

    private final Client client;
    private final UrlMetaData urlMetaData;
    private final SecurityUtil securityUtil;
    private final ProgramRepository programRepository;

    @Autowired
    public WalletServiceImpl(@Qualifier(value = "client") Client client,
                             final UrlMetaData urlMetaData,
                             final SecurityUtil securityUtil, final ProgramRepository programRepository) {
        this.client = client;
        this.urlMetaData = urlMetaData;
        this.securityUtil = securityUtil;
        this.programRepository = programRepository;
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public CommonServiceResponse createDefaultPocket(final long programId, final long clientId, String programUrl) {
        CommonServiceResponse commonServiceResponse = new CommonServiceResponse();
        try {
            JSONObject request = new JSONObject();
            request.put("programId", programId);
            request.put("clientId", clientId);

            String str = urlMetaData.ADD_DEFAULT_POCKET.replaceAll("\\{\\}", programUrl);

            Response response = client.target(str)
                    .request()
                    .accept(MediaType.APPLICATION_JSON_VALUE)
                    .header(AppConstants.HEADER_X_REQUEST_ID, Interaction.getRequestId())
                    .header(AppConstants.AUTH_HEADER, securityUtil.getAuthorizationHeader())
                    .post(Entity.entity(request.toString(), MediaType.APPLICATION_JSON_VALUE));

            final String jsonResp = response.readEntity(String.class);

            if (response.getStatus() == 200) {
                commonServiceResponse.setCode(AppConstants.successCode);
                commonServiceResponse.setMessage(MessageConstants.POCKET_ADDED_SUCCESSFULLY);
            } else {
                commonServiceResponse.setCode(AppConstants.errorCode);
                commonServiceResponse.setMessage(jsonResp);
            }

        } catch (Exception ex) {
            log.error(ex.getMessage(), ex);
            throw new CustomRuntimeException(false, MessageConstants.ERROR_ADDING_POCKET_TO_CLIENT, HttpStatus.INTERNAL_SERVER_ERROR);
        }

        return commonServiceResponse;
    }

    @Override
    public void addCurrencyWithClient(final long clientId, final ProgramRequestDTO programDTO, String programUrl) {
        log.info("*** inside add currency for client :{}, programId :{}, currencyCode :{}", clientId, programDTO.getProgramId(), programDTO.getCurrency());
        final long startTime = System.currentTimeMillis();
        try {
            final JSONObject req = new JSONObject();
            req.put("clientId", clientId);
            req.put("programId", programDTO.getProgramId());
            req.put("currencyCode", programDTO.getCurrency());
//            req.put("allowedCurrencyCode", clientDTO.getAllowedCurrencyCode());

            String str = urlMetaData.ADD_CURRENCY.replaceAll("\\{\\}", programUrl);
            final Response response = client.target(str)
                    .request()
                    .accept(APPLICATION_JSON_VALUE)
                    .header(AppConstants.HEADER_X_REQUEST_ID, Interaction.getRequestId())
                    .header(AppConstants.AUTH_HEADER, securityUtil.getAuthorizationHeader())
                    .post(Entity.entity(req.toString(), APPLICATION_JSON_VALUE));

            if (response.getStatus() != HttpStatus.OK.value()) {
                throw new ValidationException("Unable to add currency");
            }
        } catch (ValidationException e) {
            log.error(e.getMessage(), e);
            throw e;
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            throw new ValidationException(e.getMessage(), e);
        } finally {
            log.info("Currency added with client time: {} millis", (System.currentTimeMillis() - startTime));
        }
    }

    @Override
    public void createClientAccount(long clientId, String programUrl) {
        final long startTime = System.currentTimeMillis();
        try {
            final JSONObject jsonObject = new JSONObject();
            jsonObject.put("clientId", clientId);

            String str = urlMetaData.ADD_CLIENT_ACCOUNT.replaceAll("\\{\\}", programUrl) + clientId;

            final Response response = client.target(str)
                    .request()
                    .header(AppConstants.HEADER_X_REQUEST_ID, Interaction.getRequestId())
                    .header(AppConstants.AUTH_HEADER, securityUtil.getAuthorizationHeader())
                    .accept(APPLICATION_JSON_VALUE).post(Entity.json("{}"));

            if (response.getStatus() != HttpStatus.OK.value()) {
                throw new ValidationException("Unable to add create client account");
            }
        } catch (ValidationException e) {
            log.error(e.getMessage(), e);
            throw e;
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            throw new ValidationException(e.getMessage(), e);
        } finally {
            log.info("Create client account time: {} millis", (System.currentTimeMillis() - startTime));
        }
    }


    @Override
    public void addDefaultFeesForClient(long clientId, long programId, String programUrl) {
        log.info("Adding default fees for clientId :{}, programId :{}", clientId, programId);
        final long startTime = System.currentTimeMillis();
        try {
            String str = urlMetaData.ADD_DEFAULT_FEES.replaceAll("\\{\\}", programUrl);

            final Response response = client.target(str)
                    .request()
                    .header(AppConstants.HEADER_X_REQUEST_ID, Interaction.getRequestId())
                    .header(AppConstants.AUTH_HEADER, securityUtil.getAuthorizationHeader())
                    .header("clientId", clientId)
                    .header("programId", programId)
                    .accept(APPLICATION_JSON_VALUE).get();

            if (response.getStatus() != HttpStatus.OK.value()) {
                throw new ValidationException("Unable to add default fees for the client");
            }
        } catch (ValidationException e) {
            log.error(e.getMessage(), e);
            throw e;
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            throw new ValidationException(e.getMessage(), e);
        } finally {
            log.info("Default Fees creation time: {} millis", (System.currentTimeMillis() - startTime));
        }
    }

    @Override
    public double getClientAccountBalances(String clientId, String programUrl) {
        final long startTime = System.currentTimeMillis();
        try {
            String str = urlMetaData.GET_CLIENT_ACCOUNT_BALANCES.replaceAll("\\{\\}", programUrl);

            final Response response = client.target(String.format(str, clientId))
                    .request()
                    .header(AppConstants.HEADER_X_REQUEST_ID, Interaction.getRequestId())
                    .header(AppConstants.AUTH_HEADER, securityUtil.getAuthorizationHeader())
                    .accept(APPLICATION_JSON_VALUE).get();

            if (response.getStatus() != HttpStatus.OK.value()) {
                throw new ValidationException("Unable to get client account balances");
            }

            final String jsonResp = response.readEntity(String.class);
            JSONObject jsonObject = new JSONObject(jsonResp);
            return jsonObject.getDouble("details");
        } catch (ValidationException e) {
            log.error(e.getMessage(), e);
            throw e;
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            throw new ValidationException(e.getMessage(), e);
        } finally {
            log.info("Get client account balance time: {} millis", (System.currentTimeMillis() - startTime));
        }
    }

    @Override
    public double getDistributorAccountBalances(String clientId, String programUrl, String loggedInUserRole, String loggedInUserHashId) {
        final long startTime = System.currentTimeMillis();
        try {
            String str = urlMetaData.GET_DISTRIBUTOR_ACCOUNT_BALANCES.replaceAll("\\{\\}", programUrl);

            final Response response = client.target(str)
                    .request()
                    .header("role", loggedInUserRole)
                    .header("clientId", clientId)
                    .header("loginUserHashId", loggedInUserHashId)
                    .header(AppConstants.HEADER_X_REQUEST_ID, Interaction.getRequestId())
                    .header(AppConstants.AUTH_HEADER, securityUtil.getAuthorizationHeader())
                    .accept(APPLICATION_JSON_VALUE).get();

            if (response.getStatus() != HttpStatus.OK.value()) {
                throw new ValidationException("Unable to get account balances");
            }

            final String jsonResp = response.readEntity(String.class);
            JSONObject jsonObject = new JSONObject(jsonResp);
            return jsonObject.getDouble("details");
        } catch (ValidationException e) {
            log.error(e.getMessage(), e);
            throw e;
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            throw new ValidationException(e.getMessage(), e);
        } finally {
            log.info("Get client account balance time: {} millis", (System.currentTimeMillis() - startTime));
        }
    }

    @Override
    public void addDefaultRevenueForProgram(long clientId, long programId, String programUrl) {

        log.info("Adding default revenue for clientId :{}, programId :{}", clientId, programId);
        final long startTime = System.currentTimeMillis();
        try {
            String str = urlMetaData.ADD_DEFAULT_REVENUE.replaceAll("\\{\\}", programUrl);

            final Response response = client.target(str)
                    .request()
                    .header(AppConstants.HEADER_X_REQUEST_ID, Interaction.getRequestId())
                    .header(AppConstants.AUTH_HEADER, securityUtil.getAuthorizationHeader())
                    .header("clientId", clientId)
                    .header("programId", programId)
                    .accept(APPLICATION_JSON_VALUE).get();

            if (response.getStatus() != HttpStatus.OK.value()) {
                throw new ValidationException("Unable to add default revenue for the client");
            }
        } catch (ValidationException e) {
            log.error(e.getMessage(), e);
            throw e;
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            throw new ValidationException(e.getMessage(), e);
        } finally {
            log.info("Default revenue creation time: {} millis", (System.currentTimeMillis() - startTime));
        }
    }

    @Override
    public void addDefaultComm(long distributorId, long programId, String programUrl) {


        log.info("Adding default commission for distributorId :{}, programId :{}", distributorId, programId);
        final long startTime = System.currentTimeMillis();
        try {
            String str = urlMetaData.ADD_DEFAULT_COMM.replaceAll("\\{\\}", programUrl);

            final Response response = client.target(str)
                    .request()
                    .header(AppConstants.HEADER_X_REQUEST_ID, Interaction.getRequestId())
                    .header(AppConstants.AUTH_HEADER, securityUtil.getAuthorizationHeader())
                    .header("distributorId", distributorId)
                    .header("programId", programId)
                    .accept(APPLICATION_JSON_VALUE).get();

            if (response.getStatus() != HttpStatus.OK.value()) {
                throw new ValidationException("Unable to add default commission for the distributor");
            }
        } catch (ValidationException e) {
            log.error(e.getMessage(), e);
            throw e;
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            throw new ValidationException(e.getMessage(), e);
        } finally {
            log.info("Default commission creation time: {} millis", (System.currentTimeMillis() - startTime));
        }
    }

    @Override
    public void addDefaultRetailerComm(long retailerId, long programId, String programUrl) {
        log.info("Adding default commission for retailerId :{}, programId :{}", retailerId, programId);
        final long startTime = System.currentTimeMillis();
        try {
            String str = urlMetaData.ADD_DEFAULT_RETAILER_COMM.replaceAll("\\{\\}", programUrl);

            final Response response = client.target(str)
                    .request()
                    .header(AppConstants.HEADER_X_REQUEST_ID, Interaction.getRequestId())
                    .header(AppConstants.AUTH_HEADER, securityUtil.getAuthorizationHeader())
                    .header("distributorId", retailerId)
                    .header("programId", programId)
                    .accept(APPLICATION_JSON_VALUE).get();

            if (response.getStatus() != HttpStatus.OK.value()) {
                throw new ValidationException("Unable to add default commission for the retailer");
            }
        } catch (ValidationException e) {
            log.error(e.getMessage(), e);
            throw e;
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            throw new ValidationException(e.getMessage(), e);
        } finally {
            log.info("Default commission creation time: {} millis", (System.currentTimeMillis() - startTime));
        }
    }

    @Override
    public void createAgentAccount(long agentId, long clientId, String programUrl) {
        final long startTime = System.currentTimeMillis();
        try {
            String str = urlMetaData.ADD_AGENT_ACCOUNT.replaceAll("\\{\\}", programUrl);
            final Response response = client.target(str)
                    .request()
                    .header(AppConstants.HEADER_X_REQUEST_ID, Interaction.getRequestId())
                    .header(AppConstants.AUTH_HEADER, securityUtil.getAuthorizationHeader())
                    .header("agentId", agentId)
                    .header("clientId", clientId)
                    .accept(APPLICATION_JSON_VALUE).post(Entity.json("{}"));

            if (response.getStatus() != HttpStatus.OK.value()) {
                throw new ValidationException("Unable to add create agent account");
            }
        } catch (ValidationException e) {
            log.error(e.getMessage(), e);
            throw e;
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            throw new ValidationException(e.getMessage(), e);
        } finally {
            log.info("Create client account time: {} millis", (System.currentTimeMillis() - startTime));
        }
    }

    @Override
    public void addDefaultClientCommissionForClientByProgram(long clientId, long programId, String programUrl) {

        log.info("Adding default commission for clientId :{}, programId :{}", clientId, programId);
        final long startTime = System.currentTimeMillis();
        try {
            String str = urlMetaData.ADD_DEFAULT_CLIENT_COMMISSION.replaceAll("\\{\\}", programUrl);

            final Response response = client.target(str)
                    .request()
                    .header(AppConstants.HEADER_X_REQUEST_ID, Interaction.getRequestId())
                    .header(AppConstants.AUTH_HEADER, securityUtil.getAuthorizationHeader())
                    .header("clientId", clientId)
                    .header("programId", programId)
                    .accept(APPLICATION_JSON_VALUE).get();

            if (response.getStatus() != HttpStatus.OK.value()) {
                throw new ValidationException("Unable to add default commission for the client");
            }
        } catch (ValidationException e) {
            log.error(e.getMessage(), e);
            throw e;
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            throw new ValidationException(e.getMessage(), e);
        } finally {
            log.info("Default client commission creation time: {} millis", (System.currentTimeMillis() - startTime));
        }
    }

    @Override
    public void addDefaultClientDiscountForClientByProgram(long clientId, long programId, String programUrl) {

        log.info("Adding default discount for clientId :{}, programId :{}", clientId, programId);
        final long startTime = System.currentTimeMillis();
        try {
            String str = urlMetaData.ADD_DEFAULT_CLIENT_DISCOUNT.replaceAll("\\{\\}", programUrl);

            final Response response = client.target(str)
                    .request()
                    .header(AppConstants.HEADER_X_REQUEST_ID, Interaction.getRequestId())
                    .header(AppConstants.AUTH_HEADER, securityUtil.getAuthorizationHeader())
                    .header("clientId", clientId)
                    .header("programId", programId)
                    .accept(APPLICATION_JSON_VALUE).get();

            if (response.getStatus() != HttpStatus.OK.value()) {
                throw new ValidationException("Unable to add default discount for the client");
            }
        } catch (ValidationException e) {
            log.error(e.getMessage(), e);
            throw e;
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            throw new ValidationException(e.getMessage(), e);
        } finally {
            log.info("Default client discount creation time: {} millis", (System.currentTimeMillis() - startTime));
        }
    }

    @Override
    public void addDefaultRetailerDiscount(long retailerId, long programId, String programUrl) {
        log.info("Adding default discount for retailerId :{}, programId :{}", retailerId, programId);
        final long startTime = System.currentTimeMillis();
        try {
            String str = urlMetaData.ADD_DEFAULT_RETAILER_DISCOUNT.replaceAll("\\{\\}", programUrl);

            final Response response = client.target(str)
                    .request()
                    .header(AppConstants.HEADER_X_REQUEST_ID, Interaction.getRequestId())
                    .header(AppConstants.AUTH_HEADER, securityUtil.getAuthorizationHeader())
                    .header("agentId", retailerId)
                    .header("programId", programId)
                    .accept(APPLICATION_JSON_VALUE).get();

            if (response.getStatus() != HttpStatus.OK.value()) {
                throw new ValidationException("Unable to add default discount for the retailer");
            }
        } catch (ValidationException e) {
            log.error(e.getMessage(), e);
            throw e;
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            throw new ValidationException(e.getMessage(), e);
        } finally {
            log.info("Default discount creation time: {} millis", (System.currentTimeMillis() - startTime));
        }
    }
}
