package com.cards.zokudo.services;

import com.cards.zokudo.dto.request.AgentDTO;
import com.cards.zokudo.response.ApiResponse;

import javax.servlet.http.HttpServletRequest;

public interface AgentInf {
    ApiResponse onBoardAgent(AgentDTO dto, HttpServletRequest request, String programUrl);

    Object agentList(AgentDTO dto, String role,String programUrl);

    Object executeBlockUnblock(AgentDTO agentDto);

    ApiResponse<?> getAgentDetails(String agentHashId);
	Object agentListByDsitributorHash(String header,String productType, String programUrl,String distributorHashId,String category);

	Object getRetailerList(String programUrl, String role,String loginUserHashId);
	
	Object getAgentById(String id,String programUrl);

    Object getAgentAndDistributorDetailsByAgentId(String id,String programUrl);

    Object getAgentByEmailId(String emailId);

    Object allRetailer();
}
