package com.cards.zokudo.enums;

public enum OnboardingState {

    CLEAR("CLEAR"),
    CLIENT("CLIENT"),
    PROGRAM("PROGRAM"),
    DISTRIBUTOR("DISTRIBUTOR"),
    COMMERCIAL("COMMERCIAL"),
    OTP("OTP"),
    ;

    private final String value;

    OnboardingState(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    @Override
    public String toString() {
        return value;
    }
}
