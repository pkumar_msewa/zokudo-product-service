package com.cards.zokudo.enums;


public enum BalanceTypeAtProcessor {

    SHARED("SHARED_BALANCE"), NON_SHARED("NON_SHARED_BALANCE");

    private final String value;

    BalanceTypeAtProcessor(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return value;
    }

    public String getValue() {
        return value;
    }

    public static BalanceTypeAtProcessor getEnum(String value) {
        if (value == null)
            throw new IllegalArgumentException();
        for (BalanceTypeAtProcessor v : values())
            if (value.equalsIgnoreCase(v.getValue()))
                return v;
        throw new IllegalArgumentException();
    }

}
