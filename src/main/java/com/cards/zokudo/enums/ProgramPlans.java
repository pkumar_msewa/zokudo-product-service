package com.cards.zokudo.enums;

public enum ProgramPlans {
    
	DEFAULT("DEFAULT"),
    DISTRIBUTOR("DISTRIBUTOR"),
    LENDING("LENDING");

    private final String value;

    ProgramPlans(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return value;
    }

    public String getValue() {
        return value;
    }

    public static ProgramPlans getEnum(String value) {

        if (value == null)
            throw new IllegalArgumentException();
        for (ProgramPlans v : values())
            if (value.equalsIgnoreCase(v.getValue()))
                return v;
        throw new IllegalArgumentException();
    }
}
