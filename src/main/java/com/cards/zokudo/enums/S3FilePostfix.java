package com.cards.zokudo.enums;

public enum S3FilePostfix {

    CLIENT_LOGO("CLIENT_LOGO"),
    DIRECTOR_FRONT("DIRECTOR_FRONT"),
    DIRECTOR_BACK("DIRECTOR_BACK"),
    DIRECTOR_ADDRESS_FRONT("DIRECTOR_ADDRESS_FRONT"),
    DIRECTOR_ADDRESS_BACK("DIRECTOR_ADDRESS_BACK"),
    GST("GST"),
    BOARD_RESOLUTION_FORM("BOARD_RESOLUTION_FORM"),
    COMPANY_PAN("COMPANY_PAN"),
    AGREEMENT_SOW("AGREEMENT_SOW"),
    PROGRAM_ONBOARDING_PLAN("PROGRAM_ONBOARDING_PLAN"),
    PROJECTION_COMMITMENT("PROJECTION_COMMITMENT"),
    INCORPORATION_CERTIFICATE("INCORPORATION_CERTIFICATE"),
    PENALTY_CLAUSE("PENALTY_CLAUSE"),
    COMMERCIAL_DOCUMENT("COMMERCIAL_DOCUMENT"),
    ;

    private final String value;

    S3FilePostfix(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    @Override
    public String toString() {
        return value;
    }
}
