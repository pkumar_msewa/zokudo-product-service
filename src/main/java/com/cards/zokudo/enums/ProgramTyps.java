package com.cards.zokudo.enums;

public enum ProgramTyps {

    PHYSICALGPR("PHYSICALGPR"),
    VIRTUALGPR("VIRTUALGPR"),
    PHYSICALGC("PHYSICALGC"),
    //PHYSICALGC1("PHYSICALGC1"),
    VIRTUALGC("VIRTUALGC"),
    //VIRTUALGC1("VIRTUALGC1"),
    PHY_VIRGPR("PHY_VIRGPR"),
    PHY_VIRGC("PHY_VIRGC");
    //PHY_VIRGC1("PHY_VIRGC1");

    private final String value;

    ProgramTyps(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return value;
    }

    public String getValue() {
        return value;
    }

    public static ProgramTyps getEnum(String value) {

        if (value == null)
            throw new IllegalArgumentException();
        for (ProgramTyps v : values())
            if (value.equalsIgnoreCase(v.getValue()))
                return v;
        throw new IllegalArgumentException();
    }

}
