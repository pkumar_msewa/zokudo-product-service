package com.cards.zokudo.enums;

public enum FeeFrequency {

    EACH_TRX("EACH_TRX"),
    MONTHLY("MONTHLY"),
    ONE_TIME("ONE_TIME");

    private final String value;

    FeeFrequency(final String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return value;
    }

    public String getValue() {
        return value;
    }

    public static FeeFrequency getEnum(String value) {
        if (value == null)
            throw new IllegalArgumentException();
        for (FeeFrequency v : values())
            if (value.equalsIgnoreCase(v.getValue()))
                return v;
        throw new IllegalArgumentException();
    }
}
