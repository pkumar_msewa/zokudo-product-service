package com.cards.zokudo.enums;

public interface UserType {

    String SUPER_ADMIN = "Super Admin";
    String SUPER_ADMIN_USER = "Super Admin User";
    String CLIENT_ADMIN = "Client Admin";
    String CLIENT_ADMIN_USER = "Client Admin User";
    String CUSTOMER_ADMIN = "Customer Admin";
    String PROGRAM_MANAGER = "Program Manager";
    String SUB_CLIENT = "Sub Client";
    String DISTRIBUTOR = "Distributor";
    String AGENT = "Agent";
}
