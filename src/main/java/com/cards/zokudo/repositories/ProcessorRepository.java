package com.cards.zokudo.repositories;

import com.cards.zokudo.entities.Processor;
import com.cards.zokudo.enums.Status;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ProcessorRepository extends JpaRepository<Processor, Long> {
    List<Processor> findByStatus(Status active);
}
