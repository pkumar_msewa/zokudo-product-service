package com.cards.zokudo.repositories;

import com.cards.zokudo.entities.Program;
import com.cards.zokudo.enums.ProgramPlans;
import org.codehaus.jettison.json.JSONObject;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

@Repository
public interface ProgramRepository extends JpaRepository<Program, Long>, JpaSpecificationExecutor<Program> {

    @Query("select p from Program p")
    Program findByTokenAndTokenSecret(String token, String token_secret);

    Program findByHostUrl(String program_url);

    Program findByProgramHashId(String programHashId);

    @Query("SELECT  p FROM Program p" +
            " INNER JOIN p.usersPrograms up " +
            " WHERE up.users.id = :userId")
    List<Program> findByClientId(@Param("userId") Long userId);

    Page<Program> findAllByCreatedAtBetween(Date startDate, Date endDate, Pageable pageable);

    @Query("SELECT p FROM Program p WHERE p.balance < 100000")
    List<Program> findAllProgramsBasedOnBalance();

    @Query("SELECT p.id, p.programName FROM Program p")
    List<Object> getProgramIdAndName();

    @Query("SELECT p.id, p.programName FROM Program p where p.id in (?1)")
    List<Object> getListOfProgramIdAndAndName(List<Long> ids );

    @Query("SELECT p.id, p.programName, p.programHashId, p.clientId FROM Program p where p.programPlan='DISTRIBUTOR'")
    List<Object> getProgramIdList();

    Program findById(long id);

    @Query("SELECT p FROM Program p WHERE clientId = :clientId")
    List<Program> findAllProgramByClientId(@Param("clientId") Long clientId);

    @Query("SELECT p FROM Program p WHERE programPlan =?1")
    List<Program> findAllProgramByProgramPlans(ProgramPlans programPlan);

    @Query("SELECT DISTINCT(p.clientId) FROM Program p WHERE p.programPlan in ('DEFAULT', 'LENDING')")
    List<Long> getListOfClientIds();

    @Query("SELECT DISTINCT(p.clientId) FROM Program p WHERE p.programPlan=?1")
    List<Long> getListOfSuperDistClientIds(ProgramPlans programPlan);

    @Query("SELECT p.id, p.programName, p.programHashId, p.clientId FROM Program p WHERE p.programPlan=?1 ")
    List<JSONObject> getListOfSuperDistPrograms(ProgramPlans programPlans);

    @Query("SELECT p.id, p.programName, p.programHashId, p.clientId FROM Program p WHERE p.programPlan=?1 ")
    List<Object> getListOfProgramsByProgramPlans(ProgramPlans programPlans);

    Page<Program> findByClientId(long clientId, Pageable pageable);

    Program findByProgramName(String programName);
}
