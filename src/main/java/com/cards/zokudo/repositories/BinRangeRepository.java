package com.cards.zokudo.repositories;

import com.cards.zokudo.entities.BinRange;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;


@Repository
public interface BinRangeRepository extends JpaRepository<BinRange, Long>{

    BinRange findByProgramType(String programType);

    @Query("select bin from BinRange b where b.programType=?1")
    String findBinByProgramType(String programType);
}
