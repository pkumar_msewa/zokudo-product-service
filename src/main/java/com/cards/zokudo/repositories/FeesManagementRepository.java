package com.cards.zokudo.repositories;

import com.cards.zokudo.entities.FeesManagement;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface FeesManagementRepository extends JpaRepository<FeesManagement, Long> {
}
