package com.cards.zokudo.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.cards.zokudo.entities.Role;

@Repository
public interface RoleRepository extends JpaRepository<Role, Long> {

    Role findByName(String valueOf);

    @Query("SELECT count(r.name) FROM Role r WHERE name like %:roleName ")
	int findByRoleNameIgnoreCase(@Param("roleName") String roleName);
    
    @Query("SELECT r FROM Role r WHERE r.createdBy=?1")
    List<Role> findAllRolesByCreatedBy(String role);
}
