package com.cards.zokudo.repositories;

import com.cards.zokudo.entities.UsersHasProgram;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UsersHasProgramRepository extends JpaRepository<UsersHasProgram , Long> {

}
