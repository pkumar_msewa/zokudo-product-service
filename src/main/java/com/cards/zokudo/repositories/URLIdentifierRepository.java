package com.cards.zokudo.repositories;

import com.cards.zokudo.entities.URLIdentifier;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
public interface URLIdentifierRepository extends JpaRepository<URLIdentifier, Long>, JpaSpecificationExecutor<URLIdentifier> {


    @Transactional(readOnly = true)
    @Query(value =
            "SELECT u.id as userId" +
            ", u.username as userName" +
            ", u.password as password" +
            ", r.id as roleId" +
            ", r.name as roleName" +
            ", p.id as privilegeId" +
            ", p.name as privilegeName" +
            ", ui.id as urlId" +
            ", ui.url as url" +
            ", pgm.hostUrl as hostUrl" +
            ", pgm.programName as programName" +
            " FROM URLIdentifier ui INNER JOIN ui.privilege p" +
            " INNER JOIN p.privilegeRoles r" +
            " INNER JOIN r.users u" +
            " INNER JOIN u.usersPrograms up" +
            " INNER JOIN up.program pgm" +
            " WHERE u.username = :username" +
            " AND pgm.hostUrl = :hostUrl")
    List<Object[]> getUserAuthDetails(@Param("username") final String username, @Param("hostUrl") final String hostUrl);


}
