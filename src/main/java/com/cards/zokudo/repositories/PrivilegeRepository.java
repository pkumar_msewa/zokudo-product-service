package com.cards.zokudo.repositories;

import com.cards.zokudo.entities.Privilege;

import java.util.List;
import java.util.Set;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface PrivilegeRepository extends JpaRepository<Privilege , Long> {

	@Query("SELECT p FROM Privilege p WHERE p.name in (:privileges) ")
	List<Privilege> getPrivileges(@Param("privileges")List<String> privileges);
}
