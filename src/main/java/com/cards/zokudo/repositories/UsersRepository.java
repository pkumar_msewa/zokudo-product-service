package com.cards.zokudo.repositories;

import com.cards.zokudo.entities.Users;
import com.cards.zokudo.enums.Status;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

@Repository
public interface UsersRepository extends JpaRepository<Users, Long>, JpaSpecificationExecutor<Users> {


    @Query("select u from Users u ")
    List<Object[]> getUserAuthDetails(String username, String programUrl);


    @Transactional(readOnly = true)
    @Query("SELECT u FROM Users u " +
            " INNER JOIN u.usersPrograms up " +
            "INNER JOIN up.program p " +
            " WHERE u.username = :username" +
            " AND p.hostUrl = :hostUrl" +
            " AND p.status = :status")
    Users findByUsername(@Param("username") final String username, @Param("hostUrl") final String hostUrl, @Param("status") final Status status);


    @Transactional(readOnly = true)
    @Query("SELECT u FROM Users u " +
            " INNER JOIN u.usersPrograms up " +
            " INNER JOIN up.program p " +
            " WHERE u.username = :username" +
            " AND p.hostUrl = :hostUrl")
    Users findByUsername(@Param("username") final String username, @Param("hostUrl") final String hostUrl);


    Users findByEmail(String email);

    Users findByMobile(String mobile);

    Users findById(long id);

    Users findByUserHashId(String userHashId);


    Page<Users> findAllByCreatedAtBetween(Date startDate, Date endDate, Pageable pageable);
}
