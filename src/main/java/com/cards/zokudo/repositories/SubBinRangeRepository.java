package com.cards.zokudo.repositories;

import com.cards.zokudo.entities.SubBinRange;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface SubBinRangeRepository extends JpaRepository<SubBinRange, Long>{

    List<SubBinRange> findAllByCardType(String cardType);

    List<SubBinRange> findAll();

    SubBinRange findBySubBin(String subBin);
}
