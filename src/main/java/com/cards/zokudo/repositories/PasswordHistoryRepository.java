package com.cards.zokudo.repositories;

import com.cards.zokudo.entities.PasswordHistory;
import com.cards.zokudo.entities.Users;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface PasswordHistoryRepository
		extends CrudRepository<PasswordHistory, Long>, JpaSpecificationExecutor<PasswordHistory> {

	List<PasswordHistory> findByUsersOrderByCreatedAtDesc(Users user);

}
