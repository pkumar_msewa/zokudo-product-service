package com.cards.zokudo.repositories;

import com.cards.zokudo.entities.CardType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CardTypeRepository  extends JpaRepository<CardType , Long> {

    CardType findByCode(String code);

}
