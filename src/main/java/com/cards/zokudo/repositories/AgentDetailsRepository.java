package com.cards.zokudo.repositories;

import com.cards.zokudo.entities.AgentDetails;

import java.util.ArrayList;
import java.util.List;

import org.codehaus.jettison.json.JSONObject;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface AgentDetailsRepository extends JpaRepository<AgentDetails, Long>, JpaSpecificationExecutor<AgentDetails> {

    AgentDetails findByCompanyName(String companyName);

    AgentDetails findByEmail(String email);

    AgentDetails findByMobile(String mobile);

    AgentDetails findByUserHashId(String userHashId);

    @Query("select a from AgentDetails a where a.id=?1")
    AgentDetails findByAgentId(Long id);

	List<AgentDetails> findByDistributorHashId(String distributorHashId);

    List<AgentDetails> findByProgramHashId(String programHashId);

    List<Long> getAgentIdByProgramHashId(String programHashId);

    @Query("SELECT a.companyName, a.fullName, a.userHashId, a.status, a.distributorHashId FROM AgentDetails a WHERE a.distributorHashId= ?1")
    List<JSONObject> getAgentIdByDistributorHashId(String distributorHashId);
}
