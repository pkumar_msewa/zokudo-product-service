package com.cards.zokudo.repositories;

import com.cards.zokudo.entities.ProgramHasBin;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ProgramHasBinRepository extends JpaRepository<ProgramHasBin, Long> {

    ProgramHasBin findByProgramId(Long programId);

    @Query("select binNumber from ProgramHasBin where bin_number like :bin%")
    List<String> findByProgramType(@Param("bin") String bin);
}
