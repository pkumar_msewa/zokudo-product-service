package com.cards.zokudo.repositories;

import com.cards.zokudo.entities.LoginLogs;
import com.cards.zokudo.enums.Status;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface LoginLogsRepository extends JpaRepository<LoginLogs, Long> {

    List<LoginLogs> findByUserIdAndStatus(long userId, Status status);

}
