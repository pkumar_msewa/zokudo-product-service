package com.cards.zokudo.repositories;

import com.cards.zokudo.entities.AccountType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AccountTypeRepository extends JpaRepository<AccountType , Long> {

}
