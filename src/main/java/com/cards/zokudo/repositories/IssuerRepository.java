package com.cards.zokudo.repositories;

import com.cards.zokudo.entities.Issuer;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface IssuerRepository extends JpaRepository<Issuer , Long> {
}
