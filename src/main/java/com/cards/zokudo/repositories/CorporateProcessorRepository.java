package com.cards.zokudo.repositories;

import com.cards.zokudo.entities.CorporateProcessor;
import com.cards.zokudo.enums.Status;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

@Repository
public interface CorporateProcessorRepository extends JpaRepository<CorporateProcessor, Long>, JpaSpecificationExecutor<CorporateProcessor> {

    List<CorporateProcessor> findAllByStatus(Status status);

    @Query("SELECT c FROM CorporateProcessor c WHERE c.createdAt BETWEEN ?1 AND ?2")
    Page<CorporateProcessor> findAllByDate(Date start, Date end, Pageable pageable);
}
