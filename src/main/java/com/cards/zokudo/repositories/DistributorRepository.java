package com.cards.zokudo.repositories;

import com.cards.zokudo.entities.DistributorEntity;

import java.util.List;

import org.codehaus.jettison.json.JSONObject;
import org.springframework.data.domain.Page;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;

public interface DistributorRepository extends JpaRepository<DistributorEntity, Long>, JpaSpecificationExecutor<DistributorEntity> {


    DistributorEntity findByCompanyName(String companyName);

    DistributorEntity findByEmail(String email);

    DistributorEntity findByMobile(String mobile);

	List<DistributorEntity> findByProgramHashId(String programHashId);

    @Query("SELECT d.companyName, d.userHashId, d.email, d.mobile FROM DistributorEntity d WHERE d.programHashId=?1")
    List<JSONObject> getDistributorByProgramHashId(String programHashId);

	DistributorEntity findByUserHashId(String userHashId);
}
