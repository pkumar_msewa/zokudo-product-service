package com.cards.zokudo.repositories;

import com.cards.zokudo.entities.Commercials;
import com.cards.zokudo.entities.Program;

import java.util.Date;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
public interface CommercialsRepository extends JpaRepository<Commercials, Long> , JpaSpecificationExecutor<Commercials>, PagingAndSortingRepository<Commercials, Long> {

    Commercials findByHashId(String hashId);

    Commercials findByProgram(Program program);
    
    Commercials findByClientId(Long clientId);

    @Transactional
    @Query("SELECT c FROM Commercials c where c.program.id=?1")
    Commercials findByProgramId(Long program_id);

	Page<Commercials> findAllByCreatedAtBetween(Date startDate, Date endDate, Pageable pageable);
}
