package com.cards.zokudo.repositories;

import com.cards.zokudo.entities.ClientDetails;
import com.cards.zokudo.entities.Users;
import org.codehaus.jettison.json.JSONObject;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;
import java.util.Optional;

@Repository
public interface ClientDetailsRepository extends JpaRepository<ClientDetails , Long>, JpaSpecificationExecutor<ClientDetails> {

    ClientDetails findByMobile(String mobile);

    ClientDetails findByEmail(String email);

    ClientDetails findByUsers(Users users);

    @Query("select c.users.id FROM ClientDetails c where c.id=?1")
    Long findUserId(long clientId);

    ClientDetails findByEntityName(String entityName);
    
    Optional<ClientDetails> findById(Long clientId);

    @Query("SELECT c.id, c.entityName, c.email, c.mobile, c.clientHashId FROM ClientDetails c WHERE c.id=?1")
    JSONObject getclientDetailsById(Long clientId);

    @Query("SELECT c FROM ClientDetails c WHERE c.createdAt BETWEEN ?1 AND ?2")
    Page<ClientDetails> findAllByDate(Date startDate, Date endDate, Pageable pageable);

    ClientDetails getByClientHashId(String clientHashId);

    @Query(value = "SELECT DISTINCT(cd.id) as unique_id, cd.* FROM `client_details` cd JOIN `program` p ON cd.`id` = p.`client_id`  WHERE `program_plan` IN :program_plan", nativeQuery = true)
    List<ClientDetails> clientUniqueListByProgramPlan(@Param("program_plan") List program_plan);
}
