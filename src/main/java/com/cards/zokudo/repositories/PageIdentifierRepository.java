package com.cards.zokudo.repositories;

import com.cards.zokudo.entities.PageIdentifier;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PageIdentifierRepository extends JpaRepository<PageIdentifier, Long> {
}
