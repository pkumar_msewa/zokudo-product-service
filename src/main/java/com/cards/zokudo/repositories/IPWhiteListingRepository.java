package com.cards.zokudo.repositories;

import com.cards.zokudo.entities.IPWhiteListing;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface IPWhiteListingRepository extends JpaRepository<IPWhiteListing, Long> {
    IPWhiteListing findByIpAddress(String ipAddress);

    IPWhiteListing findById(long id);
}
