package com.cards.zokudo.request;

import lombok.Data;

@Data
public class UpdatePasswordRequest {

    private String newPassword;

    private String confirmPassword;

    private String userName;

    private String otp;



}
