package com.cards.zokudo.request;

import lombok.Data;

@Data
public class ChangePasswordRequest {


    private String existingPassword;

    private String newPassword;

    private String confirmPassword;

    private String captchaResponse;

    private String mobileToken;


}
