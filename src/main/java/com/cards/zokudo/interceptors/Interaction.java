package com.cards.zokudo.interceptors;

import com.cards.zokudo.util.AppConstants;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.MDC;

import java.util.UUID;

public class Interaction {

    public static String getRequestId() {
        return StringUtils.defaultIfEmpty(MDC.get(AppConstants.HEADER_X_REQUEST_ID), UUID.randomUUID().toString());
    }
}
